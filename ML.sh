#!/bin/bash

cd /msl2/preprocessing/

touch /tmp/healthy


if [[ ${MSL2_PRECIP_MASK_TRAINING} == "True" ]]; then
    conda run --no-capture-output -n precipmask_env python precip-mask/precipmask_mltraining.py \
    -c ./precip-mask/config/precip-mask_config.yaml --msl_config ../msl2.json

else
    conda run --no-capture-output -n precipmask_env python precip-mask/precipmask_rtprod.py \
    -c ./precip-mask/config/precip-mask_config.yaml --msl_config ../msl2.json
    cd /msl2/postprocessing/
fi

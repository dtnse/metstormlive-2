FROM condaforge/mambaforge

# Create and set working dir
RUN mkdir -p /msl2
WORKDIR /msl2

# Create the environment:
RUN apt-get -y update && apt-get -y install pigz
COPY requirements/env.yaml .
RUN mamba env create -f env.yaml

# The code to run when container is started:
COPY postprocessing/ /msl2/postprocessing/
COPY k8s_user/ /msl2/
COPY docker-entrypoint.sh /usr/local/bin/
COPY msl2.json /msl2/.
RUN chmod +x /usr/local/bin/docker-entrypoint.sh
CMD ["docker-entrypoint.sh"]

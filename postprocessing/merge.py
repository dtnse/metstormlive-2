import gc
import glob
import tempfile
import os
import sys
import logging.config
from datetime import datetime
import time
import json

import pandas as pd
import geopandas
from osgeo import gdal
import s3fs
import xarray as xr
import numpy as np
import boto3
from scipy.interpolate import griddata

from utils import generate_tiles

s3_fs = s3fs.S3FileSystem(anon=False)
s3 = boto3.resource('s3', region_name="us-east-1")

ENV = os.getenv("ENV", "dev")

MSL2_INTERMEDIATE_BUCKET = os.getenv(
    "MSL2_INTERMEDIATE_BUCKET",
    "metstormlive-v2-intermediate-dev")

MSL2_MODI_BUCKET = os.getenv(
    "MSL2_MODI_BUCKET",
    "mg-obs-gridded-dtn-1qpe-dev-euw1")

logging.config.fileConfig("logging.ini", disable_existing_loggers=False)
logger = logging.getLogger(__name__)

epoch_timestamp = datetime.utcnow().timestamp()


def merge_CC(zulu_time, build_tiles=True):
    """Diagostic QPE based on the workflow found at:
    https://dtnse1.atlassian.net/wiki/spaces/CCS/pages/33565901012/Proposed+Multi-sensor+source+Integration+Algorithm+for+MetStormLive2+QPE
    """
    config_file = os.getcwd().replace("postprocessing", "msl2.json")
    config_data = dict()
    with open(config_file) as f:
        config_data.update(json.load(f))
    config_CC = config_data["domains"][0]
    dom = config_CC["abbreviated_name"]
    start_time = time.time()
    logger.info(f" {dom}MergeBegan: {zulu_time:%Y%m%d_%H%M%S}")

    # get climo basemap file for current month for creation of dynamic basemap
    try:
        basemap_prefix = f"/basemap/CC_bmp_1981to2010_{zulu_time:%b}.nc"
        basemap_key = MSL2_INTERMEDIATE_BUCKET + basemap_prefix
        with s3_fs.open(basemap_key, "rb") as f:
            CLM = xr.load_dataset(f)
        logger.info(f" {dom}MergeClimoBackground: {basemap_prefix}")
        tot_files = 1
    except BaseException:
        logger.info(f" {dom}MergeClimoBackground: NONE")
        tot_files = 0
    # Create the lat lon grid for inverse distance weighting
    # Currently the sparse grid is required to do idw weighting at 10km
    CC_lon = np.arange(config_CC["minx"], config_CC["maxx"]+config_CC["grid_dx_deg"],
                       config_CC["grid_dx_deg"])
    CC_lat = np.arange(config_CC["miny"], config_CC["maxy"]+config_CC["grid_dy_deg"],
                       config_CC["grid_dy_deg"])
    new_2d_lon, new_2d_lat = np.meshgrid(CC_lon, CC_lat)
    new_sparse_lon = np.arange(config_CC["minx"], config_CC["maxx"], config_CC["sparse_dx_deg"])
    new_sparse_lat = np.arange(config_CC["miny"], config_CC["maxy"], config_CC["sparse_dy_deg"])
    sparse_2d_lon, sparse_2d_lat = np.meshgrid(new_sparse_lon, new_sparse_lat)

    del new_sparse_lon, new_sparse_lat
    gc.collect()

    # Get the rest of the files and put the data they contain into a dictionary for
    # easy access later
    object_prefix = f"/preprocessed/{zulu_time:%Y%m%d_%H}/{dom}_*001hr.nc"

    s3_fs.invalidate_cache()

    file_list = s3_fs.glob(MSL2_INTERMEDIATE_BUCKET + object_prefix)
    # Create a dictionary with the data files to pull from later
    files_dict = {}
    for f in file_list:
        f_dataset = f.split("/")[-1]
        dataset = f_dataset[3:6]
        files_dict[dataset] = f

    logger.info(f" {dom}MergeFilesAvail: {files_dict}")
    tot_files = tot_files + len(files_dict.keys())
    if np.isin('DEN', files_dict.keys()):
        tot_files = tot_files - 1
    data_dict = {}
    for k in files_dict.keys():
        logger.info(f" {dom}MergeProcessingFile: {files_dict[k]}")
        with s3_fs.open(files_dict[k], "rb") as f:

            data = xr.load_dataset(f, decode_times=False)
            var = data[config_data["dataset_names"][k]]

            data_dict[k] = var.values.flatten()

    # Look for missing files
    init_keys = np.array(["PPR", "Z2P", "GHE", "PER", "QPF"])
    avail_data = np.fromiter(files_dict.keys(), dtype="S128")
    avail_data = [avail_data[i].decode("utf-8") for i in range(len(avail_data))]
    isin_test_missing = np.isin(init_keys, avail_data)
    w_notin_missing = np.where(~isin_test_missing)[0]
    # Have to separate these so can correctly get missing list from init_keys
    missing_files = []
    if len(w_notin_missing) == 1:
        missing_files = init_keys[w_notin_missing[0]]
        missing_files = [missing_files]
    elif len(w_notin_missing) > 1:
        missing_files = init_keys[w_notin_missing]
    logger.info(f" {dom}MergeMissFiles: {','.join(missing_files)}")

    # If RCO from this hour is not available, find most recent RCO file
    # If neither precip dataset present, set RCO 0 everywhere

    if 'PPR' in missing_files and 'Z2P' in missing_files:
        base_rco = np.zeros(new_2d_lat.shape)
        have_rco = 'Both radar missing'
    elif np.isin('RCO', data_dict.keys()):
        base_rco = np.nan_to_num(data_dict["RCO"])
        have_rco = "Current Hour"
    else:
        have_rco = "Latest"
        latest_rco = s3_fs.glob(MSL2_INTERMEDIATE_BUCKET + "/default/CC_RCO_latest.nc")
        with s3_fs.open(latest_rco[0], "rb") as f:
            data = xr.load_dataset(f, decode_times=False)
            var = data[config_data["dataset_names"]["RCO"]]
            data_dict["RCO"] = var.values.flatten()
            base_rco = np.nan_to_num(data_dict["RCO"])

    # If after checking the last 24 hours we still don't have an RCO file:
    logger.info(f"{dom}MergeHaveRCO: {have_rco}")

    # Initialize merging weights
    ppr_init = (16 * base_rco) / ((16 * base_rco) + base_rco + 2 * (1 - base_rco) +
                                  ((1 - base_rco) / 2.0))
    z2p_init = base_rco / (16 * base_rco + base_rco + 2 * (1 - base_rco) + (1 - base_rco) / 2.0)
    ghe_init = per_init = (1 - base_rco) / ((16 * base_rco) + base_rco + 2 * (1 - base_rco) +
                                            ((1 - base_rco) / 2.0))
    qpf_init = ((1 - base_rco)/2.0) / ((16 * base_rco) + base_rco + (2 * (1 - base_rco)) +
                                       ((1 - base_rco) / 2.0))
    init_list = np.array([ppr_init, z2p_init, ghe_init, per_init, qpf_init])

    del base_rco, ghe_init, per_init, qpf_init
    gc.collect()

    # Delete the files that are missing from the list of valid keys
    init_keys = np.delete(init_keys, w_notin_missing)
    init_list = np.delete(init_list, w_notin_missing, axis=0)
    # recalculate weights based on what is missing based on
    # what percentage of the sum of weights each weight is
    # after removing the missing files

    initwt_dict = {}

    sum_of_weights = np.zeros(init_list[0].shape)
    for i in range(len(init_keys)):
        sum_of_weights = sum_of_weights + init_list[i]
    first_guess = np.zeros(init_list[0].shape)
    for j, key in enumerate(init_keys):
        initwt_dict[key] = init_list[j] / sum_of_weights
        var = data_dict[key]
        var = np.nan_to_num(var)
        first_guess = first_guess + var * initwt_dict[key]

    # Calculate confidence metrics here so that there will
    # still be confidence metrics even if we stop at first guess

    # Variance of input precip datasets ####
    max_conf_var = 7.0
    stack_this = []
    for key in data_dict.keys():
        stack_this.append(data_dict[key])

    v = np.nanvar(np.vstack(stack_this), axis=0)
    conf_var = (1 - (v/np.nanmax(v)))*7.0
    conf_var = np.reshape(conf_var, new_2d_lat.shape)

    del stack_this, v
    gc.collect()

    # Variance of this months climatological basemap ####
    # should really just make climo confidence files honestly
    max_conf_cvar = 3.0
    try:
        basemap_var_prefix = f"/basemap/CC_bvr_1981to2010_{zulu_time:%b}.nc"
        basemap_var_key = MSL2_INTERMEDIATE_BUCKET + basemap_var_prefix
        with s3_fs.open(basemap_var_key, "rb") as f:
            CLM_var = xr.load_dataset(f)
        logger.info(f" {dom}MergeClimoBackground: {basemap_prefix}")

    except BaseException:
        logger.info(f" {dom}MergeClimoBackground: NONE")

    conf_cvar = (1 - (CLM_var.bmap_variance.values/np.nanmax(CLM_var.bmap_variance.values)))*3.0
    conf_cvar = np.reshape(conf_cvar, new_2d_lat.shape)

    del CLM_var
    gc.collect()

    # Where is density high

    max_conf_den = 12.0
    if 'DEN' in data_dict.keys():
        conf_den = np.reshape(data_dict["DEN"], new_2d_lat.shape) * 12
    else:
        conf_den = np.zeros(new_2d_lat.shape)

    # Beam block mask 0 = no blockage; 1 = totally blocked ####
    max_conf_blk = 5.0
    blockage_prefix = "/default/CC_BLK_latest.nc"
    blockage_key = MSL2_INTERMEDIATE_BUCKET + blockage_prefix
    with s3_fs.open(blockage_key, "rb") as f:
        BLK = xr.load_dataset(f)
    rco_msk = np.nan_to_num(np.reshape(data_dict["RCO"], new_2d_lat.shape))
    rco_msk[rco_msk > 0] = 1
    conf_blk = (1 - BLK["blockagemap"]) * rco_msk
    conf_blk = conf_blk * 5.0

    del BLK
    gc.collect()

    # Do we have PPR (if yes do RCO)
    max_conf_ppr = 8.0
    max_conf_rco = 5.0
    if 'PPR' in data_dict.keys():
        conf_ppr = np.ones(new_2d_lat.shape)*8.0*rco_msk
        conf_rco = np.reshape(data_dict["RCO"], new_2d_lat.shape) * 5
    else:
        conf_ppr = np.zeros(new_2d_lat.shape)
        conf_rco = 0
    # Do we have Z2P (if yes and rco not already done do RCO)
    max_conf_z2p = 6.0
    if 'Z2P' in data_dict.keys():
        conf_z2p = np.ones(new_2d_lat.shape)*6.0
        conf_z2p = conf_z2p * np.isfinite(np.reshape(data_dict['Z2P'],
                                          new_2d_lat.shape)).astype(int)

        if isinstance(conf_rco, int):
            conf_rco = np.reshape(data_dict["RCO"], new_2d_lat.shape) * 5
    else:
        conf_z2p = np.zeros(new_2d_lat.shape)
        conf_rco = np.zeros(new_2d_lat.shape)

    del rco_msk
    gc.collect()

    # Satellite Prob of Precip MSK
    max_conf_msk = 4.0
    conf_msk = np.zeros(new_2d_lat.shape)
    if 'MSK' in data_dict.keys():
        conf_msk[np.reshape(data_dict["MSK"], new_2d_lat.shape) > 0.6] = 1.0
        conf_msk[np.reshape(data_dict["MSK"], new_2d_lat.shape) < 0.4] = 1.0
        conf_msk[np.reshape(data_dict["MSK"], new_2d_lat.shape) > 0.7] = 2.0
        conf_msk[np.reshape(data_dict["MSK"], new_2d_lat.shape) < 0.3] = 2.0
        conf_msk[np.reshape(data_dict["MSK"], new_2d_lat.shape) > 0.8] = 3.0
        conf_msk[np.reshape(data_dict["MSK"], new_2d_lat.shape) < 0.2] = 3.0
        conf_msk[np.reshape(data_dict["MSK"], new_2d_lat.shape) > 0.9] = 4.0
        conf_msk[np.reshape(data_dict["MSK"], new_2d_lat.shape) < 0.1] = 4.0

    # do we have GHE
    max_conf_ghe = 3.0
    if 'GHE' in data_dict.keys():
        conf_ghe = np.ones(new_2d_lat.shape)*3.0
    else:
        conf_ghe = np.zeros(new_2d_lat.shape)
    # do we have PER
    max_conf_per = 3.0
    if 'PER' in data_dict.keys():
        conf_per = np.ones(new_2d_lat.shape)*3.0
    else:
        conf_per = np.zeros(new_2d_lat.shape)
    # do we have QPF
    max_conf_qpf = 2.0
    if 'QPF' in data_dict.keys():
        conf_qpf = np.ones(new_2d_lat.shape)*2.0
    else:
        conf_qpf = np.zeros(new_2d_lat.shape)
    # Is TYP liquid or frozen
    max_conf_typ = 2.0
    if 'TYP' in data_dict.keys():
        typ_frz = np.where((np.reshape(data_dict["TYP"], new_2d_lat.shape) == 1) |
                           (np.reshape(data_dict["TYP"], new_2d_lat.shape) == 5) |
                           (np.reshape(data_dict["TYP"], new_2d_lat.shape) == 2))
        conf_typ = np.ones(new_2d_lat.shape)
        conf_typ[typ_frz] = 0
        conf_typ = conf_typ*2
    else:
        conf_typ = np.zeros(new_2d_lat.shape)
    # latitude N/S 60
    max_conf_lat = 2.0
    conf_lat = np.ones(new_2d_lat.shape)*2
    conf_lat[np.where(abs(new_2d_lat) > 60)] = 0.0

    conf_sum = conf_den + conf_ppr + conf_var + conf_z2p + conf_rco + conf_blk +\
        conf_msk + conf_cvar + conf_ghe + conf_per + conf_typ + conf_qpf + conf_lat
    max_conf_sum = max_conf_den + max_conf_ppr + max_conf_var + max_conf_z2p + max_conf_rco +\
        max_conf_blk + max_conf_msk + max_conf_cvar + max_conf_ghe + max_conf_per + max_conf_typ +\
        max_conf_qpf + max_conf_lat

    conf_sum = conf_sum/max_conf_sum

    del conf_den, conf_ppr, conf_var, conf_z2p, conf_rco, conf_blk, conf_msk
    del conf_cvar, conf_ghe, conf_per, conf_typ, conf_qpf, conf_lat
    gc.collect()

# Save the confidence metric
    ecm_filename = f"CC_ECM_{zulu_time:%Y%m%d_%H}00_001hr.nc"

    ecm = xr.Dataset({
        "qpe_conf": (("lat", "lon"), conf_sum),
    })

    ecm.coords["lon"] = (("lon"), CC_lon)
    ecm.coords["lat"] = (("lat"), CC_lat)

    ecm["time"] = epoch_timestamp
    ecm.time.attrs["units"] = "Seconds since 1970-01-01 00:00:00"

    comp = dict(zlib=True, complevel=4)
    encoding = {var: comp for var in ecm.data_vars}

    with tempfile.NamedTemporaryFile() as nc_tmpfile:
        ecm.to_netcdf(nc_tmpfile.name, encoding=encoding)
        bucket_obj = s3.Bucket(MSL2_INTERMEDIATE_BUCKET)
        ecm_object_key = f"postprocessed/{zulu_time:%Y%m%d_%H}/{ecm_filename}"
        bucket_obj.upload_file(Filename=nc_tmpfile.name, Key=ecm_object_key)
        logger.info(f" {dom}MergePostprocessUpload: {ecm_filename}")

        # Upload to MODI -- Skip in Staging environment,
        # as there is no Staging MODI bucket!
        if ENV.lower() != 'stg':
            modi_filename = f"{dom}_msl-ecm_{zulu_time:%Y%m%d_%H}00_001hr.nc"
            MSL2_MODI_BUCKET_obj = s3.Bucket(MSL2_MODI_BUCKET)
            modi_object_key = f"incoming/netcdf/{dom}/{zulu_time:%Y%m%dT%H}0000Z/{modi_filename}"
            MSL2_MODI_BUCKET_obj.upload_file(Filename=nc_tmpfile.name,
                                        Key=modi_object_key,
                                        ExtraArgs={'ACL': 'bucket-owner-full-control'})
            logger.info(f" {dom}MergePostprocessUpload: {modi_filename}")
    if build_tiles:
        with tempfile.NamedTemporaryFile() as raster_tmpfile:
            ecm_tiles = xr.Dataset({"qpe_conf": (("latitude", "longitude"),
                                    ecm["qpe_conf"].values)})
            ecm_tiles.coords["longitude"] = (("longitude"), ecm.coords["lon"].values)
            ecm_tiles.coords["latitude"] = (("latitude"), ecm.coords["lat"].values)
            ecm_tiles["qpe_conf"].rio.set_spatial_dims(x_dim="longitude", y_dim="latitude",
                                                       inplace=True)
            raster = ecm_tiles["qpe_conf"].rio.write_crs("EPSG:4326")
            raster.rio.to_raster(raster_tmpfile.name, driver="GTiff")
            mbtiles = generate_tiles(raster_tmpfile.name)
            ecm_tilesname = ecm_filename.replace(".nc", ".mbtiles")
            bucket_obj.upload_file(Filename=mbtiles,
                                   Key=f"tiles/{zulu_time:%Y%m%d_%H}/{ecm_tilesname}")
            logger.info(f" {dom}MergeTilesUpload: {ecm_tilesname}")

    # Apply the satellite MSK here
    try:
        if 'PPR' in missing_files:
            ppr_init[:] = 0.0
        if 'Z2P' in missing_files:
            z2p_init[:] = 0.0
        MSK_adjust = first_guess * (1 - ((ppr_init + z2p_init)/sum_of_weights)) *\
            (1 - data_dict["MSK"])
        first_guess_plot = first_guess - MSK_adjust
        first_guess_plot[first_guess_plot < 0.0] = 0.0
        logger.info(f" {dom}MergeHaveSatMSK: {True}")
    except KeyError:
        logger.info(f" {dom}MergeHaveSatMSK: {False}")
    del sum_of_weights, ppr_init, z2p_init
    gc.collect()
    # Reshape first_guess for easier saving
    first_guess_plot = np.reshape(first_guess, new_2d_lat.shape)
    # Add in values for the missing data here so that the fields identifying which
    # dataset contains the max/min is correct
    for mf in missing_files:
        data_dict[mf] = np.zeros(new_2d_lat.flatten().shape)
    which_max = np.argmax((data_dict["GHE"], data_dict["PER"],
                           data_dict["PPR"], data_dict["Z2P"], data_dict["QPF"]), 0)
    for mf in missing_files:
        data_dict[mf] = np.zeros(new_2d_lat.flatten().shape) + 999
    which_min = np.argmin((data_dict["GHE"], data_dict["PER"],
                           data_dict["PPR"], data_dict["Z2P"], data_dict["QPF"]), 0)
    avail_precip_dict = {}
    for avail_key in init_keys:
        avail_precip_dict[avail_key] = data_dict[avail_key]

    # Find the max/min, median, average, standard dev
    # "GHE", "Z2P", "PPR", "MSK", "QPF", "CLM", "RCO", "PER"

    max_val = np.nanmax([data_dict[ikey] for ikey in init_keys], axis=0)
    min_val = np.nanmin([data_dict[ikey] for ikey in init_keys], axis=0)
    mean_val = np.nanmean([data_dict[ikey] for ikey in init_keys], axis=0)
    med_val = np.nanmedian([data_dict[ikey] for ikey in init_keys], axis=0)
    sd_val = np.nanstd([data_dict[ikey] for ikey in init_keys], axis=0)
    # reshape all the datasets for saving into netcdf
    which_max_2d = np.reshape(which_max, new_2d_lat.shape)
    which_min_2d = np.reshape(which_min, new_2d_lat.shape)
    max_val_2d = np.reshape(max_val, new_2d_lat.shape)
    min_val_2d = np.reshape(min_val, new_2d_lat.shape)
    mean_val_2d = np.reshape(mean_val, new_2d_lat.shape)
    med_val_2d = np.reshape(med_val, new_2d_lat.shape)
    sd_val_2d = np.reshape(sd_val, new_2d_lat.shape)

    object_prefix = f"/preprocessed/{zulu_time:%Y%m%d_%H}/CC_OBS*"

    gauge_list = s3_fs.glob(MSL2_INTERMEDIATE_BUCKET + object_prefix)
    logger.info(f" {dom}MergeAllAvailGaugeFiles: {','.join(gauge_list)}")
    if gauge_list:
        gauge_file = gauge_list[-1]
        with s3_fs.open(gauge_file, "rb") as f:
            GGE = pd.read_csv(f, sep=",")

            GGE = GGE.rename(columns=lambda x: x.strip())
            tot_gauge_num = len(GGE["obs_ppt_in"][:])
            GGE = GGE[GGE["qc_flag"] >= config_CC["stn_qc_threshold"]]
        logger.info(f" {dom}MergeHasGaugeFile: {gauge_file}")
        tot_files += 1
        logger.info(f" {dom}MergeTotAvailFiles: {tot_files}")
    if (not gauge_list) or (tot_gauge_num == 0):
        logger.info(f" {dom}MergeNoGaugeFile: first guess is best qpe")
        tot_gauge_num = 0
        logger.info(f" {dom}MergeTotalGaugesInFile: {tot_gauge_num}")
        logger.info(f" {dom}MergeTotAvailFiles: {tot_files}")
        first_guess_plot[first_guess_plot <= 0.254] = 0.0
        # try:
        #     first_guess_plot[data_dict["MSK"].reshape(first_guess_plot.shape) <=
        #                      config_CC["sat_prob_thresh"]] = 0.0
        #     logger.info(f" {dom}MergeHaveSatMSK: {True}")
        # except BaseException:
        #     logger.info(f" {dom}MergeHaveSatMSK: {False}")
        max_qpe = np.nanmax(first_guess_plot)
        logger.info(f" {dom}MergeMaxQPE: {max_qpe}")
        qpe_filename = f"CC_QPE_{zulu_time:%Y%m%d_%H}00_001hr.nc"

        qpe = xr.Dataset({
            "best_qpe": (("lat", "lon"), first_guess_plot),
            "which_max": (("lat", "lon"), which_max_2d),
            "which_min": (("lat", "lon"), which_min_2d),
            "data_max": (("lat", "lon"), max_val_2d),
            "data_min": (("lat", "lon"), min_val_2d),
            "data_mean": (("lat", "lon"), mean_val_2d),
            "data_median": (("lat", "lon"), med_val_2d),
            "data_standard_dev": (("lat", "lon"), sd_val_2d)
        })

        qpe.coords["lon"] = (("lon"), CC_lon)
        qpe.coords["lat"] = (("lat"), CC_lat)

        qpe["time"] = epoch_timestamp
        qpe.time.attrs["units"] = "Seconds since 1970-01-01 00:00:00"

        qpe["best_qpe"].attrs["units"] = "mm"
        qpe["best_qpe"].attrs["missing"] = missing_files
        qpe["best_qpe"].attrs["have_gauges"] = "False"

        qpe["which_max"].attrs["dataset_map"] = (0, "GHE", 1, "PER", 2, "PPR", 3, "Z2P", 4, "QPF")
        qpe["which_min"].attrs["dataset_map"] = (0, "GHE", 1, "PER", 2, "PPR", 3, "Z2P", 4, "QPF")

        qpe["data_max"].attrs["units"] = "mm"
        qpe["data_min"].attrs["units"] = "mm"
        qpe["data_mean"].attrs["units"] = "mm"
        qpe["data_median"].attrs["units"] = "mm"
        qpe["data_standard_dev"].attrs["units"] = "mm"

        comp = dict(zlib=True, complevel=4)
        encoding = {var: comp for var in qpe.data_vars}

        with tempfile.NamedTemporaryFile() as nc_tmpfile:
            qpe.to_netcdf(nc_tmpfile.name, encoding=encoding)
            bucket_obj = s3.Bucket(MSL2_INTERMEDIATE_BUCKET)
            qpe_object_key = f"postprocessed/{zulu_time:%Y%m%d_%H}/{qpe_filename}"
            bucket_obj.upload_file(Filename=nc_tmpfile.name, Key=qpe_object_key)
            logger.info(f" {dom}MergePostprocessUpload: {qpe_filename}")

            # Upload to MODI -- Skip in Staging environment,
            # as there is no Staging MODI bucket!
            if ENV.lower() != 'stg':
                modi_filename = f"{dom}_msl-qpe_{zulu_time:%Y%m%d_%H}00_001hr.nc"
                MSL2_MODI_BUCKET_obj = s3.Bucket(MSL2_MODI_BUCKET)
                modi_object_key = (f"incoming/netcdf/{dom}/{zulu_time:%Y%m%dT%H}0000Z/"
                                   f"{modi_filename}")
                MSL2_MODI_BUCKET_obj.upload_file(Filename=nc_tmpfile.name,
                                            Key=modi_object_key,
                                            ExtraArgs={'ACL': 'bucket-owner-full-control'})
                logger.info(f" {dom}MergePostprocessUpload: {modi_filename}")
        if build_tiles:
            with tempfile.NamedTemporaryFile() as raster_tmpfile:
                qpe_tiles = xr.Dataset({"best_qpe": (("latitude", "longitude"),
                                       qpe["best_qpe"].values)})
                qpe_tiles.coords["longitude"] = (("longitude"), qpe.coords["lon"].values)
                qpe_tiles.coords["latitude"] = (("latitude"), qpe.coords["lat"].values)
                qpe_tiles["best_qpe"].rio.set_spatial_dims(x_dim="longitude", y_dim="latitude",
                                                           inplace=True)
                raster = qpe_tiles["best_qpe"].rio.write_crs("EPSG:4326")
                raster.rio.to_raster(raster_tmpfile.name, driver="GTiff")
                mbtiles = generate_tiles(raster_tmpfile.name)
                qpe_tilesname = qpe_filename.replace(".nc", ".mbtiles")
                bucket_obj.upload_file(Filename=mbtiles,
                                       Key=f"tiles/{zulu_time:%Y%m%d_%H}/{qpe_tilesname}")
                logger.info(f" {dom}MergeTilesUpload: {qpe_tilesname}")
        execution_time = (time.time() - start_time)
        logger.info(f" {dom}MergeTotalExecuteTime: {execution_time:.2f} seconds")
        return "first_guess"

    # Create the normalized basemap from climo and first guess field
    bmap = CLM["Basemap"].values.flatten()
    norm_bmap = bmap / (np.nanmax(bmap))
    norm_fgss = first_guess / np.amax(first_guess)
    first_guess_plot = np.reshape(first_guess, new_2d_lat.shape)
    dbmap = (norm_bmap + norm_fgss) / 2.0

    # compute nearest neighbor interpolation of first guess field to gauge points

    firstguess_at_gauge_nearest = griddata(
        (new_2d_lat.flatten(),
         new_2d_lon.flatten()),
        first_guess,
        (GGE["lat_dd"],
         GGE["lon_dd"]),
        method="nearest")
    dbmap_at_gauge_nearest = griddata((new_2d_lat.flatten(), new_2d_lon.flatten(
    )), dbmap, (GGE["lat_dd"], GGE["lon_dd"]), method="nearest")
    # Convert gauge obs inches to mm
    gauge_obs = np.array(GGE["obs_ppt_in"][:] * 25.4)
    len_gauges = len(gauge_obs)
    logger.info(f" {dom}MergeTotalGaugesInFile: {tot_gauge_num}")
    logger.info(f" {dom}MergeGaugesUsedInBiasCorrection: {len_gauges}")
    # Compute the normalized bias at the gauge points to
    # the dynamic basemap value at the nearest gridpoint

    nbias_file = "nbias_tmpfile.shp"
    nbias_points = (gauge_obs - firstguess_at_gauge_nearest) / dbmap_at_gauge_nearest
    geo_nbias = geopandas.GeoDataFrame(
        nbias_points, geometry=geopandas.points_from_xy(GGE.lon_dd, GGE.lat_dd), crs="EPSG:4326")
    geo_nbias.columns = ['nbias', 'geometry']
    geo_nbias.to_file(nbias_file)

    MISSING_VALUE = -9999.0
    grid_width = config_CC["nx"]
    grid_height = config_CC["ny"]

    bounds_minx = config_CC["minx"]-(config_CC["grid_dx_deg"]*0.5)
    bounds_miny = config_CC["miny"]-(config_CC["grid_dy_deg"]*0.5)
    bounds_maxx = config_CC["maxx"]+(config_CC["grid_dx_deg"]*0.5)
    bounds_maxy = config_CC["maxy"]+(config_CC["grid_dy_deg"]*0.5)

    grid_options = gdal.GridOptions(outputBounds=[bounds_minx, bounds_miny,
                                    bounds_maxx, bounds_maxy], outputSRS="EPSG:4326",
                                    algorithm="invdist", format="NetCDF",
                                    width=grid_width, height=grid_height,
                                    noData=MISSING_VALUE, zfield="nbias")
    obs_out = "nbias_grid_tmpfile.nc"

    gdal.Grid(obs_out, 'nbias_tmpfile.shp', options=grid_options)

    nbias_grid = xr.load_dataset(obs_out)
    nbias_grid = np.flip(nbias_grid['Band1'], axis=0)

    bias_idw = nbias_grid*np.reshape(dbmap, new_2d_lat.shape)

    bias_idw = nbias_grid*np.reshape(dbmap, new_2d_lat.shape)

    final_qpe_idw = bias_idw + first_guess_plot
    final_qpe_idw = np.array(final_qpe_idw)
    # nbias_sparse_grid_idw = np.zeros(sparse_2d_lat.shape)
    # # Compute the idw bias distance weighting
    # # Doing this on the sparse grid now so it doesn't take forever
    # for i in range(sparse_2d_lon.shape[0]):
    #     for j in range(sparse_2d_lat.shape[0]):

    #         if 'DEN' in data_dict.keys():
    #             if data_dict['DEN'].reshape(new_2d_lat.shape)[::10, ::10][i, j] > 0.0025:
    #                 y_grd = sparse_2d_lat[i, j]
    #                 x_grd = sparse_2d_lon[i, j]
    #                 i_dists = 1.0 / (np.sqrt((x_grd - GGE["lon_dd"])**2 +
    #                                  (y_grd - GGE["lat_dd"])**2 + 1)**config_CC["idw"])

    #                 nbias_sparse_grid_idw[i, j] = np.sum(nbias_points * i_dists) / np.sum(i_dists)
    #             else:
    #                 nbias_sparse_grid_idw[i, j] = 0.0

    #         else:
    #             y_grd = sparse_2d_lat[i, j]
    #             x_grd = sparse_2d_lon[i, j]
    #             i_dists = 1.0 / (np.sqrt((x_grd - GGE["lon_dd"])**2 +
    #                              (y_grd - GGE["lat_dd"])**2 + 1)**config_CC["idw"])

    #             nbias_sparse_grid_idw[i, j] = np.sum(nbias_points * i_dists) / np.sum(i_dists)
    # # Get the nbias back to the denser grid from the sparse grid
    # nbias_grid_idw_nearest = griddata((sparse_2d_lat.flatten(), sparse_2d_lon.flatten()),
    #                                   nbias_sparse_grid_idw.flatten(), (new_2d_lat, new_2d_lon),
    #                                   method="nearest")
    # # reshape to do the gaussian smoothing
    # nbias_grid_idw_nearest = np.reshape(nbias_grid_idw_nearest, new_2d_lat.shape)
    # dbmap = np.reshape(dbmap, new_2d_lat.shape)
    # nbias_grid_idw_nearest = gaussian_filter(nbias_grid_idw_nearest, config_CC["nbias_smth"])
    # # Multiply by the dynamic basemap to convert to a precip amount from bias space
    # # (see line 333 where conversion to bias space was done)
    # bias_idw_nearest = nbias_grid_idw_nearest * dbmap
    # # Add bias amounts to qpe
    # second_qpe_idw = bias_idw_nearest + first_guess_plot
    # final_qpe_idw = second_qpe_idw
    # Final tweaks of applying satellite mask and removing precip below a trace

    # try:
    #     final_qpe_idw[data_dict["MSK"].reshape(second_qpe_idw.shape) <=
    #                   config_CC["sat_prob_thresh"]] = 0.0
    #     logger.info(f" {dom}MergeHaveSatMSK: {True}")
    # except BaseException:
    #     logger.info(f" {dom}MergeHaveSatMSK: {False}")

    # Let's do a little cleanup of arrays and files no longer needed
    del nbias_grid, bias_idw, first_guess_plot
    if os.path.isfile(obs_out):
        os.remove(obs_out)
    for shpfile in glob.glob("%s.*" % nbias_file.split(".")[0]):
        os.remove(shpfile)
    gc.collect()

    final_qpe_idw[final_qpe_idw <= 0.254] = 0.0
    max_qpe = np.nanmax(final_qpe_idw)
    logger.info(f" {dom}MergeMaxQPE: {str(max_qpe)}")
    # Save the file
    object_prefix = f"/preprocessed/{zulu_time:%Y%m%d_%H}/CC_*001hr.nc"

    qpe_filename = f"CC_QPE_{zulu_time:%Y%m%d_%H}00_001hr.nc"

    qpe = xr.Dataset({
        "best_qpe": (("lat", "lon"), final_qpe_idw),
        "which_max": (("lat", "lon"), which_max_2d),
        "which_min": (("lat", "lon"), which_min_2d),
        "data_max": (("lat", "lon"), max_val_2d),
        "data_min": (("lat", "lon"), min_val_2d),
        "data_mean": (("lat", "lon"), mean_val_2d),
        "data_median": (("lat", "lon"), med_val_2d),
        "data_standard_dev": (("lat", "lon"), sd_val_2d)
    })

    qpe.coords["lon"] = (("lon"), CC_lon)
    qpe.coords["lat"] = (("lat"), CC_lat)

    qpe["time"] = epoch_timestamp
    qpe.time.attrs["units"] = "Seconds since 1970-01-01 00:00:00"

    qpe["best_qpe"].attrs["units"] = "mm"
    qpe["best_qpe"].attrs["missing"] = missing_files
    qpe["best_qpe"].attrs["have_gauges"] = gauge_file

    qpe["which_max"].attrs["dataset_map"] = (0, "GHE", 1, "PER", 2, "PPR", 3, "Z2P", 4, "QPF")
    qpe["which_min"].attrs["dataset_map"] = (0, "GHE", 1, "PER", 2, "PPR", 3, "Z2P", 4, "QPF")

    qpe["data_max"].attrs["units"] = "mm"
    qpe["data_min"].attrs["units"] = "mm"
    qpe["data_mean"].attrs["units"] = "mm"
    qpe["data_median"].attrs["units"] = "mm"
    qpe["data_standard_dev"].attrs["units"] = "mm"

    comp = dict(zlib=True, complevel=4)
    encoding = {var: comp for var in qpe.data_vars}

    with tempfile.NamedTemporaryFile() as nc_tmpfile:
        qpe.to_netcdf(nc_tmpfile.name, encoding=encoding)
        bucket_obj = s3.Bucket(MSL2_INTERMEDIATE_BUCKET)
        qpe_object_key = f"postprocessed/{zulu_time:%Y%m%d_%H}/{qpe_filename}"
        bucket_obj.upload_file(Filename=nc_tmpfile.name, Key=qpe_object_key)
        logger.info(f" {dom}MergePostprocessUpload: {qpe_filename}")

        # Upload to MODI -- Skip in Staging environment,
        # as there is no Staging MODI bucket!
        if ENV.lower() != 'stg':
            modi_filename = f"{dom}_msl-qpe_{zulu_time:%Y%m%d_%H}00_001hr.nc"
            MSL2_MODI_BUCKET_obj = s3.Bucket(MSL2_MODI_BUCKET)
            modi_object_key = f"incoming/netcdf/{dom}/{zulu_time:%Y%m%dT%H}0000Z/{modi_filename}"
            MSL2_MODI_BUCKET_obj.upload_file(Filename=nc_tmpfile.name,
                                        Key=modi_object_key,
                                        ExtraArgs={'ACL': 'bucket-owner-full-control'})
            logger.info(f" {dom}MergePostprocessUpload: {modi_filename}")
    if build_tiles:
        with tempfile.NamedTemporaryFile() as raster_tmpfile:
            qpe_tiles = xr.Dataset({"best_qpe": (("latitude", "longitude"),
                                    qpe["best_qpe"].values)})
            qpe_tiles.coords["longitude"] = (("longitude"), qpe.coords["lon"].values)
            qpe_tiles.coords["latitude"] = (("latitude"), qpe.coords["lat"].values)
            qpe_tiles["best_qpe"].rio.set_spatial_dims(x_dim="longitude", y_dim="latitude",
                                                       inplace=True)
            raster = qpe_tiles["best_qpe"].rio.write_crs("EPSG:4326")
            raster.rio.to_raster(raster_tmpfile.name, driver="GTiff")
            mbtiles = generate_tiles(raster_tmpfile.name)
            qpe_tilesname = qpe_filename.replace(".nc", ".mbtiles")
            bucket_obj.upload_file(Filename=mbtiles,
                                   Key=f"tiles/{zulu_time:%Y%m%d_%H}/{qpe_tilesname}")
            logger.info(f" {dom}MergeTilesUpload: {qpe_tilesname}")

    execution_time = (time.time() - start_time)

    logger.info(f" {dom}MergeTotalExecuteTime: {execution_time:.2f} seconds")

    return "second_guess"


if __name__ == "__main__":
    logger.info('Merge began')
    if len(sys.argv) > 1:
        zulu_time = datetime.strptime(sys.argv[1], "%Y%m%d%H")
    else:
        zulu_time = datetime.utcnow()
    merge_CC(zulu_time)
    exit(0)

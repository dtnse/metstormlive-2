import shutil
import os
import logging.config

from osgeo import gdal

logging.config.fileConfig('logging.ini', disable_existing_loggers=False)
logger = logging.getLogger(__name__)


def generate_tiles(fname, tiles_name=None, nc_dataset_name=None):
    CREATION_OPTS = ["TYPE=baselayer", "ZOOM_LEVEL_STRATEGY=UPPER", "ZLEVEL=9"]
    gdal.SetConfigOption("COMPRESS_OVERVIEW", "DEFLATE")
    gdal.SetConfigOption("GDAL_NUM_THREADS", "8")
    gdal.SetConfigOption("GDAL_CACHEMAX", "16000")
    palette_fname = "palettes/mm.txt"
    if nc_dataset_name:
        shutil.copyfile(fname, fname + ".nc")
        mbtiles_fname = tiles_name
        ds = gdal.Open(f"NETCDF:{fname}:{nc_dataset_name}")
    else:
        ds = gdal.Open(fname)
        mbtiles_fname = f"{fname}.mbtiles"

    # reproject to web mercator

    logger.info("Reprojecting to Web Mercator (EPSG:3857)")
    warp_output = "/tmp/ppr_warp"
    colors_output = "/tmp/ppr_colors"
    warp_options = gdal.WarpOptions(srcSRS="EPSG:4326", dstSRS="EPSG:3857")
    warp = gdal.Warp(warp_output, ds, options=warp_options)

    # apply custom color palette

    logger.info("Applying custom color palette to raster...")
    dem_options = gdal.DEMProcessingOptions(colorFilename=palette_fname,
                                            format="GTiff", addAlpha=True)
    colors = gdal.DEMProcessing(colors_output, warp, "color-relief", options=dem_options)

    # generate highest zoom level of tiles

    translate_options = gdal.TranslateOptions(format="MBTILES", creationOptions=CREATION_OPTS)
    logger.info("Creating Base tile layer...")
    translate = gdal.Translate(mbtiles_fname, colors, options=translate_options)

    # build the rest of the tile pyramid

    logger.info("Creating the rest of the tile pyramid...")
    translate.BuildOverviews("AVERAGE", [2, 4, 8, 16])
    os.remove(colors_output)
    os.remove(warp_output)
    return mbtiles_fname


def generate_tiles_vsis3(queue_listener, fname, output_fname, ds_name):
    s3_bucket_name = queue_listener.MSL2_INTERMEDIATE_BUCKET

    CREATION_OPTS = ["TYPE=baselayer", "ZOOM_LEVEL_STRATEGY=UPPER", "ZLEVEL=9"]
    gdal.SetConfigOption("COMPRESS_OVERVIEW", "DEFLATE")
    gdal.SetConfigOption("GDAL_NUM_THREADS", "8")
    gdal.SetConfigOption("GDAL_CACHEMAX", "16000")
    gdal.SetConfigOption("CPL_VSIL_USE_TEMP_FILE_FOR_RANDOM_WRITE", "YES")

    tiles_fname = output_fname.replace(".nc", ".mbtiles")

    colors_tmp = "/tmp/colors.tif"
    warp_tmp = "/tmp/warp.tif"
    mbtiles_tmp = f"/tmp/{tiles_fname}"

    palette_fname = "palettes/ppr.txt"

    nc = gdal.Open(f"NETCDF:{fname}:{ds_name}")
    # reproject to web mercator
    logger.info("Reprojecting to Web Mercator (EPSG:3857)")
    warp_options = gdal.WarpOptions(srcSRS="EPSG:4326", dstSRS="EPSG:3857", format="GTiff")
    gdal.Warp(warp_tmp, nc, options=warp_options)

    # apply custom color palette
    logger.info("Applying custom color palette to raster...")
    dem_options = gdal.DEMProcessingOptions(colorFilename=palette_fname,
                                            format="GTiff", addAlpha=True)
    colors = gdal.DEMProcessing(colors_tmp, warp_tmp, "color-relief", options=dem_options)

    # generate highest zoom level of tiles
    translate_options = gdal.TranslateOptions(format="MBTILES", creationOptions=CREATION_OPTS)
    ("Creating Base tile layer...")
    translate = gdal.Translate(mbtiles_tmp, colors, options=translate_options)

    # build the rest of the tile pyramid
    logger.info("Creating the rest of the tile pyramid...")
    translate.BuildOverviews("AVERAGE", [2, 4, 8, 16])

    # upload tiles to metstormlive2-intermediate-dev, to be picked up by the loki-mbtileserver
    logger.info(f"TilesUpload: {tiles_fname} to {s3_bucket_name}")
    bucket_obj = queue_listener.s3.Bucket(
        s3_bucket_name
    )
    prefix = f"tiles/{tiles_fname[7:18]}"
    bucket_obj.upload_file(Filename=mbtiles_tmp, Key=f"{prefix}/{tiles_fname}")

    os.remove(colors_tmp)
    os.remove(warp_tmp)
    os.remove(mbtiles_tmp)

    return tiles_fname

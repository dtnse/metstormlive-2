"""
allocate.py

Purpose: Allocate 1-hour best-estimate QPE into 5-minute buckets.

Written:  T Rost, Nov. 2022

Modifications:


"""

# ========================================================================================
# Built-in modules
# ========================================================================================
import json
import logging.config
import os
import sys

from datetime import datetime, timezone

# Third party modules
import boto3
import numpy as np

from netCDF4 import Dataset

logging.config.fileConfig("logging.ini", disable_existing_loggers=False)
logger = logging.getLogger(__name__)

# ========================================================================================
# Program defaults and global variables
# ========================================================================================

s3 = boto3.resource("s3")

ENV = os.getenv("ENV", "dev")

MSL2_BUCKET = s3.Bucket(os.getenv(
    "MSL2_INTERMEDIATE_BUCKET",
    "metstormlive-v2-intermediate-dev"))

MSL2_MODI_BUCKET = s3.Bucket(os.getenv(
    "MSL2_MODI_BUCKET",
    "mg-obs-gridded-dtn-1qpe-dev-euw1"))

MISSING_VALUE = -9999.0
INTERVAL = 300  # allocation interval (s)

# General MSL2 configuration file
config_file = "../msl2.json"
CONFIG_DATA = dict()
with open(config_file) as f:
    CONFIG_DATA.update(json.load(f))


def read_nc(file, variable, projection):
    """
    Open NetCDF data file and read specified variable into memory. Note that dimensions
    and projection type are expected to already match output grid.
    """

    # Open the file for reading
    nc = Dataset(file, "r")

    # Read lat/lon into 1D NumPy arrays
    lats = nc.variables["lat"]
    lons = nc.variables["lon"]

    # Read data array
    data = nc.variables[variable][:, :]

    # Verify expected dimensions
    if len(lats) != projection["ny"] or len(lons) != projection["nx"]:
        err = (" allocate.py: Dimensions ({} rows, {} cols) do not match output grid"
               " ({} rows, {} cols).  Exiting...")
        logger.error(err.format(len(lats), len(lons), projection["ny"], projection["nx"]))
        nc.close()
        exit(-1)

    # close file
    nc.close()

    return data


def write_nc(outfile, data, projection, dt_obj):

    # Projection specs
    proj4 = projection["proj4"]
    lat_sw = projection["miny"]
    lon_sw = projection["minx"]
    lat_ne = projection["maxy"]
    lon_ne = projection["maxx"]
    nx = projection["nx"]
    ny = projection["ny"]
    dx = projection["grid_dx_km"]
    dy = projection["grid_dy_km"]
    dlon = projection["grid_dx_deg"]
    dlat = projection["grid_dy_deg"]
    complev = projection["nc_compression_level"]

    # Following logic only applicable for lat-lon projection
    supported_projections = ["latlon", "latlong", "lonlat", "longlat"]
    if all([p not in proj4 for p in supported_projections]):
        logger.info("Warning: Expected one of the following for output projection: %s" %
                    str(supported_projections))

    lats1d = np.linspace(lat_sw, lat_ne, ny)  # coordinates are evenly spaced on lat-lon proj
    lons1d = np.linspace(lon_sw, lon_ne, nx)
    lats2d, lons2d = np.meshgrid(lons1d, lats1d)

    # Open output NetCDF file for writing
    nc = Dataset(outfile, "w", format="NETCDF4")

    # Precision for floating point attribute values
    decimals = 6

    # Add global attributes
    nc.Conventions = "CF-1.6"
    nc.title = "MetStormLive_Final_5min_QPE"
    nc.map_proj = "lat-lon"
    nc.institution = "DTN"
    nc.grid_mapping_name = "latitude_longitude"
    nc.DataConvention = "WE:SN"
    nc.nx = nx
    nc.ny = ny
    nc.dx = round(dx, decimals)  # km
    nc.dy = round(dy, decimals)
    nc.llcrnrlat = round(lat_sw, decimals)
    nc.llcrnrlon = round(lon_sw, decimals)
    nc.urcrnrlat = round(lat_ne, decimals)
    nc.urcrnrlon = round(lon_ne, decimals)
    nc.lowerleft_latlon = (round(lon_sw, decimals), round(lat_sw, decimals))
    nc.upperright_latlon = (round(lon_ne, decimals), round(lat_ne, decimals))
    nc.dlat = round(dlat, decimals)  # deg
    nc.dlon = round(dlon, decimals)

    # Add array dimensions
    nc.createDimension("lat", ny)
    nc.createDimension("lon", nx)

    # Add file variables
    # ... 5-minute best-estimate QPE
    nc_qpe = nc.createVariable(
        "qpe5min", "d", ("lat", "lon",), zlib=True, least_significant_digit=2, complevel=complev,
        fill_value=MISSING_VALUE
    )
    nc_qpe.coordinates = "lat lon"
    nc_qpe.grid_mapping = "latitude_longitude"
    nc_qpe.deflate = 1
    nc_qpe.deflate_level = complev
    nc_qpe.units = "mm"
    nc_qpe.long_name = "Msl5minQpeBestEstimate"
    nc_qpe.description = "MetStormLive2 5-minute QPE best-estimate, allocated from 1h QPE."
    nc_qpe[:, :] = np.ma.MaskedArray.copy(data["qpe5mn"])

    # ... 1h QPE allocation method
    nc_method = nc.createVariable(
        "method", "d", ("lat", "lon",), zlib=True, least_significant_digit=0, complevel=complev,
        fill_value=MISSING_VALUE
    )
    nc_method.coordinates = "lat lon"
    nc_method.grid_mapping = "latitude_longitude"
    nc_method.deflate = 1
    nc_method.deflate_level = complev
    nc_method.units = "dimensionless"
    nc_method.long_name = "1h QPE Allocation Method"
    nc_method.description = "MetStormLive2 1h QPE allocation method: 0 (Z2P), 1 (GHE), " + \
                            "2 (Z2P and GHE), and 3 (linear distribution)."
    nc_method[:, :] = np.ma.MaskedArray.copy(data["method"])

    # ... coordinate reference system definition
    nc_crs = nc.createVariable("crs", "c")
    nc_crs.grid_mapping_name = "latitude_longitude"
    nc_crs.long_name = "CRS definition"
    nc_crs.longitude_of_prime_meridian = 0.0
    nc_crs.semi_major_axis = 6378137.0
    nc_crs.inverse_flattening = 298.257223563
    nc_crs.spatial_ref = proj4
    nc_crs.GeoTransform = "%s %s 0 %s 0 %s " % (  # what do zeros indicate?
        round(lon_sw, decimals), round(dlon, decimals),
        round(lat_ne, decimals), round(dlat, decimals)
    )

    # ... 1D latitude array
    nc_lat = nc.createVariable("lat", "d", ("lat",), fill_value=MISSING_VALUE)
    nc_lat.standard_name = "latitude"
    nc_lat.long_name = "latitude"
    nc_lat.units = "degrees_north"
    nc_lat[:] = np.copy(lats1d)

    # ... 1D longitude array
    nc_lon = nc.createVariable("lon", "d", ("lon",), fill_value=MISSING_VALUE)
    nc_lon.standard_name = "longitude"
    nc_lon.long_name = "longitude"
    nc_lon.units = "degrees_east"
    nc_lon[:] = np.copy(lons1d)

    # ... data reference time
    nc_time = nc.createVariable("time", "i8")
    nc_time.standard_name = "time"
    nc_time.string = f"{dt_obj:%Y-%m-%dT%H:%M:%SZ}"
    nc_time.period_string = "0 minutes"
    nc_time.units = f"days since {dt_obj:%Y-%m-%d %H:%M:%S}"
    nc_time.calendar = "proleptic_gregorian"
    nc_time = 0

    # ... SW and NE corner coordinates
    nc_latlon = nc.createVariable("Latitude_Longitude", "i8")
    nc_latlon.grid_mapping_name = "latitude_longitude"
    nc_latlon.llcrnrlon = round(lon_sw, decimals)
    nc_latlon.llcrnrlat = round(lat_sw, decimals)
    nc_latlon.urcrnrlon = round(lon_ne, decimals)
    nc_latlon.urcrnrlat = round(lat_ne, decimals)
    nc_latlon = 0

    # ... 2D latitude array
    nc_latitude = nc.createVariable(
        'latitude', 'd', ('lat', 'lon'), zlib=True, least_significant_digit=4, complevel=complev,
        fill_value=MISSING_VALUE
    )
    nc_latitude.deflate = 1
    nc_latitude.deflate_level = complev
    nc_latitude.long_name = "latitude"
    nc_latitude.units = "degrees_north"
    nc_latitude.standard_name = "latitude"
    nc_latitude[:, :] = np.copy(lats2d)

    # ... 2D longitude array
    nc_longitude = nc.createVariable(
        'longitude', 'd', ('lat', 'lon'), zlib=True, least_significant_digit=4, complevel=complev,
        fill_value=MISSING_VALUE
    )
    nc_longitude.deflate = 1
    nc_longitude.deflate_level = complev
    nc_longitude.long_name = "longitude"
    nc_longitude.units = "degrees_east"
    nc_longitude.standard_name = "longitude"
    nc_longitude[:, :] = np.copy(lons2d)

    nc.close()

    return


def select_points(qpe1h, z2p1h, ghe1h, rco1h):
    """
    Allocation rules for all combinations of inputs in grid points where qpe1h>0:
        If rco1h, z2p1h, & ghe1h data all *available:
            If z2p1h>0 and ghe1h>0:
                If rco1h==0, use only GHE allocation percentages.
                If rco1h==1, use only Z2P allocation percentages.
                If 0<rco1h<1, use rco1h to compute weighted average of Z2P & GHE 5-minute
                    allocation percentages.
            If z2p1h>0 and ghe1h==0:
                If rco1h>0, use only Z2P allocation percentages.
                If rco1h==0, use only linear allocation percentages.
            If z2p1h==0 and ghe1h>0, use only GHE allocation percentages.
            If z2p1h==0 and ghe1h==0, use only linear allocation percentages.
        If only rco1h & z2p1h available:
            If z2p1h>0:
                If rco1h>0, use only Z2P allocation percentages.
                If rco1h==0, use only linear allocation percentages.
            If z2p1h==0, use only linear allocation percentages.
        If only rco1h & ghe1h available:
            If ghe1h>0, use only GHE allocation percentages.
            If ghe1h==0, use only linear allocation percentages.
        If only z2p1h & ghe1h available:
            If z2p1h>0, use only Z2P allocation percentages.
            If z2p1h==0 and ghe1h>0, use only GHE allocation percentages.
            If z2p1h==0 and ghe1h==0, use only linear allocation percentages.
        If only z2p1h available:
            If z2p1h>0, use only Z2P allocation percentages.
            If z2p1h==0, use only linear allocation percentages.
        If only ghe1h available:
            If ghe1h>0, use only GHE allocation percentages.
            If ghe1h==0, use only linear allocation percentages.
        If only rco1h available, use only linear allocation percentages.
        If no data available, use only linear allocation percentages.

    * "Available" means the value is non-missing in a grid point.  Note that if source grids are
    completely missing, the corresponding array normally holding the data in memory will be
    defined but filled with missing values.
    """

    # Select points based on hourly accumulations and radar confidence for different flavors of
    # QPE allocation. Point selection logic could be condensed and simplified, but is spelled
    # out according to above rules for clarity.
    point_selection = dict()

    # ... Z2P-based (0) allocation
    point_selection["0"] = (
        ((qpe1h > 0.0) & (z2p1h > 0.0) & (ghe1h > 0.0) & (rco1h == 1.0)) |
        ((qpe1h > 0.0) & (z2p1h > 0.0) & (ghe1h == 0.0) & (rco1h > 0.0)) |
        ((qpe1h > 0.0) & (z2p1h > 0.0) & (ghe1h == ghe1h.fill_value) & (rco1h > 0.0)) |
        ((qpe1h > 0.0) & (z2p1h > 0.0) & (ghe1h >= 0.0) & (rco1h == rco1h.fill_value)) |
        ((qpe1h > 0.0) & (z2p1h > 0.0) & (ghe1h == ghe1h.fill_value) & (rco1h == rco1h.fill_value))
    )

    # ... GHE-based (1) allocation
    point_selection["1"] = (
        ((qpe1h > 0.0) & (ghe1h > 0.0) & (z2p1h > 0.0) & (rco1h == 0.0)) |
        ((qpe1h > 0.0) & (ghe1h > 0.0) & (z2p1h == 0.0) & (rco1h >= 0.0)) |
        ((qpe1h > 0.0) & (ghe1h > 0.0) & (z2p1h == z2p1h.fill_value) & (rco1h >= 0.0)) |
        ((qpe1h > 0.0) & (ghe1h > 0.0) & (z2p1h == 0.0) & (rco1h == rco1h.fill_value)) |
        ((qpe1h > 0.0) & (ghe1h > 0.0) & (z2p1h == z2p1h.fill_value) & (rco1h == rco1h.fill_value))
    )

    # ... Z2P & GHE (2) allocation
    point_selection["2"] = (
        (qpe1h > 0.0) & (z2p1h > 0.0) & (ghe1h > 0.0) & (rco1h > 0.0) & (rco1h < 1.0)
    )

    # ... linear / even (3) allocation (ALL OTHER POINTS where QPE1h>0)
    point_selection["3"] = (
        (qpe1h > 0.0) & (np.invert(point_selection["0"] | point_selection["1"] |
                         point_selection["2"]))
    )

    return point_selection


def process(valid_dt):

    # -- Ensure datetime corresponds to top of the hour

    valid_dt = valid_dt.replace(minute=0, second=0, microsecond=0, tzinfo=timezone.utc)
    logger.info(f" allocate.py: Allocating 1h QPE valid {valid_dt:%Y-%m-%d %H:%M UTC}...")

    # -- Convert datetime object to unix timestamp

    valid_time = int(datetime.timestamp(valid_dt))

    # -- Obtain gridded data and subdivide 1h QPE into subhourly buckets

    for region in CONFIG_DATA["domains"]:

        # ... download and read 1h QPE grid ...
        qpe1h = np.ma.masked_all((region["ny"], region["nx"]))

        s3key = f"postprocessed/{valid_dt:%Y%m%d_%H}"
        s3key += f"/{region['abbreviated_name']}_QPE_{valid_dt:%Y%m%d_%H%M}_001hr.nc"
        local = (s3key.split("/"))[-1]
        try:
            MSL2_BUCKET.download_file(s3key, local)
            qpe1h = read_nc(local, "best_qpe", region)
            os.remove(local)
        except Exception:
            logger.error(f" allocate.py: Failed to read s3://{MSL2_BUCKET.name}/{s3key}."
                         f" Not allocating 5-minute {region['abbreviated_name']} QPE for"
                         f" the hour ending {valid_dt:%Y-%m-%d %H:%M} UTC!")
            continue
        qpe1h.fill_value = MISSING_VALUE

        # ... download and read 1h RCO grid ...
        rco1h = np.ma.masked_all((region["ny"], region["nx"]))
        s3key = f"preprocessed/{valid_dt:%Y%m%d_%H}"
        s3key += f"/{region['abbreviated_name']}_RCO_{valid_dt:%Y%m%d_%H%M}_001hr.nc"
        local = (s3key.split("/"))[-1]
        try:
            MSL2_BUCKET.download_file(s3key, local)
            rco1h = read_nc(local, "RadarConfidence", region)
            rco1h = np.ma.round(rco1h, decimals=2)
            os.remove(local)
        except Exception:
            logger.info(" allocate.py: Failed to read s3://{MSL2_BUCKET.name}/{s3key}.")
        rco1h.fill_value = MISSING_VALUE

        # ... download and accumulate 5-minute Z2P into 1h total. Note that there is upstream logic
        # to interpolate 5-minute data if up to 6/12 source grids are unavailable, so we can assume
        # all 5-minute grids are going to be available. If any are missing, we'll treat Z2P data as
        # being entirely unavailable ...
        z2p1h = np.ma.zeros((region["ny"], region["nx"]), fill_value=MISSING_VALUE)

        z2p5mn_arrays = dict()  # store 5-minute data in memory for later use

        for unix in range(valid_time, valid_time-3600, -INTERVAL):
            dt_obj = datetime.fromtimestamp(unix, tz=timezone.utc)

            s3key = f"preprocessed/{dt_obj:%Y%m%d_%H}"
            s3key += f"/{region['abbreviated_name']}_Z2P_{dt_obj:%Y%m%d_%H%M}_005mn.nc"
            local = (s3key.split("/"))[-1]
            try:
                MSL2_BUCKET.download_file(s3key, local)
                z2p5mn_arrays[unix] = read_nc(local, "Precipitation_from_Lowalt", region)
                z2p1h += z2p5mn_arrays[unix]  # aggregate -- masked points should propagate
                os.remove(local)
            except Exception:
                logger.info(f" allocate.py: Failed to read s3://{MSL2_BUCKET.name}/{s3key}."
                            f" Will not use radar-based QPE to allocate 5-minute"
                            f" {region['abbreviated_name']} QPE for the hour ending"
                            f" {valid_dt:%Y-%m-%d %H:%M} UTC.")
                z2p5mn_arrays = dict()
                z2p1h.mask = True  # mask out entire array
                break

        # ... download and accumulate 15-minute GHE into 1h total. No upstream interpolation
        # logic to handle missing source grids in this case, but we'll still treat it as
        # being unavailable if any are missing ...
        ghe1h = np.ma.zeros((region["ny"], region["nx"]), fill_value=MISSING_VALUE)

        ghe5mn_arrays = dict()  # store 5-minute data in memory for later use
        ghe15min = None

        for unix in range(valid_time, valid_time-3600, -INTERVAL):
            dt_obj = datetime.fromtimestamp(unix, tz=timezone.utc)

            if unix % 900 == 0:
                s3key = f"preprocessed/{dt_obj:%Y%m%d_%H}"
                s3key += f"/{region['abbreviated_name']}_GHE_{dt_obj:%Y%m%d_%H%M}_015mn.nc"
                local = (s3key.split("/"))[-1]
                try:
                    MSL2_BUCKET.download_file(s3key, local)
                    ghe15min = read_nc(local, "ghe15min", region)
                    os.remove(local)
                except Exception:
                    logger.info(f" allocate.py: Failed to read s3://{MSL2_BUCKET.name}/{s3key}."
                                f" Will not use GHE-based QPE to allocate 5-minute"
                                f" {region['abbreviated_name']} QPE for the hour ending"
                                f" {valid_dt:%Y-%m-%d %H:%M} UTC.")
                    ghe5mn_arrays = dict()
                    ghe1h.mask = True  # mask out entire array
                    break

            ghe5mn_arrays[unix] = ghe15min / 3.0  # evenly distribute into 5-minute intervals
            ghe1h += ghe5mn_arrays[unix]  # aggregate -- masked points should propagate

        del ghe15min

        # ... use hourly data to identify allocation method in each grid point ...
        point_selection = select_points(qpe1h, z2p1h, ghe1h, rco1h)

        # ... subdivide 1h QPE ...
        for unix in range(valid_time, valid_time-3600, -INTERVAL):
            dt_obj = datetime.fromtimestamp(unix, tz=timezone.utc)

            data = dict(
                qpe5mn=np.ma.zeros((region["ny"], region["nx"])),
                method=np.ma.masked_all((region["ny"], region["nx"]))
            )

            # 5-minute Z2P total
            z2p5mn = z2p5mn_arrays.get(unix, np.zeros_like(data["qpe5mn"]))

            # 5-minute GHE total
            ghe5mn = ghe5mn_arrays.get(unix, np.zeros_like(data["qpe5mn"]))

            # Allocate 1-hour best-estimate into 5-minute buckets
            # ... Z2P (0)
            data["method"][point_selection["0"]] = 0.0
            data["qpe5mn"][point_selection["0"]] = qpe1h[point_selection["0"]] * \
                (z2p5mn[point_selection["0"]] / z2p1h[point_selection["0"]])

            # ... GHE (1)
            data["method"][point_selection["1"]] = 1.0
            data["qpe5mn"][point_selection["1"]] = qpe1h[point_selection["1"]] * \
                (ghe5mn[point_selection["1"]] / ghe1h[point_selection["1"]])

            # ... Z2P & GHE (2 - weighted average of allocation fractions)
            data["method"][point_selection["2"]] = 2.0
            data["qpe5mn"][point_selection["2"]] = qpe1h[point_selection["2"]] * (
                (rco1h[point_selection["2"]] * (z2p5mn[point_selection["2"]] /
                                                z2p1h[point_selection["2"]])) +
                ((1.0 - rco1h[point_selection["2"]]) * (ghe5mn[point_selection["2"]] /
                                                        ghe1h[point_selection["2"]]))
            )

            # ... linear (3)
            data["method"][point_selection["3"]] = 3.0
            data["qpe5mn"][point_selection["3"]] = qpe1h[point_selection["3"]] * \
                (INTERVAL / 3600.0)

            # Set mask from 1h QPE grid
            data["qpe5mn"] = np.ma.masked_where(np.ma.getmask(qpe1h), data["qpe5mn"])

            # Round to 2 decimals
            data["qpe5mn"] = np.ma.MaskedArray.round(data["qpe5mn"], decimals=2)

            # Above logic can flag points with an allocation method even though QPE 5-min is 0 mm;
            # these should be masked out
            data["method"] = np.ma.masked_where(data["qpe5mn"] == 0.0, data["method"])

            # Write out
            local = f"{region['abbreviated_name']}_QPE_{dt_obj:%Y%m%d_%H%M}_005mn.nc"
            write_nc(local, data, region, dt_obj)
            if not os.path.isfile(local):
                logger.error(" allocate.py: Failed to write {local}.")
                continue

            # Push to MSL2 S3 bucket
            s3key = (f"postprocessed/{dt_obj:%Y%m%d_%H}/{local}")
            MSL2_BUCKET.upload_file(Filename=local, Key=s3key)
            logger.info(f"PostprocessUpload: {local} to s3://{MSL2_BUCKET.name}/{s3key}.")

            # Push to MODI bucket
            if ENV.lower() != "stg":
                s3key = (f"incoming/netcdf/{region['abbreviated_name']}/{dt_obj:%Y%m%dT%H%M%S}Z/")
                s3key += local.replace("QPE", "msl-qpe")
                MSL2_MODI_BUCKET.upload_file(
                    Filename=local, Key=s3key, ExtraArgs={"ACL": "bucket-owner-full-control"})
                logger.info(f"PostprocessUpload: {local} to s3://{MSL2_MODI_BUCKET.name}/{s3key}.")

            # File cleanup
            os.remove(local)

    logger.info(f" allocate.py: Finished processing 1h period ending"
                f" {valid_dt:%Y-%m-%d %H:%M UTC}. Exiting...")
    return


if __name__ == "__main__":

    valid_dt = datetime.utcnow()
    if len(sys.argv) > 1:
        valid_dt = datetime.strptime(sys.argv[1], "%Y%m%d%H")

    process(valid_dt)

    exit(0)

import tempfile
import os
import json
import sys
import logging.config
from datetime import datetime, timedelta
import time

import pandas as pd
import requests
import s3fs
import tensorflow as tf
import xarray as xr
import numpy as np
import boto3
from osgeo import gdal

from utils import generate_tiles

s3_fs = s3fs.S3FileSystem(anon=False)
s3 = boto3.resource("s3", region_name="us-east-1")

MSL2_INTERMEDIATE_BUCKET = os.getenv(
    "MSL2_INTERMEDIATE_BUCKET",
    "metstormlive-v2-intermediate-dev")

logging.config.fileConfig("logging.ini", disable_existing_loggers=False)
logger = logging.getLogger(__name__)

config_fname = "merge.json"
epoch_timestamp = datetime.utcnow().timestamp()
with open(config_fname) as f:
    CONFIG = json.load(f)


def assemble_data(all_data):
    ppr_vector = all_data["PPR"].flatten()
    qpf_vector = all_data["QPF"].flatten()
    ghe_vector = all_data["GHE"].flatten()
    typ_vector = all_data["TYP"].flatten()
    z2p_vector = all_data["Z2P"].flatten()
    msk_vector = all_data["MSK"].flatten()

    stacked = np.hstack(
        (
         ppr_vector[:, None],
         qpf_vector[:, None],
         ghe_vector[:, None],
         typ_vector[:, None],
         z2p_vector[:, None],
         msk_vector[:, None],
        )
    )

    df = pd.DataFrame(stacked, columns=["PPR", "QPF", "GHE", "TYP", "Z2P", "MSK"])

    return df


def stage_four():

    remote_url = (
        f"https://mesonet.agron.iastate.edu/archive/data/{zulu_time:%Y}/{zulu_time:%m}/"
        f"{zulu_time:%d}/stage4/ST4.{zulu_time:%Y%m%d%H}.01h.grib"
    )
    config_CC = CONFIG["domains"][0]
    nc_fname = f"/tmp/ST4.{zulu_time:%Y%m%d%H}.01h.nc"
    data = requests.get(remote_url)

    st4_input_tmp = "/tmp/st4.grib"

    open(st4_input_tmp, "wb").write(data.content)

    w = config_CC["minx"]
    s = config_CC["miny"]
    e = config_CC["maxx"]
    n = config_CC["maxy"]
    grid_width = config_CC["nx"]
    grid_height = config_CC["ny"]
    srs = config_CC["spatial_reference_sys"]

    warp_options = gdal.WarpOptions(dstSRS=srs,
                                    outputBounds=[w, s, e, n], format="netCDF",
                                    width=grid_width, height=grid_height)
    ds = gdal.Warp(nc_fname, st4_input_tmp, options=warp_options)
    ds = None
    os.remove(st4_input_tmp)
    ds = xr.load_dataset(nc_fname)
    ds.Band1.values = np.nan_to_num(ds.Band1.values)
    ds = ds.rename_vars({"Band1": "Stage4Hourly"})
    return {"ST4": ds["Stage4Hourly"].values}


def merge_CC(zulu_time, build_tiles=True):
    start_time = time.time()

    all_data = {}

    config_CC = CONFIG["domains"][0]
    grid_width = config_CC["nx"]
    grid_height = config_CC["ny"]

    # Read TYP data from s3
    ds_name = "TYP"
    s3_remote_key = (
        f"{MSL2_INTERMEDIATE_BUCKET}/preprocessed/"
        f"{zulu_time:%Y%m%d_%H}/CC_{ds_name}_{zulu_time:%Y%m%d_%H}00_001hr.nc"
    )
    s3_fs.invalidate_cache()

    try:
        with s3_fs.open(s3_remote_key) as f:
            typ = xr.open_dataset(f, chunks="auto")
            logger.info(f"Loaded: {s3_remote_key}")
            all_data.update({ds_name: typ["qpftype"].values})
    except FileNotFoundError:
        logger.info(f"Not Found: {s3_remote_key}")
        typ_empty = np.empty((grid_height, grid_width))
        typ_empty[:] = np.nan
        all_data.update({ds_name: typ_empty})

    # Read QPF data from s3
    ds_name = "QPF"
    s3_remote_key = (
        f"{MSL2_INTERMEDIATE_BUCKET}/preprocessed/"
        f"{zulu_time:%Y%m%d_%H}/CC_{ds_name}_{zulu_time:%Y%m%d_%H}00_001hr.nc"
    )
    with tempfile.NamedTemporaryFile() as s3_tmpfile:
        try:
            s3_fs.get_file(s3_remote_key, s3_tmpfile.name)
            logger.info(f"Loaded: {s3_remote_key}")
            qpf = xr.open_dataset(s3_tmpfile.name, engine="h5netcdf")
            all_data.update({ds_name: qpf["qpf60min"].values})
        except FileNotFoundError:
            logger.info(f"Not Found: {s3_remote_key}")
            qpf_empty = np.empty((grid_height, grid_width))
            qpf_empty[:] = np.nan
            all_data.update({ds_name: qpf_empty})

    # Read GHE data from s3
    ds_name = "GHE"
    s3_remote_key = (
        f"{MSL2_INTERMEDIATE_BUCKET}/preprocessed/"
        f"{zulu_time:%Y%m%d_%H}/CC_{ds_name}_{zulu_time:%Y%m%d_%H}00_001hr.nc"
    )
    with tempfile.NamedTemporaryFile() as s3_tmpfile:
        try:
            s3_fs.get_file(s3_remote_key, s3_tmpfile.name)
            logger.info(f"Loaded: {s3_remote_key}")
            ghe = xr.load_dataset(s3_tmpfile.name, engine="h5netcdf")
            all_data.update({ds_name: ghe["ghe60min"].values})
        except FileNotFoundError:
            logger.info(f"Not Found: {s3_remote_key}")
            ghe_empty = np.empty((grid_height, grid_width))
            ghe_empty[:] = np.nan
            all_data.update({ds_name: ghe_empty})

    # Read PPR data from s3
    ds_name = "PPR"
    s3_remote_key = (
        f"{MSL2_INTERMEDIATE_BUCKET}/preprocessed/"
        f"{zulu_time:%Y%m%d_%H}/CC_{ds_name}_{zulu_time:%Y%m%d_%H}00_001hr.nc"
    )
    with tempfile.NamedTemporaryFile() as s3_tmpfile:
        try:
            s3_fs.get_file(s3_remote_key, s3_tmpfile.name)
            ppr = xr.open_dataset(s3_tmpfile.name, engine="h5netcdf")
            logger.info(f"Loaded: {s3_remote_key}")
            all_data.update({ds_name: ppr["HourlyPolarimetricPrecipAccum"].values})
        except FileNotFoundError:
            logger.info(f"Not Found: {s3_remote_key}")
            ppr_empty = np.empty((grid_height, grid_width))
            ppr_empty[:] = np.nan
            all_data.update({ds_name: ppr_empty})

    # Read Z2P data from s3
    ds_name = "Z2P"
    s3_remote_key = (
        f"{MSL2_INTERMEDIATE_BUCKET}/preprocessed/"
        f"{zulu_time:%Y%m%d_%H}/CC_{ds_name}_{zulu_time:%Y%m%d_%H}00_001hr.nc"
    )
    with tempfile.NamedTemporaryFile() as s3_tmpfile:
        try:
            s3_fs.get_file(s3_remote_key, s3_tmpfile.name)
            z2p = xr.open_dataset(s3_tmpfile.name, engine="h5netcdf")
            logger.info(f"Loaded: {s3_remote_key}")
            pad_z2p = np.pad(z2p["Precipitation_from_Lowalt"].values,
                             ((0, 0), (0, 10)), "constant", constant_values=np.nan)
            all_data.update({ds_name: pad_z2p[:-1, :]})
        except FileNotFoundError:
            logger.info(f"Not Found: {s3_remote_key}")
            z2p_empty = np.empty((grid_height, grid_width))
            z2p_empty[:] = np.nan
            all_data.update({ds_name: z2p_empty})

    # Read MSK data from s3
    ds_name = "MSK"
    s3_remote_key = (
        f"{MSL2_INTERMEDIATE_BUCKET}/preprocessed/"
        f"{zulu_time:%Y%m%d_%H}/CC_{ds_name}_{zulu_time:%Y%m%d_%H}00_001hr.nc"
    )
    with tempfile.NamedTemporaryFile() as s3_tmpfile:
        try:
            s3_fs.get_file(s3_remote_key, s3_tmpfile.name)
            msk = xr.open_dataset(s3_tmpfile.name, engine="h5netcdf")
            logger.info(f"Loaded: {s3_remote_key}")
            all_data.update({ds_name: msk["probprecip60min"][:-1, :-1].values})
        except FileNotFoundError:
            msk_empty = np.empty((grid_height, grid_width))
            msk_empty[:] = np.nan
            all_data.update({ds_name: msk_empty})

    predict_dataset_all = assemble_data(all_data)
    logger.info(predict_dataset_all)

    predict_dataset = predict_dataset_all.loc[~(predict_dataset_all["QPF"] == 0)]

    logger.info(predict_dataset.describe().transpose()[["mean", "std"]])

    dnn_model = tf.keras.models.load_model("dnn_regression_v1.10")

    predictions = dnn_model.predict(predict_dataset).flatten()

    predict_dataset_all["Predicted_QPE"] = predict_dataset_all["QPF"].copy()
    predict_dataset_all.loc[(predict_dataset_all["QPF"] == 0), "Predicted_QPE"] = 0
    predict_dataset_all.loc[~(predict_dataset_all["QPF"] == 0), "Predicted_QPE"] = predictions

    ghe["ghe60min"].values = predict_dataset_all["Predicted_QPE"].to_numpy().reshape(
        (grid_height, grid_width)
    )
    ghe.attrs["title"] = "DNN_QPE_Prediction"

    ghe = ghe.rename_vars({"ghe60min": "DNN_QPE_Prediction"})

    comp = dict(zlib=True, complevel=9)
    encoding = {var: comp for var in ghe.data_vars}

    with tempfile.NamedTemporaryFile() as nc_tmpfile:
        dnn_filename = f"CC_DNN_{zulu_time:%Y%m%d_%H}00_001hr.nc"
        ghe.to_netcdf(nc_tmpfile.name, encoding=encoding)
        bucket_obj = s3.Bucket(MSL2_INTERMEDIATE_BUCKET)
        dnn_object_key = f"postprocessed/{zulu_time:%Y%m%d_%H}/{dnn_filename}"
        bucket_obj.upload_file(Filename=nc_tmpfile.name, Key=dnn_object_key)
        logger.info(f"PostprocessUpload: {dnn_filename}")
        tiles_prefix = f"tiles/{zulu_time:%Y%m%d_%H}"
        if build_tiles:
            dnn_tilesname = dnn_filename.replace(".nc", ".mbtiles")
            mbtiles = generate_tiles(nc_tmpfile.name,
                                     tiles_name=dnn_tilesname,
                                     nc_dataset_name="DNN_QPE_Prediction")
            bucket_obj.upload_file(Filename=mbtiles, Key=f"{tiles_prefix}/{dnn_tilesname}")
            logger.info(f"TilesUpload: {dnn_tilesname}")
    execution_time = (time.time() - start_time)
    logger.info(f"Merging process completed in: {execution_time:.2f} seconds")


if __name__ == "__main__":
    if len(sys.argv) > 1:
        zulu_time = datetime.strptime(sys.argv[1], "%Y%m%dT%H")
    else:
        zulu_time = datetime.utcnow() - timedelta(hours=1)
    merge_CC(zulu_time)
    exit(0)

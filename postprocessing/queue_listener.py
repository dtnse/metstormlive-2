import os
import json
import time
import gc
import glob
import logging.config
from pathlib import Path

import boto3
import s3fs

import ghe
import qpf
import ptype
import lowalt
import persiann

logging.config.fileConfig('logging.ini', disable_existing_loggers=False)
logger = logging.getLogger(__name__)


class MSL2QueueListener(object):
    """
    Class that listens to MSL2 file ingest queue for messages,
    and processes accordingly

    """

    def __init__(self, init_sqs=True) -> None:
        self.attempts = 0
        self.pause_between_receives_seconds = os.getenv(
            "PAUSE_BETWEEN_RECEIVES_SECONDS",
            1
        )
        self.sqs_name = os.getenv(
            'SQS_NAME',
            'metstormlive2-file-queue-filtered'
        )
        self.aws_account_id = os.getenv('AWS_ACCOUNT_ID', '688714617473')
        self.aws_region = os.getenv('AWS_REGION', 'us-east-1')
        self.role_arn = os.getenv(
            'ROLE_ARN',
            'arn:aws:iam::688714617473:role/metstormlive2-s3-read-write-sqs-all'
        )
        self.MSL2_INTERMEDIATE_BUCKET = os.getenv(
            'MSL2_INTERMEDIATE_BUCKET',
            'metstormlive-v2-intermediate-dev'
        )
        self.env = os.getenv('ENV', 'dev')

        # if running in AWS EKS, permissions will be assigned to the
        # pods this will be running on
        self.sqs_client = boto3.client('sqs', region_name=self.aws_region)
        self.s3 = boto3.resource('s3', region_name=self.aws_region)

        self.s3_fs = s3fs.S3FileSystem(anon=False)

        config_fname = os.getcwd().replace("postprocessing", "msl2.json")

        with open(config_fname) as f:
            self.config = json.load(f)

        if init_sqs:
            response = self.sqs_client.get_queue_url(
                QueueName=self.sqs_name,
                QueueOwnerAWSAccountId=self.aws_account_id
            )
            self.queue_url = response.get('QueueUrl')
        else:
            self.queue_url = None

    def _delete_from_queue(self, object_key, receipt):
        self.sqs_client.delete_message(
            QueueUrl=self.queue_url,
            ReceiptHandle=receipt
        )
        logging.info(f"...Deleted {object_key} from queue")

    def _parse_s3_notification(self, response):
        """
        Parse standard S3 notification message
        These messages have a subject line of:
        'Amazon S3 Notification'
        """
        messages = response.get("Messages")
        if messages:
            message = messages[0]
            message_body = message["Body"]
            receipt = message["ReceiptHandle"]
            message_json = json.loads(message_body)
            records_json = json.loads(message_json["Message"])
            record = records_json["Records"][0]
            object_key = record["s3"]["object"]["key"]
            bucket = record["s3"]["bucket"]["name"]
            return bucket, object_key, receipt

        return None, None, None

    def _parse_wdtfam(self, response):
        """
        Parse SNS notification message
        that follow the WDT FAM (File Availability Message)
        format

        """
        messages = response.get("Messages")
        if messages:
            message = messages[0]
            message_body = message["Body"]
            receipt = message["ReceiptHandle"]
            message_json = json.loads(message_body)
            records_json = json.loads(message_json["Message"])
            uri = records_json["uri"].split("s3://")[1]
            uri_split = uri.split("/")
            object_key = "/" + "/".join(uri_split[1:])
            bucket = uri_split[0]
            return bucket, object_key, receipt

        return None, None, None

    def start_listening(self):
        """
        Listen indefinitely to queue

        """

        while True:

            # for k8s health check
            Path('/tmp/healthy').touch()

            self.attempts += 1
            processed = False
            logger.debug(f"Receive attempt #{self.attempts}")

            try:
                response = self.sqs_client.receive_message(
                    QueueUrl=self.queue_url, MaxNumberOfMessages=1,
                    WaitTimeSeconds=self.pause_between_receives_seconds,
                    AttributeNames=['All']
                )
                stringified_response = json.dumps(response)

                if "sat-gridded-dtn" in stringified_response:
                    bucket, object_key, receipt = self._parse_s3_notification(response)
                    top_of_hour = object_key.split("/")[-1].split("-")[2][2:4] == "00"
                    if top_of_hour and "15min" in object_key:
                        logger.info(f"...Processing: {object_key} from {bucket}")
                        processed = ghe.process(
                                self,
                                bucket,
                                object_key
                        )
                    else:
                        self._delete_from_queue(object_key, receipt)

                elif "nwp-dtn-1fx" in stringified_response:
                    bucket, object_key, receipt = self._parse_s3_notification(response)
                    logger.info(f"...Processing: {object_key} from {bucket}")
                    if "precipitation_rate_mean_over_1h" in object_key:
                        processed = qpf.process(self, bucket, object_key)
                    elif "precipitation_type" in object_key:
                        processed = ptype.process(self, bucket, object_key)
                    else:
                        self._delete_from_queue(object_key, receipt)

                elif "wdtfam-radar-na" in stringified_response:
                    bucket, object_key, receipt = self._parse_wdtfam(response)
                    logger.info(f"...Processing: {object_key} from {bucket}")
                    if "MaskedLowaltReflectivity" in object_key:
                        processed = lowalt.process(self, bucket, object_key)

                elif "wdtfam-persiann" in stringified_response:
                    bucket, object_key, receipt = self._parse_wdtfam(response)
                    logger.info(f"...Processing: {object_key} from {bucket}")
                    processed = persiann.process(self, bucket, object_key)

                if processed:
                    self.sqs_client.delete_message(
                        QueueUrl=self.queue_url,
                        ReceiptHandle=receipt
                    )
                    logging.info(f"...Deleted {object_key} from queue")

            except Exception as e:
                logger.error(f"Unknown error processing message: {stringified_response}")
                logger.error(e, exc_info=True)

            # garbage collection
            gc.collect()

            # ensure all tmp files are deleted
            for fname in glob.glob("/tmp/*.nc"):
                os.remove(fname)

            time.sleep(self.pause_between_receives_seconds)


if __name__ == "__main__":
    queue = MSL2QueueListener()
    queue.start_listening()

# README

This repo contains the code for MetStormLive2 infrastructure and algorithm

## Prerequisites: 

Install Conda:

https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html

## How do I get set up?

`conda env create -f requirements/env.yaml` or `mamba env create -f requirements/env.yaml`
`conda activate metstormlive2-preprocessing` then
`cd preprocessing; python run_tests.py` (Default, runs all tests for data from 3 hours ago)
or specify which dataset to test and a time `python run_tests.py ppr_accum 20220317T12`


## Configuration
JSON config, including grid specs is stored under preprocessing/msl2.json.
merging config is located at postprocessing/merge.json
The above is now deprecated and one json file (msl2.json) in the top directory is used

## Dependencies
Right now, a conda env.yaml file contains the enviromment for deploying on a miniconda3 container
and also on JupyterHub in development.


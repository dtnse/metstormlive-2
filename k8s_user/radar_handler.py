#!/usr/bin/env python3.8
# coding: utf-8
#
# DTN, LLC - METSTORM Live2
# PREPROCESSING HANDLER
#   ICON & Remote Sensing Team (Oct, 2022)
######################################################################################
######################################################################################
import os
import time
from optparse import OptionParser
import logging.config
import traceback
from datetime import datetime

from msl2_qpe_pkg.base import aws_utils
from msl2_qpe_pkg.preprocessing import ppr
from msl2_qpe_pkg.preprocessing import lowalt_accum
from msl2_qpe_pkg.preprocessing import radar_confidence

######################################################################################

######################################################################################

if __name__ == "__main__":

    usage = ''
    p = OptionParser(usage=usage)
    p.add_option('--verbose', '-v', dest='verbose', default=False, action='store_true',
        help='increase logging to the DEBUG level.'
        )
    p.add_option('--config_path', help='Path to Ingest Config Files',default='./config',type='string')
    p.add_option('--logterminal', help='Path to configuration file',default=False,action='store_true')

    # =================================================================== 
    # Loading configurations 
    opts, args = p.parse_args()

    logging.config.fileConfig("msl2_qpe_pkg/base/logging.ini", disable_existing_loggers=False)
    opts.logger = logging.getLogger(__name__)

    opts.logger.info("+++++ MSL2: BEGING INPUTs PRE-PROCESSING ++++++ ")

   # =================================================================== 
    # Get environment variables

    opts.ENV = os.getenv("ENV", "dev")

    opts.MSL2_INTERMEDIATE_BUCKET = os.getenv(
        "MSL2_INTERMEDIATE_BUCKET",
        "metstormlive-v2-intermediate-dev")

    # =================================================================== 
    # Initiate AWS clients

    opts.sqs_obj = aws_utils.start_sqs_clients(opts,env_var="MSL2_PRODUCT_QUEUE")

    opts.s3_fs   = aws_utils.init_s3FileSystem(anon=False)
    opts.s3      = aws_utils.init_boto3S3Client()

    # =================================================================== 
    queue_obj = aws_utils.MSL2QueueListener(opts)

    # =================================================================== 
    #PPR ACUM JOB
    try:
        opts.logger.info(" - Start PPR ACUM JOB")
        now_dt = datetime.utcnow()
        ppr.accum(opts,queue_obj, valid_dt=now_dt)
    except:
        print(traceback.format_exc())
        opts.logger.error(" FAILED on PPR ACUM job")

    # =================================================================== 
    #LOWALT ACUM JOB
    try:
        opts.logger.info(" - Start LOWALT ACUM JOB")        
        attempts = 3
        for a in range(1, attempts+1):
            try:
                now_dt = datetime.utcnow()
                lowalt_accum.accum(opts,queue_obj, valid_dt=now_dt)
                break
            except Exception as e:
                opts.logger.error(f" accumLowalt: [ Attempt {a} of {attempts} ] Failed to process"
                            f" {now_dt:%Y-%m-%d %H}:00 UTC with error: {e}")
                time.sleep(3)
    except:
        print(traceback.format_exc())
        opts.logger.error(" FAILED on LOWALT ACUM job")

    # =================================================================== 
    # RADAR CONFIDENCE JOB
    try:
        opts.logger.info(" - Start RADAR CONFIDENCE JOB")
        now_dt = datetime.utcnow()
        radar_confidence.process(opts,queue_obj, valid_dt=now_dt)
    except:
        print(traceback.format_exc())
        opts.logger.error(" FAILED on RADAR CONFIDENCE job")
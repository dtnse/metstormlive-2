#!/usr/bin/env python3.8
# coding: utf-8
#
# DTN, LLC - METSTORM Live2
# PREPROCESSING HANDLER
#   ICON & Remote Sensing Team (Oct, 2022)
######################################################################################
######################################################################################
import gc
import os
import glob
from pathlib import Path
import time
import json

from optparse import OptionParser
import logging.config

from msl2_qpe_pkg.base import aws_utils
from msl2_qpe_pkg.preprocessing import ghe
from msl2_qpe_pkg.preprocessing import qpf
from msl2_qpe_pkg.preprocessing import ptype
from msl2_qpe_pkg.preprocessing import pmask
from msl2_qpe_pkg.preprocessing import lowalt
from msl2_qpe_pkg.preprocessing import persiann

######################################################################################

def start_listening(queue_listener,opts):
    """
    Listen indefinitely to queue

    """

    while True:

        queue_listener.attempts += 1
        processed = False
        opts.logger.debug(f"Receive attempt #{queue_listener.attempts}")

        try:
            response = queue_listener.sqs_client.receive_message(
                QueueUrl=queue_listener.queue_url, MaxNumberOfMessages=1,
                WaitTimeSeconds=queue_listener.pause_between_receives_seconds,
                AttributeNames=['All']
            )
            stringified_response = json.dumps(response)

            if "sat-gridded-dtn" in stringified_response:
                bucket, object_key, receipt = queue_listener._parse_s3_notification(response)

                if "global/ghe_15min" in object_key:
                    top_of_hour = object_key.split("/")[-1].split("-")[2][2:4] == "00"
                    if top_of_hour and "complete" not in object_key:
                        opts.logger.info(f"...Processing: {object_key} from {bucket}")
                        processed = ghe.process(
                                queue_listener,
                                opts,
                                bucket,
                                object_key
                        )
                    else:
                        queue_listener._delete_from_queue(opts,object_key, receipt)
                if "global/persiann" in object_key:
                    opts.logger.info(f"...Processing: {object_key} from {bucket}")
                    processed = persiann.process(queue_listener, opts,bucket, object_key)
                if "global/precip-mask" in object_key:
                    opts.logger.info(f"...Processing: {object_key} from {bucket}")                    
                    processed = pmask.process(queue_listener, opts, bucket, object_key)

            elif "nwp-dtn-1fx" in stringified_response:
                bucket, object_key, receipt = queue_listener._parse_s3_notification(response)
                opts.logger.info(f"...Processing: {object_key} from {bucket}")            
                if "precipitation_rate_mean_over_1h" in object_key \
                        and "convective" not in object_key:
                    processed = qpf.process(queue_listener, opts,bucket, object_key)
                if "precipitation_type" in object_key:
                    processed = ptype.process(queue_listener, opts,bucket, object_key)
                else:
                    queue_listener._delete_from_queue(opts,object_key, receipt)

            elif "radar-gridded-dtn" in stringified_response:
                bucket, object_key, receipt = queue_listener._parse_s3_notification(response)
                opts.logger.info(f"...Processing: {object_key} from {bucket}")                
                # NOTE: metstormlive-file-queue-* lambda function prefilters messages,
                # so no need for additional filtering here!
                processed = lowalt.process(queue_listener, opts,bucket, object_key)

            if processed:
                queue_listener._delete_from_queue(opts,object_key, receipt)

        except Exception as e:
            opts.logger.error(f"Unknown error processing message: {object_key} from {bucket}")
            opts.logger.error(e, exc_info=True)

        # garbage collection
        gc.collect()

        # ensure all tmp files are deleted
        for fname in glob.glob("/tmp/*.nc"):
            os.remove(fname)

        time.sleep(queue_listener.pause_between_receives_seconds)

######################################################################################

if __name__ == "__main__":

    usage = ''
    p = OptionParser(usage=usage)
    p.add_option('--verbose', '-v', dest='verbose', default=False, action='store_true',
        help='increase logging to the DEBUG level.'
        )
    p.add_option('--config_path', help='Path to Ingest Config Files',default='./config',type='string')
    p.add_option('--logterminal', help='Path to configuration file',default=False,action='store_true')

    # for k8s health check
    Path('/tmp/healthy').touch()
    # =================================================================== 
    # Loading configurations 
    opts, args = p.parse_args()

    logging.config.fileConfig("msl2_qpe_pkg/base/logging.ini", disable_existing_loggers=False)
    opts.logger = logging.getLogger(__name__)

    opts.logger.info("+++++ MSL2: BEGING INPUTs PRE-PROCESSING ++++++ ")

   # =================================================================== 
    # Get environment variables

    opts.ENV = os.getenv("ENV", "dev")

    opts.MSL2_INTERMEDIATE_BUCKET = os.getenv(
        "MSL2_INTERMEDIATE_BUCKET",
        "metstormlive-v2-intermediate-dev")

    # =================================================================== 
    # Initiate AWS clients

    opts.sqs_obj = aws_utils.start_sqs_clients(opts,env_var="MSL2_PRODUCT_QUEUE")

    opts.s3_fs   = aws_utils.init_s3FileSystem(anon=False)
    opts.s3      = aws_utils.init_boto3S3Client()

    # =================================================================== 
    
    queue_obj = aws_utils.MSL2QueueListener(opts)
    start_listening(queue_obj,opts)

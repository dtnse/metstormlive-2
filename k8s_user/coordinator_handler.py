#!/usr/bin/env python3.8
# coding: utf-8
#
# DTN, LLC - METSTORM Live2
# MSL2 - JOBS COORDINATOR 
#   ICON & Remote Sensing Team (DEC, 2022)
######################################################################################
######################################################################################
from optparse import OptionParser
import os
from pathlib import Path
import logging.config
import time
import traceback
import json

#from kubernetes import client
from msl2_qpe_pkg.base import aws_utils
from msl2_qpe_pkg.base import k8s as base_k8s
from msl2_qpe_pkg.base import utils as base_utils
from msl2_qpe_pkg.base import grid as base_grid
from msl2_qpe_pkg.base import message as base_message

from kubernetes import client as k8s_client
from kubernetes import config as k8s_config

######################################################################################
######################################################################################

def compose_pod_config(pods_default_config,sec_config):
    
    # grabs the default INGEST or PRODUCT app config first
    default_config = pods_default_config["defaults"]
    pod_config = default_config.copy()
    
    # OVERWRITEs pod config based on type of ingest or product, if defined
    sec_type = sec_config["type"].split("-")[0]

    if sec_type in pods_default_config:
        for cfg_key in ["Limits","Requests"]:
            for req_key in pods_default_config[sec_type][cfg_key]:
                pod_config[cfg_key][req_key] = \
                     pods_default_config[sec_type][cfg_key][req_key]

    # OVERWRITEs pod config with sector-specfic pod config, if defined
    if "pod_config" in sec_config:
        for cfg_key in ["Limits","Requests"]:
            for req_key in sec_config["pod_config"][cfg_key]:
                pod_config[cfg_key][req_key] = \
                     sec_config["pod_config"][cfg_key][req_key]

    return pod_config

def get_pods_config(opts,app_id,project_id="MSL2"):

    general_config = {   
        "namespace"      : os.getenv("{}_NAMESPACE".format(project_id)),
        "service_account": os.getenv("{}_SERVICE_ACCOUNT".format(project_id)),
        "image_id"       : os.getenv("{}_MAIN_IMAGE".format(project_id)),
        "pod_restart_policy" : "OnFailure",
        "secret_volume" : "aws-iam-token"
    }

    pods_default_config = base_utils.load_yaml_config("{}/k8s_pods_config.yaml".format(
            opts.config_path
        )
    )

    if app_id in pods_default_config:
        pod_config = pods_default_config[app_id]

    else:
        pod_config = pods_default_config["default"]        
    
    return general_config, pod_config

######################################################################################

def start_stitch_job(opts,domain_id,valid_time,n_slices):

    app_id = "stitch"

    msl2_global_config  = base_utils.load_json_config(opts)

    opts.logger.info(f"  -> STARTING STITCH JOB for {domain_id} at {valid_time}")

    custom_vars = {
        "MSL2APP" : app_id.upper(),
        "MSL2_DOMAIN_ID": domain_id,
        "MSL2_RUN_TIME": valid_time,
        "MSL2_NUM_SLICES": n_slices
    }

    trigger_job(
        opts,app_id,
        envvars_dict=custom_vars,
        job_suffix=f"--{domain_id}".lower(),
        custom_msg=f"<< {domain_id} >>"
        )

def start_merge_jobs(opts):

    app_id = "mergejobtest"

    msl2_global_config  = base_utils.load_json_config(opts)

    #for domain_id in msl2_global_config["domains"]:
    for domain_id in ['CC']:

        opts.logger.info(f" -> STARTING {domain_id} JOBS")
        domain_config       = base_utils.load_domain_config(opts,domain_id)

        subdomains = base_grid.get_subdomain_bounds(domain_config)

        n_slices = len(subdomains)
        if n_slices>1:
            is_fulldomain = "False"
        else:
            is_fulldomain = "True"

        for tile_n in subdomains:
            
            tile_id = "TILE-{:03g}".format(tile_n)
            custom_vars = {
                "MSL2APP" : app_id.upper(),
                "MSL2_DOMAIN_ID": domain_id,
                "IS_FULLDOMAIN": is_fulldomain,
                "TILE_ID": tile_id,
                "N_SLICES": "{:03g}".format(n_slices),
                "MINX" : "{:.2f}".format(subdomains[tile_n]["minx"]),
                "MAXX" : "{:.2f}".format(subdomains[tile_n]["maxx"]),
                "MINY" : "{:.2f}".format(subdomains[tile_n]["miny"]),
                "MAXY" : "{:.2f}".format(subdomains[tile_n]["maxy"])
            }

            trigger_job(
                opts,app_id,
                envvars_dict=custom_vars,
                job_suffix=f"--{domain_id}-{tile_id}".lower(),
                custom_msg=f"<< {domain_id} {tile_id} >>"
                )
            # With the dummy jobs, put in a sleep so they don't all finish at the same time
            time.sleep(5)

def trigger_job(
    opts,
    app_id,
    job_suffix="",
    envvars_dict = None,
    custom_msg = ""
    ):

    # first loads service configs
    general_config, pod_config = get_pods_config(opts,app_id)

    # Kubernetes instance
    k8s = base_k8s.Kubernetes()

    # STEP1: CREATE A CONTAINER
    _image = general_config["image_id"]
    _name  = pod_config["app_name"]

    _volumes = []
    # _efs_volume    = k8s.claim_volumes(volume_name=general_config["volume_name"])
    # _volumes.append(_efs_volume)

    _env_vars  = k8s.gather_env_vars("MSL2",other_vars=envvars_dict)

    _container = k8s.create_container(
        _image, _name, _env_vars,pod_config,
        entrypoint_cmd="/usr/local/bin/docker-entrypoint.sh"
        )

    # STEP2: CREATE A POD TEMPLATE SPEC
    _pod_spec, job_id = k8s.create_pod_template(
        "{}{}".format(pod_config["app_name"],job_suffix),
        _container,
        _volumes,general_config["service_account"],
        general_config["pod_restart_policy"]
        )
        # job_class="background-jobs"    

    # STEP3: CREATE A JOB
    _job = k8s.create_job(
        "{}{}".format(pod_config["app_name"],job_suffix), 
        _pod_spec,job_id
        )

    # STEP4: EXECUTE THE JOB
    batch_api = k8s_client.BatchV1Api()
    batch_api.create_namespaced_job(general_config["namespace"], _job) 
    opts.logger.info(" - JOB sent {} - {}| POD cpu={} | mem={} | disk={}".format(
        app_id,
        custom_msg,
        pod_config["Limits"]["cpu"],
        pod_config["Limits"]["memory"],
        pod_config["Requests"]["ephemeral-storage"]
        ))

def check_complete_tileset(opts,urlget,n_slices):

    # Assumes we'll do ECM and QPE in one job 
    # once both sets of tiles are available
    url_pieces = urlget.split('/')
    valid_time = url_pieces[4]
    domain = url_pieces[5][0:2]
    tile_id = url_pieces[5].split('_')[0]
    prod_in = url_pieces[5].split('_')[1]
    for tilenum in range(1,int(n_slices)+1):
        # Only have ECM now in the dummy data phase
        #for product in ['ECM', 'QPE']:
        for prod in ['ECM']:
            new_key = '/'.join(url_pieces[-3:])\
                         .replace(tile_id,f"{domain}-TILE-{tilenum:03g}")\
                         .replace(prod_in,prod)
            obj = opts.s3.Object(opts.MSL2_INTERMEDIATE_BUCKET,new_key)
            # If we can load its properties, it exists
            try:
                obj.load()
            except:
                opts.logger.info(f"{new_key} doesn't exist yet, no stitching")
                return False, domain, valid_time

    return True, domain, valid_time

######################################################################################
######################################################################################

if __name__ == "__main__":
    usage = ''
    p = OptionParser(usage=usage)
    p.add_option('--verbose', '-v', dest='verbose', default=False, action='store_true',
        help='increase logging to the DEBUG level.'
        )
    p.add_option('--config_path', help='Path to Ingest Config Files',default='./config',type='string')
    p.add_option('--logterminal', help='Path to configuration file',default=False,action='store_true')

    # =================================================================== 
    # Loading configurations 
    opts, args = p.parse_args()

    logging.config.fileConfig("msl2_qpe_pkg/base/logging.ini", disable_existing_loggers=False)
    opts.logger = logging.getLogger(__name__)
    opts.logger.info("+++++ MSL2: BEGIN COORDINATOR POD ++++++ ")

    # for k8s health check
    Path('/tmp/healthy').touch()    

    # =================================================================== 
    # Get environment variables

    opts.ENV = os.getenv("ENV", "dev")

    opts.MSL2_INTERMEDIATE_BUCKET = os.getenv(
        "MSL2_INTERMEDIATE_BUCKET",
        "metstormlive-v2-intermediate-dev")

    # =================================================================== 
    # Initiate AWS clients

    opts.sqs_obj = aws_utils.start_sqs_clients(opts,env_var="MSL2_COORDINATOR_QUEUE")

    opts.s3_fs   = aws_utils.init_s3FileSystem(anon=False)
    opts.s3      = aws_utils.init_boto3S3Client()

    #===================================================================================

    # k8s_config.load_kube_config()
    k8s_config.load_incluster_config()
    #===================================================================================
    #listen to queue ------------------------------------------
    while True:
        message = False
        opts.logger.info('... listening for messages')        
        try:
            response = opts.sqs_obj.sqs_client.receive_message(
                QueueUrl=opts.sqs_obj.queue_url, MaxNumberOfMessages=1, 
                WaitTimeSeconds=20
                )
            message = response.get('Messages', [{}])[0]
        except:
            opts.logger.error('... error retrieving messages from queue {0}'.format(opts.sqs_obj.queue_url))
            print(traceback.format_exc())
            message = False
        # message = True

        if message:
            try:
                message_id, urlget, message_type, downstream_user, n_tries = base_message.parse_message(
                     message,opts.sqs_obj.queue_url,opts.sqs_obj.sqs_client
                     )
                if "SUBDOMAIN" in message_type:
                    all_tiles_done,domain_id,valid_time = check_complete_tileset(opts,urlget,n_tries)
                    if all_tiles_done:
                       start_stitch_job(opts,domain_id,valid_time,n_tries)
            except:
                if "Type" not in json.loads(message['Body']):
                    start_merge_jobs(opts)

            base_message.delete_message(message, opts.sqs_obj.queue_url, opts.sqs_obj.sqs_client)

            #opts.logger.info(" - sleeping")
            #time.sleep(300)
            # try:
            #     message_id, urlget, message_type, downstream_user, n_tries = base_message.parse_message(
            #         message,sqs_obj.queue_url,sqs_obj.sqs_client
            #         )
            #     logger.debug('... checking msg: {0} {1}'.format(
            #         message_type,
            #         message_id)
            #     )
            #     is_match, secs_to_process = base_message.check_trigger_match(config_triggers, urlget,downstream_user)                
            # except:
            #     logger.error('... error parsing message from queue {0}'.format(sqs_obj.queue_url))
            #     print(traceback.format_exc())
            #     is_match = False

            #checks for a match with ingest triggers
            # base_message.delete_message(message, sqs_obj.queue_url, sqs_obj.sqs_client)

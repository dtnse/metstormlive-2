#!/usr/bin/env python3.8
# coding: utf-8
#
# DTN, LLC - METSTORM Live2
# MSL2 - MERGE.py (core code)
#   ICON & Remote Sensing Team (OCt, 2022)
######################################################################################
######################################################################################
from optparse import OptionParser
import os
import sys
import logging.config
from datetime import datetime
import time
from pathlib import Path

import numpy as np

from msl2_qpe_pkg.base import msl2class
from msl2_qpe_pkg.base import aws_utils
from msl2_qpe_pkg.base import output as base_outputs
from msl2_qpe_pkg.base import gauges as gauge_utils
from msl2_qpe_pkg.base import utils as base_utils
from msl2_qpe_pkg.base import message as base_message

######################################################################################
######################################################################################

def merge_main(opts, domain_id="CC"):
    """Diagostic QPE based on the workflow found at:
    https://dtnse1.atlassian.net/wiki/spaces/CCS/pages/33565901012/Proposed+Multi-sensor+source+Integration+Algorithm+for+MetStormLive2+QPE
    """
    # starts run
    start_time = time.time()
    opts.epoch_timestamp = datetime.utcnow().timestamp()    
    opts.logger.info(f" {domain_id} MergeBegan: {opts.zulu_time:%Y%m%d_%H%M%S}")
    opts.domain_id = domain_id

    # loads the domain and global config
    domain_config       = base_utils.load_domain_config(opts,domain_id)
    msl2_global_config  = base_utils.load_json_config(opts)

    # INIT THE MS2 Object
    MSL2obj = msl2class.MSL2classObj(domain_id)

    if not opts.is_fulldomain:
        opts.domain_id                    += "-"+opts.tile_id
        domain_config["abbreviated_name"] += "-"+opts.tile_id
        domain_config["minx"]              = opts.minx
        domain_config["maxx"]              = opts.maxx
        domain_config["miny"]              = opts.miny
        domain_config["maxy"]              = opts.maxy
        domain_config["nx"] = int((opts.maxx - opts.minx)/domain_config["grid_dx_deg"]) +1
        domain_config["ny"] = int((opts.maxy - opts.miny)/domain_config["grid_dy_deg"]) +1
        output_type                        = "subdomain_tiles"
        to_modi                            = False
    else:
        output_type                        = "postprocessed"
        to_modi                            = True

    # After this point (for subdomains):
    # opts.domain_id will have a tile ID, domain_id is the "base" domain ID
    # we have to be careful which one we are using for what
    # domain_config has the correct corners and can be used as-is

    #loading the domain grid
    MSL2obj.init_BASE_grid(domain_config)
    
    # Get the rest of the files and put the data they contain into a dictionary for
    # easy access later
    # loads everything inside the MSL2obj object 
    MSL2obj.list_msl2_inputs_from_s3(
        opts,opts.zulu_time,domain_id,msl2_global_config,domain_config
        )

    # If after checking the last 24 hours we still don't have an RCO file:
    opts.logger.info(f" {domain_id} MergeHaveRCO: {MSL2obj.have_rco}")

    # Initialize merging weights - weights are already adjusted
    opts.logger.info(f" {domain_id} Intializing Merge Weights")
    MSL2obj.get_merge_weights(opts)

    # ------------------------------------------------------------------
    # Calculates FIRST GUESS 
    opts.logger.info(f" {domain_id} Calculating FIRST GUESS QPE")
    MSL2obj.get_QPE_part01(opts,msl2_global_config,domain_config)

    # ===================================================================
    # ===================================================================
    # GETTING CONFIDENCE METRICS

    # Calculate confidence metrics here so that there will
    # still be confidence metrics even if we stop at first guess
    opts.logger.info(f" {domain_id} Calculating Input Variance")
    MSL2obj.get_inputs_variance(
        opts,
        msl2_global_config,
        domain_config
        )
    # -----------------------------------------------------------------------        
    # Climatology Variance
    opts.logger.info(f" {domain_id} Retrieving Clim Variance")
    MSL2obj.add_generic_conf_metric(
        opts,
        domain_config,
        weight_use=msl2_global_config["merge_parameters"]["max_clim_variance"],
        out_var="clim_var",
        filename=f"/basemap/CC_bvr_1981to2010_{opts.zulu_time:%b}.nc",
        var_name="bmap_variance",
        norm=True
        )

    # -----------------------------------------------------------------------
    # GAUGE CONFIDENCE
    key_use = "DEN"
    var_use = "gauge_dens_conf"
    if key_use in MSL2obj.available_inputs:
        opts.logger.info(f"   - retrieving {domain_id}  {key_use} confidence")
        MSL2obj.add_generic_conf_metric(
            opts,
            domain_config,
            weight_use=msl2_global_config["merge_parameters"][var_use],
            out_var=var_use,
            var_name=msl2_global_config["dataset_names"][key_use],
            dict_key=key_use
            )
    else:
        MSL2obj.confidence_metrics[var_use] = \
            (("lat","lon"), np.zeros(MSL2obj.base_grid.new_2d_lon.values.shape))

    # -----------------------------------------------------------------------
    # BEAM BLOCKAGE 
    # Beam block mask 0 = no blockage; 1 = totally blocked ####


    blockage_prefix = "/default/{}_BLK_latest.nc".format(domain_id)
    opts.logger.info(f"   - loading {domain_id} BEAM BLK")    
    blockage_key = opts.MSL2_INTERMEDIATE_BUCKET + blockage_prefix
    
    blockage = msl2class.load_intermediate_msl2_inputs(
                opts,
                blockage_key,
                "blockagemap",
                domain_config
            )

    # -----------------------------------------------------------------------
    # LOOPING SAT AND MODEL CONFIDENCE
    key_vars = ["GHE", "PER", "QPF","PPR","Z2P"]
    out_vars = [
        "sat_ghe_conf", "sat_per_conf","mod_qpf_conf",
        "radar_ppr_conf","radar_z2p_conf"
        ]
    cos_lat = np.cos(MSL2obj.base_grid.new_2d_lat.values * np.pi/180)

    for key_use, var_use in zip(key_vars,out_vars):
        if key_use in MSL2obj.available_inputs:
            opts.logger.info(f"   - retrieving {domain_id}  {key_use} confidence")

            weight_use=msl2_global_config["merge_parameters"][var_use]
            mult_fac = 1
            if (key_use =="PPR") | (key_use=="Z2P"):
                mult_fac = MSL2obj.base_rco * \
                            (1 - blockage)
            elif (key_use =="GHE") | (key_use=="PER"):
                mult_fac = cos_lat

            MSL2obj.confidence_metrics[var_use] = \
                (("lat","lon"),
                np.ones(
                    MSL2obj.base_grid.new_2d_lon.values.shape
                    )*weight_use*mult_fac)

        else:
            MSL2obj.confidence_metrics[var_use] = \
                (("lat","lon"), np.zeros(MSL2obj.base_grid.new_2d_lon.values.shape))

    del(blockage)
    # -----------------------------------------------------------------------    
    # SATELLITE PRECIP-MASK CONFIDENCE
    key_use = "MSK"
    var_use = "sat_msk_conf"

    if key_use in MSL2obj.available_inputs:
        opts.logger.info(f"   - retrieving {domain_id}  {key_use} confidence")
        weight_use=msl2_global_config["merge_parameters"][var_use]
        conf_msk = msl2class.load_intermediate_msl2_inputs(
                opts,
                MSL2obj.data_dict[key_use],
                msl2_global_config["dataset_names"][key_use],
                domain_config
                )
        conf_msk = abs(conf_msk - 0.5) * 2.5 * weight_use
        conf_msk[conf_msk > weight_use] = weight_use
        MSL2obj.confidence_metrics[var_use] = \
            (("lat","lon"), np.nan_to_num(conf_msk))
        del(conf_msk)
    else:
        MSL2obj.confidence_metrics[var_use] = \
            (("lat","lon"), np.zeros(MSL2obj.base_grid.new_2d_lon.values.shape))

    # -----------------------------------------------------------------------
    # PRECIP TYPE CONF 
    key_use = "TYP"
    var_use = "prec_typ_conf"    
    if 'TYP' in MSL2obj.available_inputs:

        prec_typ = msl2class.load_intermediate_msl2_inputs(
                opts,
                MSL2obj.data_dict[key_use],
                msl2_global_config["dataset_names"][key_use],
                domain_config
                )
        typ_frz = np.where((prec_typ == 1) |
                           (prec_typ == 5) |
                           (prec_typ == 3) )

        conf_typ = np.ones(prec_typ.shape)
        conf_typ[typ_frz] = 0
        conf_typ = conf_typ * msl2_global_config["merge_parameters"][var_use]
        MSL2obj.confidence_metrics[var_use] = \
            (("lat","lon"), np.nan_to_num(conf_typ))
        
        del(conf_typ,typ_frz,prec_typ)

    #=========================================================================
    # ECM FINAL CALCULATION
    opts.logger.info(f"{domain_id} Writing Confidence Metric ECM Output")
    ecm_sum = MSL2obj.confidence_metrics.to_array().sum("variable").values

    max_conf_sum = 0
    for key_conf in msl2_global_config["merge_parameters"]:
        max_conf_sum += msl2_global_config["merge_parameters"][key_conf]

    ecm_sum = ecm_sum/max_conf_sum

    MSL2obj.confidence_metrics["ECM"] = \
        (("lat","lon"), np.nan_to_num(ecm_sum))
    
    del(ecm_sum)

    output_cfg = {
        "title" : "QPE Confidence Array",
        "long_name": "QPE Confidence Array",
        "units" : "dimensionless [0 - 1]"        
    }

    s3url = base_outputs.write_output_netcdf(
        opts, 
        MSL2obj.confidence_metrics["ECM"],
        opts.zulu_time,
        msl2_global_config,
        output_cfg,
        array_type="single",
        out_var="qpe_conf",
        output_type="ECM",
        domain_id = domain_id,
        MISSING_VALUE = -9999.0,
        to_modi=to_modi,
        proc_stage = output_type
    )        
    #send SNS message
    message_body = base_message.compose_message("MERGE_HANDLER",s3url)
    is_msg       = base_message.send_message(opts.sqs_obj,message_body)
    if is_msg:
        opts.logger.info(f"{domain_id} ECM message SENT")


    del(msl2class.MSL2classObj.confidence_metrics)

    #=========================================================================
    # Apply the satellite MSK here
    opts.logger.info(f"{domain_id} Applying Satellite Mask to First Guess")

    MSL2obj.apply_satellite_mask(opts,msl2_global_config,domain_config)
    MSL2obj.add_qpe_attributes()

    # Write first guess for verification purposes
    output_cfg = {
            "title" : "MetstormLive2 - First Guess",
            "long_name": "MetstormLive2 - Cumulative Precipitation Estimates",
            "units" : "mm"
        }

    s3url = base_outputs.write_output_netcdf(
        opts,
        MSL2obj.qpe_output,
        opts.zulu_time,
        msl2_global_config,
        output_cfg,
        array_type="multi",
        out_var="best_qpe",
        output_type="FGS",
        domain_id = domain_id,
        MISSING_VALUE = -9999.0,
        to_modi=False,
        proc_stage = output_type
    )
    #send SNS message
    message_body = base_message.compose_message("MERGE_HANDLER",s3url)
    is_msg       = base_message.send_message(opts.sqs_obj,message_body)
    if is_msg:
        opts.logger.info(f"{domain_id} FGS message SENT")

    #=========================================================================
    # Looking for Gauge Data the satellite MSK here
    # IF no gauge data, saves and sends output to s3
    opts.logger.info(f"{domain_id} Looking for Gauge Data")

    is_gauge, gauge_data = gauge_utils.get_gauge_data(opts,domain_config,domain_id)

    if not is_gauge:
        opts.logger.info(f" {opts.domain_id} MergeNoGaugeFile: first guess is best qpe")

        output_cfg = {
                "title" : "MetstormLive2 - Best QPE [First Guess]",
                "long_name": "MetstormLive2 - Cumulative Precipitation Estimates",
                "units" : "mm"
            }

        s3url = base_outputs.write_output_netcdf(
            opts, 
            MSL2obj.qpe_output,
            opts.zulu_time,
            msl2_global_config,
            output_cfg,
            array_type="multi",
            out_var="best_qpe",
            output_type="QPE",
            domain_id = domain_id,
            MISSING_VALUE = -9999.0,
            to_modi=to_modi,
            proc_stage = output_type
        )            
        #send SNS message
        message_body = base_message.compose_message("MERGE_HANDLER",s3url)
        is_msg       = base_message.send_message(opts.sqs_obj,message_body)
        if is_msg:
            opts.logger.info(f"{domain_id} QPE message SENT")
        return "first guess"

    #=========================================================================
    # Continues Processing if Gauge Data is Available

    # get climo basemap file for current month for creation of dynamic basemap
    MSL2obj.load_clim_basemap(opts,opts.zulu_time,domain_id,domain_config)

    opts.logger.info(f" {opts.domain_id} Calculating Gauge Bias Field")
    bias_idw = gauge_utils.calculate_gauge_bias(
                opts,
                MSL2obj.CLM,
                MSL2obj.qpe_output,
                MSL2obj.base_grid,
                gauge_data,
                domain_config
            )

    #=========================================================================
    #=========================================================================
    # Producing Final QPE

    MSL2obj.qpe_output["best_qpe"].values[:] = \
        MSL2obj.qpe_output["best_qpe"].values[:] + \
            bias_idw
    del(bias_idw)

    # apply low precip threshold
    MSL2obj.qpe_output["best_qpe"].values[
        MSL2obj.qpe_output["best_qpe"].values <= 0.254
    ] = 0.0
    max_qpe = np.nanmax(MSL2obj.qpe_output["best_qpe"].values)

    opts.logger.info(f" {opts.domain_id} MergeMaxQPE: {str(max_qpe)}")

    # -----------------------------------------------------------------------
    # Writes Final QPE file
    output_cfg = {
            "title" : "MetstormLive2 - Best QPE [Best Guess]",
            "long_name": "MetstormLive2 - Cumulative Precipitation Estimates",
            "units" : "mm"
        }

    s3url = base_outputs.write_output_netcdf(
        opts, 
        MSL2obj.qpe_output,
        opts.zulu_time,
        msl2_global_config,
        output_cfg,
        array_type="multi",
        out_var="best_qpe",
        output_type="QPE",
        domain_id = domain_id,
        MISSING_VALUE = -9999.0,
        to_modi=to_modi,
        proc_stage = output_type
    )
    
    #send SNS message
    message_body = base_message.compose_message("MERGE_HANDLER",s3url)
    is_msg       = base_message.send_message(opts.sqs_obj,message_body)
    if is_msg:
        opts.logger.info(f"{domain_id} QPE message SENT")

    execution_time = (time.time() - start_time)
    opts.logger.info(f" {opts.domain_id} MergeTotalExecuteTime: {execution_time:.2f} seconds")

    return "second_guess"

if __name__ == "__main__":
    usage = ''
    p = OptionParser(usage=usage)
    p.add_option('--verbose', '-v', dest='verbose', default=False, action='store_true',
        help='increase logging to the DEBUG level.'
        )
    p.add_option('--config_path', help='Path to Ingest Config Files',default='./config',type='string')
    p.add_option('--logterminal', help='Path to configuration file',default=False,action='store_true')

    # =================================================================== 
    # Loading configurations 
    opts, args = p.parse_args()

    logging.config.fileConfig("msl2_qpe_pkg/base/logging.ini", disable_existing_loggers=False)
    opts.logger = logging.getLogger(__name__)
    opts.logger.info("+++++ MSL2: BEGIN MERGE POD ++++++ ")

    # for k8s health check
    Path('/tmp/healthy').touch()        
    # =================================================================== 
    # Get environment variables

    opts.ENV = os.getenv("ENV", "dev")

    opts.MSL2_INTERMEDIATE_BUCKET = os.getenv(
        "MSL2_INTERMEDIATE_BUCKET",
        "metstormlive-v2-intermediate-dev")

    opts.MSL2_MODI_BUCKET = os.getenv(
        "MSL2_MODI_BUCKET",
        "mg-obs-gridded-dtn-1qpe-dev-euw1")

    opts.domain_id = os.getenv(
        "MSL2_DOMAIN_ID",
        "CC")

    opts.is_fulldomain = base_utils.str_to_bool(
        os.getenv("IS_FULLDOMAIN",
            "True")
        )

    opts.n_slices = os.getenv(
        "N_SLICES",
        "1")

    opts.tile_id = os.getenv(
        "TILE_ID",
        "TILE-001")

    opts.minx = float(os.getenv(
        "MINX",
        "0.1"))
    opts.maxx = float(os.getenv(
        "MAXX",
        "0.2"))

    opts.miny = float(os.getenv(
        "MINY",
        "0.1"))

    opts.maxy = float(os.getenv(
        "MAXY",
        "0.2"))

    # =================================================================== 
    # Initiate AWS clients
    
    # If full domain, send the message to the product queue
    # If it's a subdomain, send it to the coordinator queue instead
    if opts.is_fulldomain:
        opts.sqs_obj = aws_utils.start_sqs_clients(opts,env_var="MSL2_PRODUCT_QUEUE")
    else:
        opts.sqs_obj = aws_utils.start_sqs_clients(opts,env_var="MSL2_COORDINATOR_QUEUE")

    opts.s3_fs   = aws_utils.init_s3FileSystem(anon=False)
    opts.s3      = aws_utils.init_boto3S3Client()

    #--------------------------------------
    # BEGIN MERGE
    if len(sys.argv) > 1:
        zulu_time = datetime.strptime(sys.argv[1], "%Y%m%d%H")
    else:
        zulu_time = datetime.utcnow()

    opts.zulu_time = zulu_time

    merge_main(opts,domain_id=opts.domain_id)
    exit(0)

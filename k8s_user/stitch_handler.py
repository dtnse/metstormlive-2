#!/usr/bin/env python3.8
# coding: utf-8
#
# DTN, LLC - METSTORM Live2
# MSL2 - STITCH.py (core code)
#   ICON & Remote Sensing Team (Dec, 2022)
######################################################################################
######################################################################################
from optparse import OptionParser
import os
import sys
import logging.config
from datetime import datetime
#import time

import numpy as np
import xarray as xr

#from msl2_qpe_pkg.base import msl2class
from msl2_qpe_pkg.base import aws_utils
from msl2_qpe_pkg.base import output as base_outputs
from msl2_qpe_pkg.base import utils as base_utils
from msl2_qpe_pkg.base import message as base_message
from msl2_qpe_pkg.base import grid as base_grid

######################################################################################
######################################################################################

def stitch_main(opts,output_type):

    domain_config = base_utils.load_domain_config(opts,opts.domain_id)
    msl2_global_config  = base_utils.load_json_config(opts)
    if output_type == "ECM":
        out_var = "qpe_conf"
    else:
        out_var = "best_qpe"

    # Create a grid of np.nan (which the mean function will skip)
    #ds_whole = create_xarray(domain_config['minx'],domain_config['maxx'],domain_config['miny'],domain_config['maxy'],out_var,"NAN")
    ds_whole = base_grid.init_BASE_grid(domain_config).expand_dims(tilenum=[0])
    aux    = np.empty((len(ds_whole.coords['lat']),len(ds_whole.coords['lon']),1))
    aux[:] = np.nan
    ds_whole[out_var] = (('lat', 'lon', 'tilenum'), aux)
    ds_whole = ds_whole.drop_vars(["new_2d_lat","new_2d_lon"])
    # Coordinates don't all match exactly if we don't do this
    ds_whole.coords['lat'] = np.round(ds_whole.coords['lat'],4)
    ds_whole.coords['lon'] = np.round(ds_whole.coords['lon'],4)
    del(aux)

    # Load the tiles from S3, which are numbered starting at 1
    for tilenum in range(1,int(opts.numslices)+1):
        file_use = f"{opts.MSL2_INTERMEDIATE_BUCKET}/subdomain_tiles/{opts.zulu_time:%Y%m%d_%H}/{opts.domain_id}-TILE-{tilenum:03g}_{output_type}_{opts.zulu_time:%Y%m%d_%H%M}_001hr.nc"
        try:
            with opts.s3_fs.open(file_use, "rb") as fobj:
                ds_temp = xr.load_dataset(fobj, decode_times=False).expand_dims(tilenum=[tilenum]).drop_vars(['time','Latitude_Longitude'])
            # Coordinates don't all match exactly if we don't do this
            ds_temp.coords['lat'] = np.round(ds_temp.coords['lat'],4)
            ds_temp.coords['lon'] = np.round(ds_temp.coords['lon'],4)
            ds_whole = xr.concat([ds_temp.copy(deep=True),ds_whole],dim="tilenum")
            opts.logger.info(f"     - loaded tile {tilenum} for {opts.domain_id}")
        except:
            opts.logger.warning(f"     - tile {tilenum} for {opts.domain_id} NOT found: {file_use}")

    result = ds_whole.mean(dim="tilenum")

    if np.isnan(np.nanmax(result[out_var])):
        opts.logger.warning("All-NaN output grid, not writing it")
        return False

    # Write Final file
    if output_type == "ECM":
        output_cfg = {
            "title" : "QPE Confidence Array",
            "long_name": "QPE Confidence Array",
            "units" : "dimensionless [0 - 1]"
        }
    else:
        output_cfg = {
            "title" : "MetstormLive2 - Best QPE [Best Guess]",
            "long_name": "MetstormLive2 - Cumulative Precipitation Estimates",
            "units" : "mm"
        }

    # Temporary workaround for not overwriting real data with junk
    base_domain_id = opts.domain_id
    opts.domain_id = "TEST_"+opts.domain_id

    s3url = base_outputs.write_output_netcdf(
        opts,
        result,
        opts.zulu_time,
        msl2_global_config,
        output_cfg,
        array_type="multi",
        out_var=out_var,
        output_type=output_type,
        domain_id = base_domain_id,
        MISSING_VALUE = -9999.0,
        to_modi=False
    )

    #send SNS message
    message_body = base_message.compose_message("STITCH_HANDLER",s3url)
    is_msg       = base_message.send_message(opts.sqs_obj,message_body)
    if is_msg:
        opts.logger.info(f"{opts.domain_id} {output_type} message SENT")

    del(ds_whole,ds_temp,result)

    # Rename tiles from S3, which are numbered starting at 1
    opts.domain_id = base_domain_id
    for tilenum in range(1,int(opts.numslices)+1):
        old_key = f"subdomain_tiles/{opts.zulu_time:%Y%m%d_%H}/{opts.domain_id}-TILE-{tilenum:03g}_{output_type}_{opts.zulu_time:%Y%m%d_%H%M}_001hr.nc"
        new_key = f"subdomain_tiles/{opts.zulu_time:%Y%m%d_%H}/{opts.domain_id}-TILE-{tilenum:03g}_{output_type}_{opts.zulu_time:%Y%m%d_%H%M}_001hr_STITCHED_{datetime.utcnow().strftime('%Y%m%d%H%M')}.nc"
        copy_src = {
                'Bucket': opts.MSL2_INTERMEDIATE_BUCKET,
                'Key': old_key
        }
        opts.s3.Object(opts.MSL2_INTERMEDIATE_BUCKET,new_key).copy(copy_src)
        opts.s3.Object(opts.MSL2_INTERMEDIATE_BUCKET,old_key).delete()
         

    return is_msg


if __name__ == "__main__":
    usage = ''
    p = OptionParser(usage=usage)
    p.add_option('--verbose', '-v', dest='verbose', default=False, action='store_true',
        help='increase logging to the DEBUG level.'
        )
    p.add_option('--config_path', help='Path to Ingest Config Files',default='./config',type='string')
    p.add_option('--logterminal', help='Path to configuration file',default=False,action='store_true')

    # ===================================================================
    # Loading configurations
    opts, args = p.parse_args()

    logging.config.fileConfig("msl2_qpe_pkg/base/logging.ini", disable_existing_loggers=False)
    opts.logger = logging.getLogger(__name__)

    # ===================================================================
    # Get environment variables

    opts.ENV = os.getenv("ENV", "dev")

    opts.MSL2_INTERMEDIATE_BUCKET = os.getenv(
        "MSL2_INTERMEDIATE_BUCKET",
        "metstormlive-v2-intermediate-dev")

    opts.MSL2_MODI_BUCKET = os.getenv(
        "MSL2_MODI_BUCKET",
        "mg-obs-gridded-dtn-1qpe-dev-euw1")

    opts.domain_id = os.getenv(
        "MSL2_DOMAIN_ID",
        "CC")

    opts.numslices = os.getenv(
        "MSL2_NUM_SLICES",
        6)

    opts.zulu_time = os.getenv(
        "MSL2_RUN_TIME",
        datetime.utcnow())
    # If the env var is defined, it is a string like 20221207_18,
    # so we need to make it a datetime object
    if isinstance(opts.zulu_time,str):
        opts.zulu_time = datetime.strptime(opts.zulu_time,"%Y%m%d_%H")

    # ===================================================================
    # Initiate AWS clients

    opts.sqs_obj = aws_utils.start_sqs_clients(opts,env_var="MSL2_PRODUCT_QUEUE")

    opts.s3_fs   = aws_utils.init_s3FileSystem(anon=False)
    opts.s3      = aws_utils.init_boto3S3Client()

    #--------------------------------------
    # BEGIN STITCH
    opts.logger.info('Stitch began')
    
    opts.logger.info(f"Stitching ECM for {opts.zulu_time:%Y%m%d-%H%M}")
    ecm_done = stitch_main(opts,"ECM")

    #opts.logger.info(f"Stitching QPE for {opts.zulu_time:%Y%m%d-%H%M}")
    #qpe_done = stitch_main(opts,"QPE")

    #opts.logger.info(f"Stitching FGS for {opts.zulu_time:%Y%m%d-%H%M}")
    #fgs_done = stitch_main(opts,"FGS")
    exit(0)

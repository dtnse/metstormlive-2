from datetime import datetime
import tempfile
import os

import xarray as xr
from osgeo import gdal

from msl2_qpe_pkg.base import output as base_outputs
from msl2_qpe_pkg.base import message as base_message


def process(queue_listener, opts,bucket, object_key, build_tiles=True):
    """
    Example input file name:
    20210704T120000Z-precipitation_rate_mean_over_1h-20210704T130000Z.grib
    """
    MISSING_VALUE = -9999.0

    # get filename and date/time of file
    path_parts = object_key.split("/")
    file_name_split = path_parts[-1].split("_")
    start_date_str = file_name_split[0].split("Z")[0]
    end_date_str = file_name_split[4].split("Z")[0]

    start_dt = datetime.strptime(start_date_str, "%Y%m%dT%H%M00")
    end_dt = datetime.strptime(end_date_str, "1h-%Y%m%dT%H%M00")
    forecast_init_str = datetime.strftime(start_dt, "%Y-%m-%dT%H:00:00Z")
    forecast_valid_str = datetime.strftime(end_dt, "%Y-%m-%dT%H:00:00Z")

    for region in queue_listener.config["domains"]:

        domain = queue_listener.config["domains"][region]

        w = domain["minx"]
        s = domain["miny"]
        e = domain["maxx"]
        n = domain["maxy"]
        grid_width = domain["nx"]
        grid_height = domain["ny"]
        long_name = domain["long_name"]

        # create output file names
        forecast_reform_file = (
            f"{region}_QPF_"
            f"{end_dt:%Y%m%d_%H%M}_001hr.nc"
        )

        warped_nc = f"{region}.nc"

        #  reproject, resample, and clip grid to domains
        with tempfile.NamedTemporaryFile() as s3_tmpfile:
            try:
                queue_listener.s3_fs.get_file(bucket + "/" + object_key, s3_tmpfile.name)
            except FileNotFoundError:
                queue_listener.s3_fs.invalidate_cache()
            else:
                opts.logger.info(f"...Warping, resampling, and clipping to domain for {long_name}")
                # In the raster format these bounds refer to the edges of pixels, and so
                # need to be offset by half of the dx and dy so that the output
                # (at the center of the pixel) lines up
                # with the intended grid
                # make four variables for the bounds to contain line length
                bounds_minx = w-(domain["grid_dx_deg"]*0.5)
                bounds_miny = s-(domain["grid_dy_deg"]*0.5)
                bounds_maxx = e+(domain["grid_dx_deg"]*0.5)
                bounds_maxy = n+(domain["grid_dy_deg"]*0.5)
                warp_options = gdal.WarpOptions(outputBounds=[bounds_minx, bounds_miny,
                                                bounds_maxx, bounds_maxy], dstSRS="EPSG:4326",
                                                resampleAlg="bilinear", format="NetCDF",
                                                width=grid_width, height=grid_height,
                                                srcNodata=MISSING_VALUE, dstNodata=MISSING_VALUE)

                ds = gdal.Warp(warped_nc, s3_tmpfile.name, options=warp_options)
                ds = None   # noqa: F841
        # grab file info for metadata
        ds = gdal.Open(warped_nc)
        llcrnrlon, xres, _, urcrnrlat, _, yres = ds.GetGeoTransform()
        warped_width = ds.RasterXSize
        warped_height = ds.RasterYSize
        urcrnrlon = llcrnrlon + (warped_width * xres)
        llcrnrlat = urcrnrlat + (warped_height * yres)

        llcrnrlat = round(llcrnrlat, 1)
        llcrnrlon = round(llcrnrlon, 1)
        urcrnrlat = round(urcrnrlat, 1)
        urcrnrlon = round(urcrnrlon, 1)
        ds = None

        # update metadata and conforming to CF1.6 standards
        forecast_reform = xr.open_dataset(warped_nc, decode_cf=False)
        # rename variable to appropriate name
        forecast_reform = forecast_reform.rename({"Band1": "qpf60min"})
        # convert from kg/(m^2 s) to mm/hr, round to 4 digits
        forecast_reform["qpf60min"] = (
            forecast_reform.qpf60min * 60 * 60
        ).round(4)

        # add other variables
        forecast_reform["time"] = end_dt
        forecast_reform["forecast_reference_time"] = start_dt
        forecast_reform["Latitude_Longitude"] = 0
        output_cfg = {
            "title" : "OneDTN_Forecast_60min_precip_rate",
            "long_name": "OneDTNForecast60minPrecipRate",
            "units" : "mm"
        }
        opts.domain_id = region
        opts.zulu_time = end_dt

        s3url = base_outputs.write_output_netcdf(
            opts, 
            forecast_reform,
            opts.zulu_time,
            queue_listener.config,
            output_cfg,
            array_type="multi",
            out_var="qpf60min",
            output_type="QPF",
            domain_id = region,
            MISSING_VALUE = MISSING_VALUE,
            to_modi=False,
            proc_stage="preprocessed",
            freq_id="001hr"
        )

        message_body = base_message.compose_message("PREPROC_HANDLER",s3url)
        is_msg       = base_message.send_message(opts.sqs_obj,message_body)
        if is_msg:
            opts.logger.info(f" - NEW-MSG {region} message SENT for {s3url}")

        os.remove(warped_nc)

    return True

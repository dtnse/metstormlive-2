#! /usr/bin/env python3

"""
DTN, LLC - MetStormLive2 - Satellite-based precipitation mask
Real-Time Satellite-Derived precipitation mask production

Remote Sensing and ICON Teams [Ric Domingues, Jul, 2021]

"""


import os
from optparse import OptionParser
from datetime import datetime
from multiprocessing import Pool
import logging.config
import tempfile
from joblib import load

import pandas as pd
import tensorflow as tf
import xarray as xr
import numpy as np


import precipmask_utils as utils

"""
Functions for producing the precip-mask in real-time
"""


def cp_source_ml_params(self, domain, model_domain):
    for key in self[model_domain]:
        if key.split("_")[0] == "ml":
            self[domain][key] = self[model_domain][key]


def init_output_ds(domain_cfg):

    lon1d = np.arange(
        domain_cfg["minx"],
        domain_cfg["maxx"] + domain_cfg["grid_dx_deg"] / 2,
        domain_cfg["grid_dx_deg"]
    )

    lat1d = np.arange(
        domain_cfg["miny"],
        domain_cfg["maxy"] + domain_cfg["grid_dx_deg"] / 2,
        domain_cfg["grid_dy_deg"]
    )

    var_mask = "probprecip{:02d}min".format(int(domain_cfg["temporal_resolution"]))
    mask_shape = [len(lat1d), len(lon1d)]
    ds = xr.Dataset(
        {var_mask: (("lat", "lon"), np.empty(mask_shape))}
    )

    # lon2d,lat2d = np.meshgrid(lon1d,lat1d)

    ds.coords['lat'] = (('lat'), np.round(lat1d, 4))
    ds.coords['lon'] = (('lon'), np.round(lon1d, 4))

    return ds, var_mask


def model_predict(model_name, X_data):

    model = tf.keras.models.load_model(model_name, compile=False)

    Probs_out = model.predict(X_data.values)

    return pd.DataFrame(index=X_data.index, data=Probs_out)


def multiprocessing_model_inference(model_name, X_Predict, n_cores=4):
    """
    This function parallelizes the model inference between n_cores
    """
    df = pd.DataFrame(data=X_Predict)
    df_split = np.array_split(df, n_cores)

    array_of_df_tuples = [(model_name, df_split[i]) for i in range(0, n_cores)]

    pool = Pool(n_cores)
    Predict_Probs = pd.concat(pool.starmap(model_predict, array_of_df_tuples)).values
    pool.close()
    pool.join()

    return Predict_Probs


def write_output_netcdf2s3(self, s3_client, var_mask, config, domain_cfg, metadata, tstamp):

    domain = domain_cfg["abbreviated_name"]

    # local file name

    # retrieving dataset dimensions
    xres = domain_cfg["grid_dx_deg"]
    yres = domain_cfg["grid_dy_deg"]

    warped_height, warped_width = self[var_mask].shape

    llcrnrlat = np.round(self.lat.values.min(), 1)
    llcrnrlon = np.round(self.lon.values.min(), 1)
    urcrnrlat = np.round(self.lat.values.max(), 1)
    urcrnrlon = np.round(self.lon.values.max(), 1)

    # set global attributes
    self.attrs["title"] = var_mask
    self.attrs["map_proj"] = metadata["map_proj"]
    self.attrs["Conventions"] = metadata["Conventions"]
    self.attrs["institution"] = metadata["institution"]
    self.attrs["grid_mapping_name"] = metadata["grid_mapping_name"]
    self.attrs["DataConvention"] = metadata["DataConvention"]
    self.attrs["nx"] = warped_width
    self.attrs["ny"] = warped_height
    self.attrs["dx"] = 1.11
    self.attrs["dy"] = 1.11
    self.attrs["llcrnrlat"] = llcrnrlat
    self.attrs["llcrnrlon"] = llcrnrlon
    self.attrs["urcrnrlat"] = urcrnrlat
    self.attrs["urcrnrlon"] = urcrnrlon
    self.attrs["lowerleft_latlon"] = (llcrnrlon, llcrnrlat,)
    self.attrs["upperright_latlon"] = (urcrnrlon, urcrnrlat,)
    self.attrs["dlat"] = np.round(yres, 2)
    self.attrs["dlon"] = np.round(xres, 2)

    # set up attributes for each band
    # ghe60min attibutes
    self[var_mask].attrs["coordinates"] = "lat lon"
    self[var_mask].attrs["grid_mapping"] = "latitude_longitude"
    self[var_mask].attrs["least_significant_digit"] = 4
    self[var_mask].attrs["deflate"] = 1
    self[var_mask].attrs["deflate_level"] = 4
    self[var_mask].attrs["units"] = "NA"
    self[var_mask].attrs["long_name"] = "Satellite-Derived Observable Precipitation Probability"
    self[var_mask].attrs["missing_value"] = -9999

    # time attributes
    self["time"] = tstamp
    self.time.attrs["standard_name"] = "time"
    self.time.attrs["string"] = datetime.strftime(tstamp, "%Y-%m-%dT%H:00:00Z")
    self.time.attrs["period_string"] = "0 minutes"

    # latitude_longitude attributes
    self["Latitude_Longitude"] = 0
    self.Latitude_Longitude.attrs["grid_mapping_name"] = "latitude_longitude"
    self.Latitude_Longitude.attrs["llcrnrlon"] = llcrnrlon
    self.Latitude_Longitude.attrs["llcrnrlat"] = llcrnrlat
    self.Latitude_Longitude.attrs["urcrnrlon"] = urcrnrlon
    self.Latitude_Longitude.attrs["urcrnrlat"] = urcrnrlat

    # longitude attributes
    self.lon.attrs["deflate"] = 1
    self.lon.attrs["deflate_level"] = 4
    self.lon.attrs["long_name"] = "longitude"
    self.lon.attrs["units"] = "degrees_east"
    self.lon.attrs["standard_name"] = "longitude"
    self.lon.attrs["least_significant_digit"] = 4

    # latitude attributes
    self.lat.attrs["deflate"] = 1
    self.lat.attrs["deflate_level"] = 4
    self.lat.attrs["long_name"] = "latitude"
    self.lat.attrs["units"] = "degrees_north"
    self.lat.attrs["standard_name"] = "latitude"
    self.lat.attrs["least_significant_digit"] = 4

    self.attrs["Product_Name"] = "{} - Version {}".format(
        config["general_config"]["title"],
        config["general_config"]["version"]
    )
    self.attrs["Product_Description"] = config["general_config"]["description"]
    self.attrs["Product_Source"] = config["general_config"]["source"]
    self.attrs["Domain"] = domain
    self.attrs["Source_ML_Domain"] = config[domain]["ml_model_key"]
    self[var_mask].attrs["valid_range"] = "(0.0 - 1.0)"

    # compress all variables and save output to new netcdf file
    comp = dict(zlib=True, complevel=4)
    encoding = {var: comp for var in self.data_vars}
    encoding.update({var: comp for var in self.coords})

    s3_netcdf = utils.get_s3_netcdf_filename(config, domain_cfg, tstamp)
    filetmp = tempfile.NamedTemporaryFile()
    self.to_netcdf(filetmp.name, encoding=encoding)
    s3_client.upload_file(
        Filename=filetmp.name,
        Bucket=config["general_config"]["s3_output_bkt"],
        Key=s3_netcdf, ExtraArgs={"ACL": "bucket-owner-full-control"}
    )

    return "s3://{}/{}".format(
        config["general_config"]["s3_output_bkt"],
        s3_netcdf
    )


if __name__ == "__main__":
    usage = ""
    p = OptionParser(usage=usage)
    p.add_option("--verbose", "-v", dest="verbose", default=False, action="store_true",
                 help="increase logging to the DEBUG level.")
    p.add_option("-c", dest="config_file", default="./config/precip-mask_config.yaml",
                 help="Path to Precipitation Mask Default Config File")
    p.add_option("-l", dest="lookback", default=0,
                 help="Number of retroactive hours to produce the output")
    p.add_option("-i", dest="initialize", default=False, action="store_true",
                 help="Initialize a new ML model")
    p.add_option("--msl_config", dest="msl_config_file",
                 default="../../config/msl2.json",
                 help="Path to MSL2 Default Config File")
    opts, args = p.parse_args()

    # Loading MSL configurations
    msl_config = utils.load_json_config(cfg_file=opts.msl_config_file)
    metadata = msl_config["metadata"]

    # ===================================================================
    # Intialize Logging
    logging.config.fileConfig("./logging.ini", disable_existing_loggers=False)
    logger = logging.getLogger(__name__)

    # ===================================================================
    # Loading ML configurations
    config = utils.load_config(logger, cfg_file=opts.config_file)
    # overwrites test config from precip-mask
    config["general_config"]["s3_output_bkt"] = os.getenv(
                            "MSL2_INTERMEDIATE_BUCKET",
                            "metstormlive2-intermediate-dev"
    )

    # ===================================================================
    # Print Current Configurations
    logger.info("BEGIN: {} - RT Production".format(config["general_config"]["title"]))
    logger.info("  Sat-based Precip-Mask Production, ML Model Version: {}".format(
        config["general_config"]["version"])
    )

    # ===================================================================================
    # Intializing AWS s3 clients
    logger.info("...Intializing boto3 client")
    s3_client = utils.init_s3botoclient()

    logger.info("...Intializing s3 FileSystem")
    s3_fs = utils.init_s3FileSystem()

    """
    LOOP THROUGH THE DOMAINS
    """

    for domain_cfg in msl_config["domains"]:

        domain = domain_cfg["abbreviated_name"]

        try:
            # return timestamp rounded back to the nearest hour
            tnow = pd.Timestamp.utcnow().floor(freq="H").tz_localize(None)

            # ===================================================================================
            # Only train models marked for training with is_ml_training=True

            logger.info("=> PROCESSING domain {} - {}".format(domain, domain_cfg["long_name"]))
            logger.info(" - Satellite source: {}".format(config[domain]["sat_s3_ingest"]))

            is_prod = any(x == domain for x in config["general_config"]["domains_to_run"])

            if not is_prod:
                logger.info("-> domain {} NOT configured for PRODUCTION ->> skipping".format(
                    domain
                    ))
                continue

            if not config[domain]["is_ml_training"]:
                logger.info("-> domain NOT trained ->> loading ML model from {}".format(
                                            config[domain]["ml_model_key"]))

                model_domain = config[domain]["ml_model_key"]
                cp_source_ml_params(config, domain, model_domain)
            else:
                model_domain = domain

            # Get PROD domain configurations from general config:
            config[domain]["prd_domain_edges"] = [
                domain_cfg["minx"], domain_cfg["maxx"],
                domain_cfg["miny"], domain_cfg["maxy"]
                ]
            # retrieve accumulation interval
            config[domain]["ml_hr_interval"] = int(int(domain_cfg["temporal_resolution"]) / 60)

            # config[domain]["prd_domain_edges"]
            # if model and training hist already exist, they are named as
            filename_model, filename_hist, filename_scaler = utils.load_default_s3_filenames(
                config, domain
            )

            logger.info("  -> loading EXISTING Tensorflow MODEL and SCALAR")
            is_model = s3_fs.exists("{}/{}".format(
                config["general_config"]["s3_output_bkt"],
                filename_model)
            )
            is_scale = s3_fs.exists("{}/{}".format(
                config["general_config"]["s3_output_bkt"],
                filename_scaler)
            )

            if is_model and is_scale:

                logger.debug("---> loading {}".format(filename_model))
                filetmp_model_dn = tempfile.NamedTemporaryFile()
                filetmp_model_dn.name += ".h5"
                s3_client.download_file(config["general_config"]["s3_output_bkt"],
                                        filename_model, filetmp_model_dn.name)

                logger.debug("  -> loading {}".format(filename_scaler))

                s3_file = "{}/{}".format(
                    config["general_config"]["s3_output_bkt"],
                    filename_scaler)

                try:
                    fobj = s3_fs.open(s3_file)
                except FileNotFoundError:
                    s3_fs.invalidate_cache()
                    fobj = s3_fs.open(s3_file)
                scaler = load(fobj)

            else:
                logger.error("Model or HIST not found in s3: {0} {1}".format(
                    filename_model, filename_scaler
                    ))
                continue
        except Exception as e:
            logger.error("Error initializing Domain {0}; skipping it ->>".format(domain))
            logger.error(e)
            continue

        # =======================================================================================
        # loops through the lookback timestamps
        for p_id in range(-1*int(opts.lookback), 1):
            tstamp = tnow + p_id*pd.Timedelta("1 hour")
            try:
                # ===================================================================================
                # initializing EMPTY output dataframe with grid specs
                output_ds, var_mask = init_output_ds(domain_cfg)

                # ===================================================================================
                # LOOP through timestamps

                logger.info("---> Loading raw satellite data for {}".format(tstamp))

                sat_data = utils.get_sat_bands_s3_multiprocess(
                    s3_fs,
                    logger,
                    tstamp,
                    hour_interval=int(config[domain]["ml_hr_interval"]),
                    s3bucket=config[domain]["sat_s3_ingest"],
                    satkey=config[domain]["ml_source_sats"],
                    verbose=opts.verbose,
                    satbands=config[domain]["ml_sat_bands"],
                    slice_area=config[domain]["prd_domain_edges"],
                    output_type="mean",
                    file_regex=config[domain]["sat_file_regex"],
                    tstamp_fmt=config[domain]["sat_tstamp_fmt"]
                )

                logger.info("---> PRODUCING sat-based precip-mask for {}".format(tstamp))

                X_Predict, output_shape = utils.gather_PredictData(sat_data)
                X_Predict_scalled = scaler.transform(X_Predict)

                # multiprocessing model inference
                logger.info("---> multiprocessing predict with {} cores".format(
                    config["general_config"]["prd_infer_ncores"]))

                Predict_Probs = multiprocessing_model_inference(
                    filetmp_model_dn.name,
                    X_Predict_scalled,
                    n_cores=config["general_config"]["prd_infer_ncores"]
                )

                sat_data["PrecipMask_prob"] = (("lat", "lon"), np.reshape(Predict_Probs,
                                                                          output_shape))

                # Composing OUTPUT NETCDF
                output_ds[var_mask].values = sat_data.PrecipMask_prob.interp(
                                                lat=output_ds.lat.values,
                                                lon=output_ds.lon.values,
                                                method="nearest").fillna(0.0).values

                # --------------------- SAVING NETCDF

                s3file = write_output_netcdf2s3(
                    output_ds,
                    s3_client,
                    var_mask,
                    config,
                    domain_cfg,
                    metadata,
                    tstamp
                )

                logger.info("---> uploaded output to {}".format(s3file))

                del sat_data, output_ds

            except Exception as e:
                logger.error("Error ingesting timestamp {0}; skipping it ->>".format(tstamp))
                logger.error(e)
                continue

    logger.info("END")

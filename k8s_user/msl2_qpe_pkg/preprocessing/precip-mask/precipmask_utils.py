#!/usr/bin/env python

"""
encoding: utf-8

DTN, LLC - Satellite-derived precipitation mask functions

Remote Sensing and ICON Teams [Ric Domingues, Jul, 2021]
"""
import os
import sys
import yaml
import json
import logging
from logging.handlers import TimedRotatingFileHandler
from datetime import datetime, timedelta
import multiprocessing as mp
import re

import boto3
import s3fs
import pandas as pd
from affine import Affine
import xarray as xr
import numpy as np
import pyproj
import gzip


def init_logging(config, args, key='test'):
    logfile = "{}/{}_{}.log".format(config['general_config']['log_dir'],
                                    datetime.now().strftime('%Y%m%d'), key)
    logger = logging.getLogger(__name__.split('.')[0])
    logger.setLevel(logging.DEBUG)
    log_format = '%(asctime)s %(levelname)-4s %(message)-7s  | %(filename)s.%(funcName)s'
    logFormatter = logging.Formatter(log_format, datefmt='%m/%d/%Y %I:%M:%S')

    logHandlerTRFH = TimedRotatingFileHandler(filename=logfile,
                                              when='H',
                                              interval=6,
                                              backupCount=40,
                                              utc=True)
    logHandlerTRFH.setLevel(logging.DEBUG if args.verbose else logging.INFO)
    logHandlerTRFH.setFormatter(logFormatter)
    logHandlerTRFH.suffix = '%Y%m%d%H'
    logger.addHandler(logHandlerTRFH)

    return logger, logfile


def init_s3FileSystem():
    return s3fs.S3FileSystem(anon=False)


def init_s3botoclient():
    return boto3.client('s3')


def load_json_config(cfg_file="../../config/msl2.json"):
    """
    Loads the configuration file of the engine and return the parameters
    Args:
        config_file: str
    Returns:
        data dictionary
    """

    with open(cfg_file, 'r') as jobj:
        return json.loads(jobj.read())


def load_default_s3_filenames(config, domain):

    filename_model = "{}/{}/{}/models/model__Latest.h5".format(
        config['general_config']['s3_model_dir'],
        config[domain]['ml_model_key'],
        config['general_config']['version']
    )

    filename_hist = (
        "{}/{}/{}/training_hist/"
        "TrainingHist__Latest.feather"
        ).format(
        config['general_config']['s3_model_dir'],
        domain,
        config['general_config']['version']
    )

    filename_scaler = "{}/{}/{}/sat_MinMaxScalar.joblib".format(
        config['general_config']['s3_model_dir'],
        config[domain]['ml_model_key'],
        config['general_config']['version']
    )

    return filename_model, filename_hist, filename_scaler


def load_latest_s3_filenames(config, domain, frames_ingested, timestamp):

    filename_model = ("{}/{}/{}/models/model__FramesIngested"
                      "-{:08d}__LastTimestamp-{}.h5").format(
        config['general_config']['s3_model_dir'],
        domain,
        config['general_config']['version'],
        frames_ingested,
        timestamp.strftime('%Y%m%dT%H%M%S')
    )

    filename_hist = ("{}/{}/{}/training_hist/"
                     "TrainingHist__FramesIngested"
                     "-{:08d}__LastTimestamp-{}.feather").format(
        config['general_config']['s3_model_dir'],
        domain,
        config['general_config']['version'],
        frames_ingested,
        timestamp.strftime('%Y%m%dT%H%M%S')
    )

    return filename_model, filename_hist


def get_s3_netcdf_filename(config, domain_cfg, tstamp):

    # RR_TYP_YYYYMMDD_HHMM_AAAUU.FMT
    filename_s3 = "{}/{}/{}_{}_{}_001hr.nc".format(
        config['general_config']['s3_output_dir'],
        tstamp.strftime('%Y%m%d_%H'),
        domain_cfg['abbreviated_name'],
        config['general_config']['output_label'],
        tstamp.strftime('%Y%m%d_%H%M')
    )

    return filename_s3


def load_config(logger, cfg_file="./config/precip-mask_config.yaml"):
    """
    Loads the configuration file with directories and other configs
    Returns:
        config: dictionary with keys
    """
    try:
        with open(cfg_file) as f:
            config = yaml.full_load(f.read())
        return config
    except Exception as e:
        logger.info("Error reading {0}: {1}".format(cfg_file, e))
        sys.exit(1)


def list_radar_files_s3(fs, bucket_dir=None):
    """
    Lists the MetStormLive files along with their timestamps
    Arg:
        direc:     parent data directory
        runtype:   type of MetStormLive run (first or last)
    Returns:
        files_df: pandas dataframe with file info
    """
    result = fs.glob(bucket_dir)
    result = result[1::]
    tiffs = []
    timestamp = []
    for k in result:
        tiffs.append(os.path.basename(k))
        timestamp.append(
            datetime.strptime(os.path.basename(k), "%Y%m%d-%H%M%S.netcdf.gz")
        )
    files_df = pd.DataFrame(
        list(zip(timestamp, result, tiffs)),
        columns=["Timestamp", "FullPath", "files"]).sort_values(
            by="FullPath").reset_index(drop=True)

    files_df = files_df.set_index("Timestamp")
    files_df["Timestamp"] = files_df.index
    # print(files_df["Timestamp"])

    return files_df.resample("1H").pad()


def list_MetStorm_files_s3(fs, bucket_dir='.', runtype='first'):
    """
    Lists the MetStormLive files along with their timestamps from S3 Buckets
    Arg:
        direc:     parent data directory
        runtype:   type of MetStormLive run (first or last)
    Returns:
        files_df: pandas dataframe with file info
    """

    result = fs.glob(bucket_dir)

    tiffs = []
    timestamp = []
    for k in result[1::]:
        tiffs.append(os.path.basename(k))
        parsed_tstamp = datetime.strptime(os.path.basename(k).split('_'))[1][0:15]
        timestamp.append(parsed_tstamp, '%Y%m%d-%H%M%S')
        files_df = (
            pd.DataFrame(
                list(zip(timestamp, result, tiffs)), columns=['Timestamp', 'FullPath', 'files']
            )
            .sort_values(by='FullPath')
            .drop_duplicates(subset=["files"], keep=runtype)
            .reset_index(drop=True)
        )

    return files_df


def get_binary_precip_class(self, precip_tshld=0.01, tgt_var='cumulative_precip'):
    self['is_precip'] = (("lat", "lon"), np.zeros(self[tgt_var].values.shape, dtype=int))
    self['is_precip'].values[(self[tgt_var] > precip_tshld)] = int(1)


def get_ingest_mask(self, dx=2, tgt_var='MetStorm_precip',
                    msk_var='MetStorm_precip_mask', msk_tshld=0, pcp_tshld=0):
    # fills array with zeros
    self['mask_ingest'] = self[msk_var].where(self[msk_var] > 10, 0).fillna(0)
    self['mask_ingest'].values[0::dx, 0::dx] = 1
    self['mask_ingest'].values[self[tgt_var] > pcp_tshld] = 1
    self['mask_ingest'].values[self[msk_var] < msk_tshld] = 0


def read_McIDAS_nc_s3(fs, ncfile, slice_area=None):
    # print(ncfile)
    fgz = fs.open(ncfile)
    if ncfile[-3:] == '.gz':
        with gzip.open(fgz, 'rb') as fobj:
            ds = xr.open_dataset(fobj, chunks="auto").rename_dims({"lines": "lat", "elems": "lon"})
    else:
        ds = xr.open_dataset(fgz, chunks="auto").rename_dims({"lines": "lat", "elems": "lon"})

    ds.coords["lat"] = (("lat"), ds.latitude[:, 0].values)
    ds.coords["lon"] = (("lon"), ds.longitude[0, :].values)

    if slice_area is not None:
        # slice_area = [lon_min, lon_max, lat_min, lat_max]
        ds = ds.sel(lon=slice(slice_area[0], slice_area[1]),
                    lat=slice(slice_area[3], slice_area[2]))

    da = xr.Dataset({"RadK": (("lat", "lon"), ds.data.squeeze().values)})
    da.coords["lat"] = (("lat"), ds.latitude[:, 0].values)
    da.coords["lon"] = (("lon"), ds.longitude[0, :].values)
    ds.close()
    del ds

    return da


def read_McIDAS_nc_s3_multi(fs, ncfile, conn, slice_area=None):
    fgz = fs.open(ncfile)
    if ncfile[-3:] == ".gz":
        with gzip.open(fgz, "rb") as fobj:
            ds = xr.open_dataset(fobj, chunks="auto").rename_dims(
                {"lines": "lat", "elems": "lon"}
            )
    else:
        ds = xr.open_dataset(fgz, chunks="auto").rename_dims(
            {"lines": "lat", "elems": "lon"}
        )
    ds.coords["lat"] = (("lat"), ds.latitude[:, 0].values)
    ds.coords["lon"] = (("lon"), ds.longitude[0, :].values)

    if slice_area is not None:
        # slice_area = [lon_min, lon_max, lat_min, lat_max]
        ds = ds.sel(
            lon=slice(slice_area[0], slice_area[1]),
            lat=slice(slice_area[3], slice_area[2]),
        )
    ds2 = xr.Dataset({"RadK": (("lat", "lon"), ds.data.squeeze().values)})
    ds2.coords["lat"] = (("lat"), ds.latitude[:, 0].values)
    ds2.coords["lon"] = (("lon"), ds.longitude[0, :].values)
    ds2.coords["band"] = ds.bands[0].values
    ds2.coords["time"] = ds.imageTime.values
    ds2 = ds2.expand_dims(["time", "band"])

    conn.send(ds2)
    conn.close()

    ds.close()
    del ds


def get_sat_bands_s3_multiprocess(
    fs,
    logger,
    TimeStamp,
    hour_interval=1,
    s3bucket=".",
    satkey="goes",
    verbose=True,
    satbands=range(1, 17),
    slice_area=None,
    output_type='mean',
    file_regex=r'goes_bd\d{2}_(.*).nc.gz',
    tstamp_fmt='%Y%m%d_%H%M'
        ):
    """
    Returns an Xarray of Averaged Band data for bands provided
    Arg:
        direc:     parent data directory
        hour_interval:   cumulative hours
    Returns:
        band_all: Xarray Dataset containing the mean of all bands.
    """
    file_pattern = re.compile(r'{}'.format(file_regex))

    # create a list to keep all processes
    parent_connections = []
    # create a list to keep connections
    processes = []
    # use forkserver as it is faster than spawn, and forksafe for support of fsspec
    # also, use a context, in case other libraries are using multiprocessing (i.e. tensorflow)
    ctx = mp.get_context("forkserver")

    for bd, band_use in enumerate(satbands):

        result = []
        # gather files for current day and one day before
        for i in range(1, -1, -1):
            yesterday = TimeStamp - timedelta(days=i)
            direc = "{}/{}{:02d}/netcdf/{}/{:02d}/{:02d}/".format(
                s3bucket, satkey,
                band_use,
                yesterday.year,
                yesterday.month,
                yesterday.day
            )
            result += fs.glob(direc)

        nc_paths = []
        timestamp = []
        for k in result[0::]:
            nc_paths.append(os.path.basename(k)), timestamp.append(
                datetime.strptime(file_pattern.search(k).group(1), tstamp_fmt)
                )

        df = (
            pd.DataFrame(
                list(zip(timestamp, result, nc_paths)),
                columns=["Timestamp", "FullPath", "files"],
            )
            .sort_values(by="Timestamp")
            .reset_index(drop=True)
        )
        DT = int(np.round(hour_interval*60))
        filtered_df = (
            df["FullPath"]
            .loc[
                (df.Timestamp > TimeStamp - timedelta(minutes=DT))
                & (df.Timestamp <= TimeStamp)
            ]
            .reset_index(drop=True)
        )

        for cb, df in enumerate(filtered_df):
            if verbose:
                logger.info("---> Averaging Band {} -> {}".format(band_use, df))
            # create a pipe for communication
            parent_conn, child_conn = ctx.Pipe()
            parent_connections.append(parent_conn)
            process = ctx.Process(
                target=read_McIDAS_nc_s3_multi,
                args=(fs, df, child_conn, slice_area, )
            )
            processes.append(process)

    bands = []

    # divide processes into groups of 14
    n = 14
    process_groups = [processes[i:i + n] for i in range(0, len(processes), n)]
    parent_connection_groups = [parent_connections[i:i + n]
                                for i in range(0, len(parent_connections), n)]

    # process in n groups
    for idx, process_group in enumerate(process_groups):
        logger.info(f"starting process group # {idx}")
        for process in process_group:
            process.start()
        # select corresponding parent connection group by index
        parent_connection_group = parent_connection_groups[idx]
        for parent_connection in parent_connection_group:
            band_aux = parent_connection.recv()
            # logger.info(f"processed {band_aux}")
            bands.append(band_aux)

    band_all = xr.combine_by_coords(bands)

    if output_type == 'mean':
        df_out = band_all.mean(dim='time')
    elif output_type == 'diff':
        df_out = band_all.diff(dim='time').squeeze()

    df_out.attrs["ref_time"] = band_all.coords["time"]

    band_all.close()
    del band_all, bands, band_aux

    return df_out


def interp_MetStorm2Goes(MetStormQPE, goes_data, tgt_var='qpe_class'):

    goes_data['MetStorm_precip'] = MetStormQPE.cumulative_precip.interp(
        lat=goes_data.lat.values,
        lon=goes_data.lon.values,
        method="linear").fillna(0)
    goes_data['MetStorm_precip_mask'] = MetStormQPE.qpe_mask.interp(
        lat=goes_data.lat.values,
        lon=goes_data.lon.values,
        method="linear").fillna(1)
    goes_data['MetStorm_precip_class'] = MetStormQPE[tgt_var].interp(
        lat=goes_data.lat.values,
        lon=goes_data.lon.values,
        method="nearest").fillna(1)

    return goes_data


def interp_ylabel2sat(self, y_ds, tgt_var='qpe_class', msk_var='qpe_mask'):
    self[tgt_var] = y_ds[tgt_var].interp(
        lat=self.lat.values,
        lon=self.lon.values,
        method="nearest").fillna(0)
    self[msk_var] = y_ds[msk_var].interp(
        lat=self.lat.values,
        lon=self.lon.values,
        method="nearest").fillna(0)


def read_MetStormLive_QPE(filetiff='.', dx=30):

    """
    Returns an Xarray of cumulative precipitation from MetStormLive
    Arg:
        filetiff:    MetStormLive tiff output
        dx = sumsampling parameter
    Returns:
        qpe_total: xarray with cumulative precipitation over the timeframe indicated
    """

    da = xr.open_rasterio(filetiff, chunks={'band': 1, 'x': 1024, 'y': 1024})

    transform = Affine(*da.attrs["transform"])
    nx, ny = da.sizes["x"], da.sizes["y"]

    x, y = transform * np.meshgrid(np.arange(nx) + 0.5, np.arange(ny) + 0.5)

    x = x[::dx, ::dx]
    y = y[::dx, ::dx]
    qpe = da.isel(band=0)[::dx, ::dx].values
    mask = np.zeros(qpe.shape)
    mask[qpe < -1] = 1

    qpe[qpe < 0.1] = 0
    transformer = pyproj.Transformer.from_crs("epsg:3857", "epsg:4326")
    lat, lon = transformer.transform(x, y)

    MetQPE = xr.Dataset({"cumulative_precip": (("lat", "lon"), qpe)})
    MetQPE['qpe_mask'] = (("lat", "lon"), mask)
    MetQPE.coords["lat"] = (("lat"), lat[:, 0])
    MetQPE.coords["lon"] = (("lon"), lon[0, :])
    MetQPE.attrs["units"] = "mm"

    da.close()
    del da

    return MetQPE


def gather_PredictData(goes_data):

    m, n = goes_data.isel(band=0).RadK.values.shape

    X_Predict = np.zeros([m*n, len(goes_data.band.values[:])])
    c = -1
    for i in goes_data.band.values.tolist():
        c += 1
        X_Predict[:, c] = np.reshape(goes_data.RadK.isel(band=c).values / 300, [m*n])

    return X_Predict, [m, n]


def gather_TrainingData(goes_data, tgt_var='MetStorm_precip_class'):

    Y_train = np.expand_dims(goes_data[tgt_var].values[goes_data.mask_ingest > 0], axis=1)
    X_train = np.zeros([len(Y_train), len(goes_data.band.values)])

    c = -1
    for i in goes_data.band.values.tolist():
        c += 1
        xr_aux = goes_data.RadK.isel(band=c).where(goes_data['mask_ingest'], 0)
        xr_aux = goes_data.isel(band=c).RadK.copy()
        xr_aux.values[goes_data['mask_ingest'] < 1] = np.nan
        X_train[:, c] = xr_aux.values[np.logical_not(np.isnan(xr_aux.values))]/300

    return X_train, Y_train


def read_sparse_nc(fs, s3file, var='RadarConfidence', latsize=3200, lonsize=6900):

    fgz = fs.open(s3file)

    if s3file[-3:] == '.gz':
        with gzip.open(fgz, 'rb') as fobj:
            ds = xr.open_dataset(fobj)
    else:
        ds = xr.open_dataset(fgz)

    nwlat = ds.attrs['Latitude']
    nwlon = ds.attrs['Longitude']
    latspacing = round(ds.attrs['LatGridSpacing'], 3)
    lonspacing = round(ds.attrs['LonGridSpacing'], 3)
    swlat = nwlat - (latsize * latspacing)
    nelon = nwlon + (lonsize * lonspacing)
    lats = np.arange(swlat, nwlat, latspacing) + latspacing / 2
    lons = np.arange(nwlon, nelon, lonspacing) + lonspacing / 2

    sparse_data = ds[var].values[:]
    pix_x = ds['pixel_x'].values[:]
    pix_y = ds['pixel_y'].values[:]
    pix_ct = ds['pixel_count'].values[:]
    starts = np.int32(lonsize) * np.int32(pix_x) + np.int32(pix_y)
    ends = starts + pix_ct
    output = np.empty(latsize*lonsize)

    for lo, hi, val in zip(starts, ends, sparse_data):
        output[lo:hi] = val
    masked_output = np.ma.masked_less_equal(output, 0)

    lats_2d, lons_2d = np.meshgrid(lats, lons)
    output_2d = np.flipud(masked_output.reshape(latsize, lonsize))

    # Writing Output xarray
    da = xr.Dataset({var: (("lat", "lon"), output_2d)})
    da.coords["lat"] = (("lat"), lats)
    da.coords["lon"] = (("lon"), lons)
    da.attrs = ds.attrs

    # Cleaning up
    ds.close()
    del ds, fgz, sparse_data, pix_x, pix_y, pix_ct, output, masked_output, output_2d

    return da


def process_input_files(fs, timestamp, files_qpe, files_conf, config, logger, verbose=False):

    logger.info("---> reading Radar Input {} {}".format(
        timestamp,
        files_qpe['files'].loc[files_qpe.Timestamp == timestamp].iloc[0])
    )
    # reads input RADAR qpe and confidence grid
    conf_da = read_sparse_nc(
        fs, files_conf['FullPath'].loc[files_conf.Timestamp == timestamp].iloc[0],
        var='RadarConfidence', latsize=config['rdr_latsize'],
        lonsize=config['rdr_lonsize']
    )
    qpe_da = read_sparse_nc(
        fs, files_qpe['FullPath'].loc[files_qpe.Timestamp == timestamp].iloc[0],
        var='qpe60min', latsize=config['rdr_latsize'], lonsize=config['rdr_lonsize']
    )
    qpe_da['RadarConfidence'] = (("lat", "lon"), conf_da['RadarConfidence'].values[:])
    qpe_da = qpe_da.fillna(0.0)
    get_binary_precip_class(qpe_da, tgt_var='qpe60min', precip_tshld=config['ml_precip_tshold'])

    # reads input SAT data
    logger.info("---> reading SAT data {}".format(timestamp))

    sat_data = get_sat_bands_s3_multiprocess(
        fs,
        logger,
        timestamp,
        hour_interval=int(config['ml_hr_interval']),
        s3bucket=config['sat_s3_ingest'],
        satkey=config['ml_source_sats'],
        verbose=verbose,
        satbands=config['ml_sat_bands'],
        slice_area=config['ml_domain_edges'],
        output_type='mean',
        file_regex=config['sat_file_regex'],
        tstamp_fmt=config['sat_tstamp_fmt']
    )

    # interpolates target data to satellite grid
    interp_ylabel2sat(sat_data, qpe_da, tgt_var='is_precip', msk_var='RadarConfidence')

    # Generates the mask to ingest data
    get_ingest_mask(
        sat_data,
        dx=config['ml_dx_mask'],
        tgt_var='is_precip', msk_var='RadarConfidence', msk_tshld=0.9, pcp_tshld=0)

    del conf_da, qpe_da
    return sat_data

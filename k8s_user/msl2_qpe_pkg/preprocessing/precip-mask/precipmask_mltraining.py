#! /usr/bin/env python3
#   DTN, LLC - MetStormLive2 - Satellite-based precipitation mask
#   Machine-Learning model training
#   Remote Sensing and ICON Teams [Ric Domingues, Jul, 2021]
##############################################################################
import logging.config
import tempfile
import os
from optparse import OptionParser
import precipmask_utils as utils

import tensorflow as tf
import pandas as pd
import numpy as np
from sklearn.preprocessing import MinMaxScaler
from joblib import dump, load


"""
SPECIFIC FUNCTIONS
"""


def build_model(logger, layer_nodes=100, input_shape=0, output_size=1):

    """
    Satellite-based Precipation-Mask Machine-Learning Model
    Version: 1.0 (Jul, 2021)
    Returns:
        model: Tensorflow v2 Neural Network Model
    """

    model = tf.keras.Sequential(
        [
            tf.keras.layers.Dense(
                layer_nodes,
                activation="tanh",
                input_shape=input_shape
            ),
            tf.keras.layers.Dropout(0.5),
            tf.keras.layers.Dense(
                layer_nodes,
                activation="tanh",
                kernel_regularizer="l2"
            ),
            tf.keras.layers.Dense(
                layer_nodes,
                activation="tanh",
                kernel_regularizer="l2"
            ),
            tf.keras.layers.Dropout(0.5),
            tf.keras.layers.Dense(
                layer_nodes,
                activation="tanh",
                kernel_regularizer="l2"
            ),
            tf.keras.layers.Dense(
                layer_nodes,
                activation="tanh",
                kernel_regularizer="l2"
            ),
            tf.keras.layers.Dropout(0.3),
            tf.keras.layers.Dense(layer_nodes, activation="tanh"),
            tf.keras.layers.Dense(output_size, activation="sigmoid")
        ]
    )

    model.compile(
        optimizer=tf.keras.optimizers.Adagrad(),
        loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
        metrics=[
            tf.keras.losses.BinaryCrossentropy(
                from_logits=True,
                name="binary_crossentropy"
            ),
            "accuracy"
        ]
    )

    model.summary(print_fn=logger.info)

    return model


"""
CORE RUN
"""

if __name__ == "__main__":
    usage = ""
    p = OptionParser(usage=usage)
    p.add_option("--verbose", "-v", dest="verbose", default=False,
                 action="store_true",
                 help="increase logging to the DEBUG level.")
    p.add_option("-c", dest="config_file",
                 default="./config/precip-mask_config.yaml",
                 help="Path to Precipitation Mask Default Config File")
    p.add_option("-l", dest="lookback", default=0,
                 help="Number of frames to ingest during training")
    p.add_option("-i", dest="initialize", default=False,
                 action="store_true", help="Initialize a new ML model")
    p.add_option("--msl_config", dest="msl_config_file",
                 default="../../config/msl2.json",
                 help="Path to Precipitation Mask Default Config File")
    opts, args = p.parse_args()

    tnow = pd.Timestamp.utcnow()

    # Loading Configurations
    msl_config = utils.load_json_config(cfg_file=opts.msl_config_file)

    #
    # Intialize Logging
    logging.config.fileConfig("./logging.ini", disable_existing_loggers=False)
    logger = logging.getLogger(__name__)

    #
    # Loading Configurations
    config = utils.load_config(logger, cfg_file=opts.config_file)
    # overwrites test config from precip-mask
    config["general_config"]["s3_output_bkt"] = os.getenv(
                            "MSL2_INTERMEDIATE_BUCKET",
                            "metstormlive2-intermediate-dev"
    )

    # Print Current Configurations
    logger.info("BEGIN: {}".format(config["general_config"]["title"]))
    logger.info("ML TRAINING, Model Version: {}".format(
        config["general_config"]["version"])
    )

    #
    # Intializing AWS s3 clients
    logger.info("...Intializing boto3 client")
    s3_client = utils.init_s3botoclient()

    logger.info("...Intializing s3 FileSystem")
    s3_fs = utils.init_s3FileSystem()

    """
    LOOPING THROUGH THE DIFFERENT DOMAINS
    """

    for domain_cfg in msl_config["domains"]:

        domain = domain_cfg["abbreviated_name"]
        #
        # Only train models marked for training with is_ml_training=True
        if not config[domain]["is_ml_training"]:
            logger.info("=> training NOT configured to {}, skipping -->>".format(domain))
            continue

        logger.info("=> PROCESSING domain {}".format(domain))
        logger.info(" - Satellite source: {}".format(
            config[domain]["sat_s3_ingest"])
        )
        logger.info(" - Label Data Source: {}".format(
            config[domain]["rdr_s3_ylabls"])
        )

        # loading from general MSL2 config
        # retrieve accumulation interval
        config[domain]["ml_hr_interval"] = int(int(domain_cfg["temporal_resolution"]) / 60)

        ml_training_latency = pd.Timedelta(
                                  config[domain]["ml_training_latency"],
                                  unit="H")
        tlatency = (tnow - ml_training_latency).tz_localize(None)

        #
        # Intializing NEW model /
        # otherwise loads previously trained model and history
        early_stop = tf.keras.callbacks.EarlyStopping(
            monitor=config["ML_parameters"]["loss_monitor"],
            patience=config["ML_parameters"]["patience"],
            restore_best_weights=True
        )

        # if model and training hist already exist, they are named as
        filename_model, filename_hist, filename_scaler = utils.load_default_s3_filenames(
            config, domain
        )

        is_model = s3_fs.exists("{}/{}".format(
            config["general_config"]["s3_output_bkt"],
            filename_model)
        )
        is_hist = s3_fs.exists("{}/{}".format(
            config["general_config"]["s3_output_bkt"],
            filename_hist)
        )
        if opts.initialize or not is_model:
            logger.info("---> iniatilzing NEW Tensorflow MODEL")
            opts.initialize = True
            model = build_model(
                logger,
                layer_nodes=config["ML_parameters"]["ml_numb_nodes"],
                input_shape=[len(config[domain]["ml_sat_bands"])],
                output_size=1
            )
            frames_ingested = 0
        else:
            logger.info(
                "---> loading EXISTING Tensorflow"
                "MODEL and training history"
            )

            if is_model and is_hist:

                filetmp_model_dn = tempfile.NamedTemporaryFile()
                filetmp_model_dn.name += ".h5"
                s3_client.download_file(
                    config["general_config"]["s3_output_bkt"],
                    filename_model, filetmp_model_dn.name
                )
                model = tf.keras.models.load_model(
                            filetmp_model_dn.name,
                            compile=False
                        )
                model.compile(
                    optimizer=tf.keras.optimizers.Adagrad(),
                    loss=tf.keras.losses.BinaryCrossentropy(
                        from_logits=True
                    ),
                    metrics=[
                                tf.keras.losses.BinaryCrossentropy(
                                    from_logits=True,
                                    name="binary_crossentropy"
                                ),
                                "accuracy"
                            ]
                )

                s3_file = "{}/{}".format(
                    config["general_config"]["s3_output_bkt"],
                    filename_hist
                )

                try:
                    fobj = s3_fs.open(s3_file)
                except FileNotFoundError:
                    s3_fs.invalidate_cache()
                    fobj = s3_fs.open(s3_file)
                fit_hist = pd.read_feather(fobj)

                frames_ingested = int(
                    fit_hist["frames_ingested"].tail(1).values[0]
                )
                last_ingest = pd.Timestamp(
                    fit_hist["last_ingest"].tail(1).values[0]
                )
            else:
                logger.error(
                    "Model or Training HIST not found in s3: \n MODEL "
                    "file: {}  \n TrainingHist file: {}".format(
                        filename_model, filename_hist
                    )
                )
                continue

        #
        # List files / timestamps to process
        logger.info("---> Listing Available Labels {}".format(
            config[domain]["ml_label_descrip"])
        )
        if domain == "CC":
            files_qpe = utils.list_radar_files_s3(
                s3_fs,
                bucket_dir=config[domain]["rdr_s3_ylabls"])

            files_conf = utils.list_radar_files_s3(
                s3_fs,
                bucket_dir=config[domain]["rdr_s3_conf"])

        if int(opts.lookback) > 0:
            logger.info(" -> LOOKBACK passed, training with {} snapshots".format(opts.lookback))
            # overwrite default config
            config[domain]["ml_training_nframes"] = int(opts.lookback)

        if opts.initialize:
            timestamp_to_process = files_qpe["Timestamp"].loc[
                files_qpe["Timestamp"] < tlatency].tail(
                    config[domain]["ml_training_nframes"]
                )
        else:
            timestamp_to_process = files_qpe["Timestamp"].loc[
                (files_qpe["Timestamp"] > last_ingest) &
                (files_qpe["Timestamp"] < tlatency)
            ].head(config[domain]["ml_training_nframes"])

        #
        # LOOP through timestamps
        for t in timestamp_to_process:
            logger.info(" -> TRAINING with snapshot {}".format(t))

            try:
                goes_data = utils.process_input_files(
                    s3_fs,
                    t,
                    files_qpe,
                    files_conf,
                    config[domain],
                    logger,
                    verbose=opts.verbose
                )

                X_train, Y_train = utils.gather_TrainingData(goes_data, tgt_var="is_precip")
                #
                # LOADING THE SCALAR OR FITTING THE SCALAR

                if frames_ingested == 0:
                    scaler = MinMaxScaler()
                    X_train_scalled = scaler.fit_transform(X_train, y=None)
                    logger.info(" -> INIT scaller here")
                    filetmp_scaler_up = tempfile.NamedTemporaryFile()
                    dump(scaler, filetmp_scaler_up.name)
                    logger.info("---- s3 saving {}".format(filename_scaler))
                    s3_client.upload_file(
                        Filename=filetmp_scaler_up.name,
                        Bucket=config["general_config"]["s3_output_bkt"],
                        Key=filename_scaler,
                        ExtraArgs={"ACL": "bucket-owner-full-control"}
                    )

                else:
                    logger.info("---> LOADING previously fitted scaller {}".format(
                        filename_scaler)
                    )

                    s3_file = "{}/{}".format(
                        config["general_config"]["s3_output_bkt"],
                        filename_scaler)

                    try:
                        fobj = s3_fs.open(s3_file)
                    except FileNotFoundError:
                        s3_fs.invalidate_cache()
                        fobj = s3_fs.open(s3_file)

                    scaler = load(fobj)
                    X_train_scalled = scaler.transform(X_train)

                logger.info("---> TRAINING precip-mask model")
                train_incr = model.fit(
                    x=X_train_scalled, y=Y_train,
                    epochs=config["ML_parameters"]["max_epoch"],
                    validation_split=config["ML_parameters"]["validation_split"],
                    verbose=1,
                    callbacks=[early_stop],
                    shuffle=True,
                    batch_size=config["ML_parameters"]["batch_size"]
                )

                loss_ones = np.ones(len(train_incr.history["loss"]))
                train_incr.history["frames_ingested"] = list(loss_ones *
                                                             (frames_ingested + 1))
                train_incr.history["last_ingest"] = t

                fit_hist = (
                    pd.DataFrame(train_incr.history) if frames_ingested == 0
                    else pd.concat([
                        fit_hist,
                        pd.DataFrame(train_incr.history)
                    ], ignore_index=True)
                )
                logger.info(fit_hist.tail(10))

                frames_ingested += 1
            except Exception as e:
                logger.error("Error ingesting timestamp {0}; skipping it ->>".format(t))
                logger.error(e)
                continue
        #
        # SAVING UPDATED MODEL & TRAINING HISTORY TO S3 =======================
        filename_model_upd, filename_hist_upd = utils.load_latest_s3_filenames(
                    config, domain, frames_ingested, t
        )

        filetmp_model_up = tempfile.NamedTemporaryFile()
        filetmp_model_up.name += ".h5"
        logger.info("---> s3 saving {}".format(filename_model_upd))
        model.save(filetmp_model_up.name)
        s3_client.upload_file(
            Filename=filetmp_model_up.name,
            Bucket=config["general_config"]["s3_output_bkt"],
            Key=filename_model_upd,
            ExtraArgs={"ACL": "bucket-owner-full-control"}
        )
        s3_client.upload_file(
            Filename=filetmp_model_up.name,
            Bucket=config["general_config"]["s3_output_bkt"],
            Key=filename_model,
            ExtraArgs={"ACL": "bucket-owner-full-control"}
        )
        #
        # ------- TRAINING HIST
        filetmp_hist_up = tempfile.NamedTemporaryFile()

        logger.info("---> s3 saving {}".format(filename_hist_upd))

        fit_hist.to_feather(filetmp_hist_up.name)
        s3_client.upload_file(
            Filename=filetmp_hist_up.name,
            Bucket=config["general_config"]["s3_output_bkt"],
            Key=filename_hist_upd,
            ExtraArgs={"ACL": "bucket-owner-full-control"}
        )
        s3_client.upload_file(
            Filename=filetmp_hist_up.name,
            Bucket=config["general_config"]["s3_output_bkt"],
            Key=filename_hist,
            ExtraArgs={"ACL": "bucket-owner-full-control"}
        )

    logger.info("END")

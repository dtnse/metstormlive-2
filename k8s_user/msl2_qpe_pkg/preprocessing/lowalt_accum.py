from datetime import datetime, timedelta
import tempfile

import xarray as xr
import numpy as np

from msl2_qpe_pkg.base import message as base_message

def write_nc(opts, bucket_obj, ds, dt, domain, minutes=60, increments=None, build_tiles=False):

    MISSING_VALUE = -9999.0
    PRECISION = 4

    region = domain["abbreviated_name"]
    complevel = domain["nc_compression_level"]

    # value rounding
    ds['Precipitation_from_Lowalt'] = \
        ds['Precipitation_from_Lowalt'].round(decimals=PRECISION)

    # load_dataset saved FillValue from source as NaNs,
    # set correct missing value flag before writing out
    ds['Precipitation_from_Lowalt'].values[
        np.isnan(ds['Precipitation_from_Lowalt'])] = MISSING_VALUE

    # we're reusing attributes from an input 5min file,
    # make the needed adjustments here
    ds.Precipitation_from_Lowalt.attrs[
        "coordinates"] = "lat lon"  # setting from source file not retained for some reason
    ds.Precipitation_from_Lowalt.attrs["least_significant_digit"] = PRECISION
    ds.Precipitation_from_Lowalt.attrs["deflate"] = 1
    ds.Precipitation_from_Lowalt.attrs["deflate_level"] = complevel
    ds.Precipitation_from_Lowalt.attrs["_FillValue"] = MISSING_VALUE
    # unique to 1hr output file
    if minutes == 60:
        ds.attrs["title"] = "Precipitation_from_LowAltitude_reflectivity_1hr"
        ds.Precipitation_from_Lowalt.attrs[
            "description"] = "Used M-P ZR to convert to precipitation & accumulated " \
                             "following 5min increments: %s" % str(increments)
        ds.Precipitation_from_Lowalt.attrs[
            "long_name"] = "Precipitation_from_LowAltitude_reflectivity_1hr"

    # set correct deflate level for other arrays
    ds.lat.attrs["deflate"] = 1
    ds.lat.attrs["deflate_level"] = complevel
    ds.lon.attrs["deflate"] = 1
    ds.lon.attrs["deflate_level"] = complevel

    # rebuild time attributes, setting correct valid time
    ds["time"] = 0.0
    ds.time.attrs["standard_name"] = "time"
    ds.time.attrs["units"] = f"minutes since {dt:%Y-%m-%dT%H:%M:%SZ}"
    ds.time.attrs["string"] = f"{dt:%Y-%m-%dT%H:%M:%SZ}"
    ds.time.attrs["period_string"] = "0 minutes"

    ds["forecast_reference_time"] = 0.0
    ds.forecast_reference_time.attrs["standard_name"] = "forecast_reference_time"
    ds.forecast_reference_time.attrs["string"] = f"{dt:%Y-%m-%dT%H:%M:%SZ}"
    ds.forecast_reference_time.attrs["period_string"] = "0 minutes"
    ds.forecast_reference_time.attrs["units"] = f"minutes since {dt:%Y-%m-%dT%H:%M:%SZ}"

    prefix = f"preprocessed/{dt:%Y%m%d_%H}"
    ncfile = f"{region}_Z2P_{dt:%Y%m%d_%H%M}_005mn.nc"
    if minutes == 60:
        ncfile = f"{region}_Z2P_{dt:%Y%m%d_%H%M}_001hr.nc"

    # compress all variables and save output to new netcdf file
    comp = dict(zlib=True, complevel=domain['nc_compression_level'])
    encoding = {var: comp for var in ds.data_vars}

    s3url = f"s3://{opts.MSL2_INTERMEDIATE_BUCKET}/{prefix}/{ncfile}"

    with tempfile.NamedTemporaryFile() as nc_tmpfile:
        ds.to_netcdf(nc_tmpfile.name, encoding=encoding)
        bucket_obj.upload_file(Filename=nc_tmpfile.name, Key=f"{prefix}/{ncfile}")
        opts.logger.info(f"PreprocessUpload: {s3url}")
    
    if build_tiles:
        message_body = base_message.compose_message("PPR_HANDLER",s3url)
        is_msg       = base_message.send_message(opts.sqs_obj,message_body)
        if is_msg:
            opts.logger.info(f" - NEW-MSG {region} message SENT for {s3url}")

    return


def accum(opts, queue_listener, valid_dt=datetime.utcnow(), build_tiles=True):
    """
    Author: Nathan Kelly (Nathan.Kelly@dtn.com)
    Creation Date: 2021-10-20
    Development Notes:

    Script to calculate the 60min accumulation of the lowest altitude radar scan
    using the Marshall-Palmer Z-R power law relation. Script sums precip
    from :05 to :60 of each hour. Next section checks for missing files
    from the summation interval and notes missing files as "missing" in time array.
    If there are more than (half) files missing, it returns without doing anything
    Then we check if the first file is missing, and if it is one is created one.
    This is done by identifying the closest non-missing preceding file
    and using it to form a weighted average with the closest non-missing successive file,
    with a linear weight based on time. Note that if the entire previous hours files are missing,
    this will fail.

    Should we decide on a maximum temporal distance that can be used to fill in for the first file?
    The sum is now calculated, using the same weighted average used above.
    If there is no following files for the weighted average, the closest preceding file is used.
    That was the simplest way to do it for now, I am not sure that is the best way.
    Perhaps there should be some ramp down to zero if multiple 5min accumulations
    at the end of an hour are missing? Or a maximum number of consecutive missing files
    that are being filled by duplication before a file is simply not generated?
    Thankfully it seems (at least when I have been working on it)
    that hours with many missing files are rare.
    the status variable gives the number and times of missing files

    The data is saved to 60min files using the same naming convention
    as the 5min files but substituting 60 for 5 in the file name. This code is copied almost
    verbatim from the previous lowalt_accum script, but with variable names changed to what I used.
    I am not too familiar with this aspect, so hopefully this does the job.

    """
    bucket = queue_listener.MSL2_INTERMEDIATE_BUCKET
    bucket_obj = queue_listener.s3.Bucket(bucket)
    # create s3_fs variable in the function scope for convenience
    s3_fs = queue_listener.s3_fs
    # make sure we have the most recent files
    s3_fs.invalidate_cache()

    for domain_id in queue_listener.config["domains"]:

        domain = queue_listener.config["domains"][domain_id]
        region = domain["abbreviated_name"]

        if region == "BR":
            continue
        opts.logger.info(f" accumLowalt: Processing region {region}")
        now_dt = valid_dt.replace(minute=0, second=0, microsecond=0)

        object_prefix = f"/preprocessed/{now_dt:%Y%m%d_%H}/{region}_Z2P_{now_dt:%Y%m%d_%H}*005mn*"
        top_hr_files = s3_fs.glob(bucket + object_prefix)

        if (len(top_hr_files) != 0) and (int(top_hr_files[0][-11:-9]) == 0):
            opts.logger.info("top of the hour file not missing.")
        else:
            opts.logger.info("top of the hour file missing")
            if len(top_hr_files) == 0:
                top_hr_files = ["missing"]
            else:
                top_hr_files[0] == "missing"

        dt_sum = now_dt - timedelta(hours=1)
        object_prefix = f"/preprocessed/{dt_sum:%Y%m%d_%H}/{region}_Z2P_{dt_sum:%Y%m%d_%H}*005mn*"
        opts.logger.info(f'Retrieve files with this prefix: {bucket}{object_prefix}')
        sum_hr_files = s3_fs.glob(bucket + object_prefix)
        if top_hr_files[0] != "missing":
            sum_hr_files.append(top_hr_files[0])
        opts.logger.info('Files to sum: %s' % str(sum_hr_files))

        if (len(sum_hr_files) > 0):
            if (sum_hr_files[0][-11:-9] == "00"):
                sum_hr_00 = sum_hr_files.pop(0)
            else:
                sum_hr_00 = "missing"
        else:
            sum_hr_00 = "missing"

        minutes_array = np.arange(5, 65, 5)

        # deal with missing files by replacing the missing file name with "missing"
        # get array of the minutes of the files that we have

        f_minutes = [int(f[-11:-9]) for f in sum_hr_files if f[-11:-9]]
        if f_minutes[-1] == 0:
            f_minutes[-1] = 60
        # check if we have every 5min period
        if len(sum_hr_files) == 12:
            opts.logger.info(f" accumLowalt: all 12 files present for domain {region}")
            missing = []
        # find the missing files
        elif len(sum_hr_files) < 6:
            opts.logger.info(f" accumLowalt: More than half missing for domain {region}. "
                        f"Trying next domain")
            continue
        else:
            # Check which of the expected full set of 5-minute end times is in the list of files
            missing = minutes_array[~np.isin(minutes_array, f_minutes)]

            if len(missing) > 0:
                opts.logger.info(f" accumLowalt: {region} is missing files at {missing} minutes after "
                            f"the hour")
            # insert the word "missing" where there are missing files in the sequence
            # so they can be properly filled
            for m in missing:
                x = np.where(m == minutes_array)[0][0]
                sum_hr_files.insert(x, "missing")

        missing_idx = (np.array(sum_hr_files) == "missing")
        # need an array of file names not populated by missing for later when
        #  we find files before and after a missing file
        files_pre_missing = np.array(sum_hr_files)[~missing_idx]

        # if the missing file is the first accum of the hour, then we need to get the data from the
        # previous hour to fill
        if (len(missing) > 0) and missing[0] == 5:

            # check if we need to get more data from the previous hour's accumulation
            # other than just the last 5mn period
            if sum_hr_00 == 'missing':

                # get the data from two hours ago
                dt_prev_2 = now_dt - timedelta(hours=2)
                object_prefix = f"/preprocessed/{dt_prev_2:%Y%m%d_%H}/CC_Z2P_{dt_prev_2:%Y%m%d_%H}*"
                prev_hr_files = s3_fs.glob(bucket + object_prefix)
                prev_f_minutes = []
                # loop through the files from two hours ago to see what files we have there
                for f in prev_hr_files:
                    prev_f_minutes.append(int(f[-11:-9]))
                # find the file closest to the end of the hour
                closest_non_miss_prev = np.amax(prev_f_minutes)
                prev_file = prev_hr_files[np.where(prev_f_minutes == closest_non_miss_prev)[0][0]]
                # find the first non-missing file of the hour we are summng
                closest_non_miss_sumhr = np.amin(f_minutes)
                next_file = files_pre_missing[np.where(f_minutes == closest_non_miss_sumhr)[0][0]]
                # find the difference between the closest to end of hour and the missing
                # accumulation ending at 5mn
                prev_dif = (65 - closest_non_miss_prev)
                # find the difference between the closest file in the sumhrent hour and the missing
                # accum ending 5mn after the hour
                next_dif = closest_non_miss_sumhr - 5

                with s3_fs.open(prev_file, "rb") as f:
                    ds = xr.load_dataset(f)
                    z2p_60min = ds.copy()

                with s3_fs.open(next_file, "rb") as f:
                    ds_1 = xr.load_dataset(f)
                # Calculate the first 5mn file as the weighted average of the nearest non-missing
                # files before and after the time.
                z2p_60min.Precipitation_from_Lowalt.values =\
                    ds.Precipitation_from_Lowalt.values*(float(prev_dif)/(prev_dif + next_dif)) +\
                    ds_1.Precipitation_from_Lowalt.values*(float(next_dif)/(prev_dif + next_dif))

            else:
                # if we have the last file from the previous hour
                prev_dif = 5
                next_dif = np.amin(f_minutes)-5
                # Filling in the 5min after the hour file
                # and if its missing then the minimum f_minutes
                # will be the next valid file
                closest_non_miss_sumhr = np.amin(f_minutes)
                next_file = files_pre_missing[np.where(f_minutes == closest_non_miss_sumhr)[0][0]]
                prev_file = sum_hr_00
                with s3_fs.open(sum_hr_00, "rb") as f:
                    ds = xr.load_dataset(f)
                    z2p_60min = ds.copy()

                with s3_fs.open(next_file, "rb") as f:
                    ds_1 = xr.load_dataset(f)
                # Calculate the first 5mn accumulation as weighted average
                # before and first non-missing after
                z2p_60min.Precipitation_from_Lowalt.values =\
                    ds.Precipitation_from_Lowalt.values*(float(prev_dif)/(prev_dif + next_dif)) +\
                    ds_1.Precipitation_from_Lowalt.values*(float(next_dif)/(prev_dif + next_dif))

        # load first file
        first_valid = [(idx, fname) for idx, fname in enumerate(sum_hr_files)
                       if fname != "missing"][0]
        if first_valid[0] == 0:
            with s3_fs.open(first_valid[1], "rb") as f:
                z2p_60min = xr.load_dataset(f)
        elif first_valid[0] == 1:
            if sum_hr_00 == "missing":
                dt_prev = datetime.utcnow() - timedelta(hours=2)
                object_prefix =\
                    f"/preprocessed/{dt_prev:%Y%m%d_%H}/{region}_Z2P_{dt_prev:%Y%m%d_%H}*"
                prev_hr_files = s3_fs.glob(bucket + object_prefix)
                prev_f_minutes = []
                # loop through the files from two hours ago to see what files we have there
                for f in prev_hr_files:
                    prev_f_minutes.append(int(f[-11:-9]))
                # find the file closest to the end of the hour
                closest_non_miss_prev = np.amax(prev_f_minutes)
                prev_file = prev_hr_files[np.where(prev_f_minutes == closest_non_miss_prev)[0][0]]
                # find the first non-missing file of the hour we are summng
                closest_non_miss_sumhr = np.amin(f_minutes)
                next_file = files_pre_missing[np.where(f_minutes == closest_non_miss_sumhr)[0][0]]
                # find the difference between the closest to end of hour and
                #  the missing accumulation ending at 5mn
                prev_dif = (65 - closest_non_miss_prev)
                # find the difference between the closest file in the sumhrent hour and
                #  the missing accum ending 5mn after the hour
                next_dif = closest_non_miss_sumhr - 5

                with s3_fs.open(prev_file, "rb") as f:
                    ds = xr.load_dataset(f)
                    z2p_60min = ds.copy()

                with s3_fs.open(next_file, "rb") as f:
                    ds_1 = xr.load_dataset(f)
                # Calculate the first 5mn file as the weighted average of the nearest non-missing
                # files before and after the time.
                z2p_60min.Precipitation_from_Lowalt.values = ds.Precipitation_from_Lowalt.values * \
                    (float(prev_dif)/(prev_dif + next_dif)) + \
                    ds_1.Precipitation_from_Lowalt.values*(float(next_dif)/(prev_dif + next_dif))

            else:
                # if we have the last file from the previous hour
                prev_dif = 5
                next_dif = np.amin(f_minutes)-5
                closest_non_miss_sumhr = np.amin(f_minutes)
                next_file = files_pre_missing[np.where(f_minutes == closest_non_miss_sumhr)[0][0]]
                prev_file = sum_hr_00
                with s3_fs.open(sum_hr_00, "rb") as f:
                    ds = xr.load_dataset(f)
                    z2p_60min = ds.copy()
                with s3_fs.open(next_file, "rb") as f:
                    ds_1 = xr.load_dataset(f)
                # Calculate the first 5mn accumulation as weighted average before
                #  and first non-missing after
                z2p_60min.Precipitation_from_Lowalt.values = ds.Precipitation_from_Lowalt.values * \
                    (float(prev_dif)/(prev_dif + next_dif)) + \
                    ds_1.Precipitation_from_Lowalt.values*(float(next_dif)/(prev_dif + next_dif))

                z2p_5min = z2p_60min.copy()
                z2p_5min.Precipitation_from_Lowalt.values = ds.Precipitation_from_Lowalt.values * \
                    (float(prev_dif)/(prev_dif + next_dif)) + \
                    ds_1.Precipitation_from_Lowalt.values*(float(next_dif)/(prev_dif + next_dif))

                write_nc(opts,bucket_obj, z2p_5min, dt_sum.replace(minute=5), domain, minutes=5,
                         build_tiles=False)

        for idx, accum_5min in enumerate(sum_hr_files[first_valid[0] + 1:]):
            tm = minutes_array[idx]
            # Need to make the time difference math work
            # -- NOTE: Is tm ever 0 ???
            if tm == 0:
                # have to make np int64 or the subtraction complains
                tm = np.int64(60)
            opts.logger.info(f" accumLowalt: Processing {tm} minutes after the hour")
            if accum_5min != "missing":
                with s3_fs.open(accum_5min, "rb") as f:
                    ds = xr.load_dataset(f)
                    values_next = ds.Precipitation_from_Lowalt.values
                    z2p_60min.Precipitation_from_Lowalt.values = (
                        z2p_60min.Precipitation_from_Lowalt.values + values_next
                    )
            else:
                # Here need to find where the closest non-missing files are
                # make a copy of f_minutes because we may need to use it again
                file_minutes = f_minutes

                find_closest_minutes = file_minutes - tm
                try:
                    prev_diff = np.amax(find_closest_minutes[find_closest_minutes < 0])
                    prev_file_w = np.where(find_closest_minutes == prev_diff)[0][0]
                    prev_file = files_pre_missing[prev_file_w]
                    prev_diff = abs(prev_diff)
                except (IndexError, ValueError):
                    if len(prev_file[0]) == 0:
                        # This is the previous file from the previous hour,
                        # to be used if there are no previous files in the sumhrent hour
                        prev_diff = prev_diff + tm
                try:
                    next_diff = np.amin(find_closest_minutes[find_closest_minutes > 0])
                    next_file = files_pre_missing[np.where(find_closest_minutes == next_diff)[0][0]]
                except (IndexError, ValueError):
                    # if there are no files in the sumhrent hour after the missing hour
                    # that can be used, duplicate the nearest previous file
                    next_file = prev_file
                    # doesn"t matter for this one since we are just duplicating the previous file
                    next_diff = prev_diff
                with s3_fs.open(prev_file, "rb") as f:
                    prev_ds = xr.load_dataset(f)
                with s3_fs.open(next_file, "rb") as f:
                    next_ds = xr.load_dataset(f)

                weight = float(prev_diff)/(prev_diff + next_diff)
                # perform weighted average
                z2p_60min.Precipitation_from_Lowalt.values = (
                    z2p_60min.Precipitation_from_Lowalt.values +
                    ((prev_ds.Precipitation_from_Lowalt.values * weight) +
                     (next_ds.Precipitation_from_Lowalt.values * weight))
                )

                z2p_5min = z2p_60min.copy()
                z2p_5min.Precipitation_from_Lowalt.values = (
                    (prev_ds.Precipitation_from_Lowalt.values * weight) +
                    (next_ds.Precipitation_from_Lowalt.values * weight))

                if tm < 60:
                    write_nc(opts,bucket_obj, z2p_5min, dt_sum.replace(minute=tm), domain, minutes=5,
                             build_tiles=False)
                elif tm == 60:
                    write_nc(opts,bucket_obj, z2p_5min, now_dt, domain, minutes=5, build_tiles=False)

        # Save the 60min file
        write_nc(opts,bucket_obj, z2p_60min, now_dt, domain, minutes=60, increments=f_minutes,
                 build_tiles=build_tiles)

    return True

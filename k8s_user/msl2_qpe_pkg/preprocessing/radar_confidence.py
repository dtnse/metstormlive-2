"""
Radar Confidence Preprocessing

confidence.py

Purpose: Reads sparse gridded radar confidence file from S3 and converts to NetCDF CF 1.6

Written: Brandon T, Jan. 2022

Modifications:

ICON-299 - 6 Jan 2022 - bt
ICON-379 - 26 May 2022 - tr
"""

from datetime import datetime, timedelta
import gzip
import tempfile

import numpy as np
import xarray as xr

from msl2_qpe_pkg.base import output as base_outputs
from msl2_qpe_pkg.base import message as base_message


def find_nearest_idx(array, value):
    # helper function to locate idx of nearest value in array
    return (np.abs(array - value)).argmin()


def process(opts, queue_listener, valid_dt=datetime.utcnow(), build_tiles=True):
    """
    Example S3 URI:
    s3://mg-radar-gridded-dtn-radar-prod-euw1/outgoing/NorthAmerica/RadarConfidence/Radar_NorthAmerica_RadarConfidence_20211101_165730.nc.gz
    """
    MISSING_VALUE = -9999.0

    bucket = "mg-radar-gridded-dtn-radar-prod-euw1"
    valid_dt = valid_dt.replace(minute=0, second=0, microsecond=0)
    prev_hour_dt = valid_dt - timedelta(hours=1)

    ds_name = "RadarConfidence"

    for domain_id in queue_listener.config["domains"]:

        domain = queue_listener.config["domains"][domain_id]
        
        rco_domain = domain["rco_domain"]
        if rco_domain is None:
            continue

        object_prefix = f"/outgoing/{rco_domain}/{ds_name}" + \
                        f"/Radar_{rco_domain}_{ds_name}_{prev_hour_dt:%Y%m%d_%H}*"

        # fetch list of last-hour files
        queue_listener.s3_fs.invalidate_cache()
        prev_hr_files = queue_listener.s3_fs.glob(bucket + object_prefix)
        source_file = prev_hr_files[-1]  # use most-recent file in the list

        opts.logger.info(
            f"confidence.py: Processing s3://{source_file} for {domain['long_name']} domain..."
        )

        with tempfile.NamedTemporaryFile() as s3_tmpfile:
            try:
                queue_listener.s3_fs.get_file(source_file, s3_tmpfile.name)
            except FileNotFoundError:
                opts.logger.info(f"File not Found! Invalidating cache and trying again: {source_file}")
                queue_listener.s3_fs.invalidate_cache()
                queue_listener.s3_fs.get_file(source_file, s3_tmpfile.name)
            else:
                # unzip and read file
                radar_confidence = None
                with gzip.open(s3_tmpfile.name) as f:
                    radar_confidence = xr.load_dataset(f, decode_cf=False, engine="h5netcdf")

                # store 1d arrays of latlons
                lats1d = radar_confidence.latitude.values[:, 0]
                lons1d = radar_confidence.longitude.values[0, :]

                # destination grid bounding coordinates
                w = domain["minx"]
                s = domain["miny"]
                e = domain["maxx"]
                n = domain["maxy"]

                # get source grid bounding coordinates of output domain, we will extract this slice
                wx = find_nearest_idx(lons1d, w)
                ex = find_nearest_idx(lons1d, e)
                sy = find_nearest_idx(lats1d, s)
                ny = find_nearest_idx(lats1d, n)

                # create 'cropped' xarray dataset, with all variables clipped to output domain
                cropped_rco = xr.Dataset({ds_name: (
                    ("lat", "lon"), radar_confidence[ds_name].values[sy:ny+1, wx:ex+1])})
                cropped_rco.coords["lat"] = ("lat", lats1d[sy:ny+1])
                cropped_rco.coords["lon"] = ("lon", lons1d[wx:ex+1])
                # forecast_reference_time attributes
                cropped_rco["forecast_reference_time"] = radar_confidence.forecast_reference_time                
                cropped_rco.forecast_reference_time.attrs[
                    "standard_name"] = "forecast_reference_time"
                cropped_rco.forecast_reference_time.attrs[
                    "string"] = f"{valid_dt:%Y-%m-%dT%H:%M:00Z}"
                cropped_rco.forecast_reference_time.attrs["period_string"] = "0 minutes"

                cropped_rco[ds_name].attrs["description"] = \
                    "Confidence on a scale from 0.0 (no confidence in radar observation) " + \
                    "to 1.0 (very confident in radar observation)."                

                # reset special flags:
                #   -99903.0 -> -9999.0: point not scanned by radar
                #   -99900.0 ->     0.0: nothing observed
                cropped_rco[ds_name].values[
                    cropped_rco[ds_name].values == -99903.0] = MISSING_VALUE
                cropped_rco[ds_name].values[
                    cropped_rco[ds_name].values == -99900.0] = 0.0

                #Writing Output
                output_cfg = {
                    "title" : "Radar_Confidence",
                    "long_name": ds_name,
                    "units" : "dimensionless"
                }

                opts.domain_id = domain_id
                opts.zulu_time = valid_dt

                s3url = base_outputs.write_output_netcdf(
                    opts, 
                    cropped_rco,
                    valid_dt,
                    queue_listener.config,
                    output_cfg,
                    array_type="multi",
                    out_var=ds_name,
                    output_type="RCO",
                    domain_id = domain_id,
                    MISSING_VALUE = MISSING_VALUE,
                    to_modi=False,
                    proc_stage="preprocessed",
                    freq_id="001hr"
                )

                output_key_default = f"s3://{opts.MSL2_INTERMEDIATE_BUCKET}/default/{domain_id}_RCO_latest.nc"

                opts.logger.info(f" - Copying {s3url} {output_key_default}")
                opts.s3_fs.copy(s3url,output_key_default)                

                message_body = base_message.compose_message("PPR_HANDLER",s3url)
                is_msg       = base_message.send_message(opts.sqs_obj,message_body)
                if is_msg:
                    opts.logger.info(f" - NEW-MSG {domain_id} message SENT for {s3url}")                    

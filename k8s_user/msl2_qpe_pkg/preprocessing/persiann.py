import os
import tempfile
import gc
import gzip
from datetime import datetime

import xarray as xr
from osgeo import gdal
import numpy as np

from msl2_qpe_pkg.base import output as base_outputs
from msl2_qpe_pkg.base import message as base_message

def process(queue_listener, opts,bucket, object_key, build_tiles=True):
    """
    Example input filename:
    dtn-sat_global_persiann_ValidTime_20220613-0100.nc.gz
    """
    MISSING_VALUE = -9999.0

    skywise = None
    start_dt = datetime.strptime(object_key.split("/")[-1].split("_")[4], "%Y%m%d-%H%M.nc.gz")
    opts.logger.info(f" persiann.py: Reading s3://{bucket}/{object_key}...")
    with queue_listener.s3_fs.open(bucket + "/" + object_key, "rb") as s3f:
        with gzip.open(s3f) as f:
            skywise = xr.load_dataset(f)

    ds_name = "persiann-qpe60min"
    persiann = xr.Dataset({
        ds_name: (("latitude", "longitude"), skywise.rain.data)
    })

    lats = skywise.lat.values
    lons = skywise.lon.values

    llcrnrlat = lats[0]
    urcrnrlat = lats[-1]
    llcrnrlon = lons[0]
    urcrnrlon = lons[-1]

    persiann.coords["longitude"] = (("longitude"), lons)
    persiann.coords["latitude"] = (("latitude"), lats)

    persiann[ds_name].rio.set_spatial_dims(
        x_dim="longitude", y_dim="latitude", inplace=True
    )

    persiann_raster = persiann[ds_name].rio.write_crs("EPSG:4326")

    zulu_time_str = datetime.strftime(start_dt, '%Y-%m-%dT%H:00:00Z')

    for region in queue_listener.config["domains"]:

        domain = queue_listener.config["domains"][region]

        persiann_raster_clipped = persiann_raster.rio.clip_box(
            minx=domain["minx"],
            miny=domain["miny"],
            maxx=domain["maxx"],
            maxy=domain["maxy"]
        )

        grid_width = domain["nx"]
        grid_height = domain["ny"]

        long_name = domain["long_name"]

        warped_nc = f"{region}.nc"

        with tempfile.NamedTemporaryFile() as tmpfile:
            persiann_raster_clipped.rio.to_raster(tmpfile.name, driver="GTiff")
            # pass to GDAL to reproject, resample and convert to NetCDF
            opts.logger.info(f"...Resampling clipped raster for: {long_name}")
            # In the raster format these bounds refer to the edges of pixels, and so
            # need to be offset by half of the dx and dy so that the output
            # (at the center of the pixel) lines up
            # with the intended grid
            # make four variables for the bounds to contain line length
            bounds_minx = domain["minx"]-(domain["grid_dx_deg"]*0.5)
            bounds_miny = domain["miny"]-(domain["grid_dy_deg"]*0.5)
            bounds_maxx = domain["maxx"]+(domain["grid_dx_deg"]*0.5)
            bounds_maxy = domain["maxy"]+(domain["grid_dy_deg"]*0.5)
            warp_options = gdal.WarpOptions(outputBounds=[bounds_minx, bounds_miny,
                                            bounds_maxx, bounds_maxy],
                                            resampleAlg="bilinear", format="NetCDF",
                                            width=grid_width, height=grid_height,
                                            srcNodata=np.nan, dstNodata=MISSING_VALUE)
            ds = gdal.Warp(warped_nc, tmpfile.name, options=warp_options)
            ds = None

        del persiann_raster_clipped
        gc.collect()

        # grab file info for metadata
        ds = gdal.Open(warped_nc)
        llcrnrlon, xres, _, urcrnrlat, _, yres = ds.GetGeoTransform()
        warped_width = ds.RasterXSize
        warped_height = ds.RasterYSize
        urcrnrlon = llcrnrlon + (warped_width * xres)
        llcrnrlat = urcrnrlat + (warped_height * yres)

        llcrnrlat = round(llcrnrlat, 1)
        llcrnrlon = round(llcrnrlon, 1)
        urcrnrlat = round(urcrnrlat, 1)
        urcrnrlon = round(urcrnrlon, 1)

        ds = None

        # update metadata and conforming to CF1.6 standards
        persiann_cf = xr.open_dataset(warped_nc, decode_cf=False)
        # rename variable to appropriate name
        persiann_cf = persiann_cf.rename({'Band1': ds_name})

        opts.domain_id = region
        opts.zulu_time = start_dt

        output_cfg = {
            "title" : 'Hourly PERSIANN-CCS',
            "long_name": 'Hourly PERSIANN-CCS',
            "units" : "mm"
        }

        s3url = base_outputs.write_output_netcdf(
            opts, 
            persiann_cf,
            opts.zulu_time,
            queue_listener.config,
            output_cfg,
            array_type="multi",
            out_var=ds_name,
            output_type="PER",
            domain_id = region,
            MISSING_VALUE = MISSING_VALUE,
            to_modi=False,
            proc_stage="preprocessed",
            freq_id="001hr"
        )

        message_body = base_message.compose_message("PREPROC_HANDLER",s3url)
        is_msg       = base_message.send_message(opts.sqs_obj,message_body)
        if is_msg:
            opts.logger.info(f" - NEW-MSG {region} message SENT for {s3url}")

        os.remove(warped_nc)

    return True

from datetime import datetime
import gzip
import tempfile

import xarray as xr
import numpy as np

from msl2_qpe_pkg.base import output as base_outputs
from msl2_qpe_pkg.base import message as base_message


def find_nearest_idx(array, value):
    # helper function to locate idx of nearest value in array
    return (np.abs(array - value)).argmin()


def process(queue_listener, opts,bucket, object_key):
    MISSING_VALUE = -9999.0
    PRECISION = 4    
    # get filename and date/time of file
    path_parts = object_key.split("/")
    file_parts = path_parts[-1].split("_")
    YYYY, MM, DD, HH, mm = (file_parts[3][0:4], file_parts[3][4:6], file_parts[3][6:8],
                            file_parts[4][0:2], file_parts[4][2:4])

    edt = datetime(int(YYYY), int(MM), int(DD), int(HH), int(mm))
    zulu_time_str = datetime.strftime(edt, '%Y-%m-%dT%H:%M:00Z')

    # Using Z = a*(R**b)
    # Transformed to R = b_coef * 10**(dbz * a_coef)
    # where:
    # a_coef = 1/(10*b)
    # b_coef = (1/a)**(1/b)
    # R is in mm/hr, so must divide by 12 for 5min
    a_coef_mp = 0.0625
    b_coef_mp = 0.036

    a_coef_cv = 0.07143
    b_coef_cv = 0.017

    # return status (False==Failure)
    status = False

    # fetch grid
    with tempfile.NamedTemporaryFile() as s3_tmpfile:
        try:
            queue_listener.s3_fs.get_file(bucket + "/" + object_key, s3_tmpfile.name)
        except FileNotFoundError:
            queue_listener.s3_fs.invalidate_cache()
            opts.logger.info(f"File not Found! Invalidating cache and trying again: {object_key}")
            queue_listener.s3_fs.get_file(bucket + "/" + object_key, s3_tmpfile.name)
        else:
            with gzip.open(s3_tmpfile.name) as f:
                lowalt = xr.load_dataset(f, decode_cf=False, engine="h5netcdf")
            # set coordinates
            lowalt.coords["lat"] = ("lat", lowalt.latitude.values[:, 0])
            lowalt.coords["lon"] = ("lon", lowalt.longitude.values[0, :])
            # fill in "missing" pixels with -30 dBZ
            lowalt.LowaltReflectivity.values[
                lowalt.LowaltReflectivity.values == -99900.] = -30
            # reset "data unavailable" flag with NaN
            lowalt.LowaltReflectivity.values[
                lowalt.LowaltReflectivity.values == -99903.] = np.nan
            # cap reflectivity to avoid hail contamination
            lowalt.LowaltReflectivity.values[
                lowalt.LowaltReflectivity.values > 55.0] = 55.0
            # convert to precip (mm)
            zr_eqn_mp = ((b_coef_mp *
                         (10 ** (lowalt.LowaltReflectivity.values * a_coef_mp))) / 12.0)
            zr_eqn_cv = ((b_coef_cv *
                         (10 ** (lowalt.LowaltReflectivity.values * a_coef_cv))) / 12.0)
            lowalt_gt30dbz = np.where(lowalt.LowaltReflectivity.values >= 30)
            lowalt.LowaltReflectivity.values = zr_eqn_mp.round(decimals=PRECISION)
            lowalt.LowaltReflectivity.values[lowalt_gt30dbz] = zr_eqn_cv[lowalt_gt30dbz]

            # reset NaN values to our output missing value flag
            lowalt.LowaltReflectivity.fillna(MISSING_VALUE)

            # reproject and clip to applicable domains
            for region in queue_listener.config["domains"]:

                domain = queue_listener.config["domains"][region]

                # region abbreviation and bounding latlon coordinates
                w = domain["minx"]
                s = domain["miny"]
                e = domain["maxx"]
                n = domain["maxy"]

                # skip if not contained within source grid extents
                if (w < lowalt.coords["lon"][0] or e > lowalt.coords["lon"][-1] or
                        s < lowalt.coords["lat"][0] or n > lowalt.coords["lat"][-1]):
                    continue

                # get source grid bounding coordinates of output domain, we will extract this slice
                wx = find_nearest_idx(lowalt.coords["lon"].values, w)
                ex = find_nearest_idx(lowalt.coords["lon"].values, e)
                sy = find_nearest_idx(lowalt.coords["lat"].values, s)
                ny = find_nearest_idx(lowalt.coords["lat"].values, n)

                # create output file name
                lowalt_reform_file = (
                    f'preprocessed/{YYYY}{MM}{DD}_{HH}/'
                    f'{region}_Z2P_{YYYY}{MM}{DD}_{HH}{mm}_005mn.nc'
                )
                # clip all variables to domain extent
                cropped_lowalt = xr.Dataset({"Precipitation_from_Lowalt": (
                    ("lat", "lon"), lowalt.LowaltReflectivity.values[sy:ny+1, wx:ex+1])})
                # assign coordinates to clipped grid
                cropped_lowalt.coords['lat'] = \
                    ("lat", lowalt.lat.values[sy:ny+1])
                cropped_lowalt.coords['lon'] = \
                    ("lon", lowalt.lon.values[wx:ex+1])
                # copy direct non 2-d variables
                cropped_lowalt['forecast_reference_time'] = lowalt.forecast_reference_time
                cropped_lowalt['time'] = lowalt.time
                cropped_lowalt['Latitude_Longitude'] = lowalt.Latitude_Longitude

                output_cfg = {
                    "title" : 'Precipitation_from_LowAltitude_reflectivity_5min',
                    "long_name": 'Precipitation_from_LowAltitude_reflectivity_5min',
                    "units" : "mm"
                }
                opts.domain_id = region
                opts.zulu_time = datetime.strptime(lowalt.time.string, "%Y-%m-%dT%H:%M:%SZ")

                s3url = base_outputs.write_output_netcdf(
                    opts, 
                    cropped_lowalt,
                    opts.zulu_time,
                    queue_listener.config,
                    output_cfg,
                    array_type="multi",
                    out_var="Precipitation_from_Lowalt",
                    output_type="Z2P",
                    domain_id = region,
                    MISSING_VALUE = MISSING_VALUE,
                    to_modi=False,
                    proc_stage="preprocessed",
                    freq_id="005mn"
                )

                message_body = base_message.compose_message("PREPROC_HANDLER",s3url)
                is_msg       = base_message.send_message(opts.sqs_obj,message_body)
                if is_msg:
                    opts.logger.info(f" - NEW-MSG {region} message SENT for {s3url}")
                    status = True

    return status

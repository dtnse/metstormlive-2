import tempfile
import logging.config
from datetime import datetime, timedelta
import gzip
import os
import multiprocessing as mp

import rioxarray  # noqa: F401
import xarray as xr
import s3fs
from osgeo import gdal
import pandas as pd
import numpy as np
import dask

from msl2_qpe_pkg.base import output as base_outputs
from msl2_qpe_pkg.base import message as base_message



def load_single_ghe(s3_bucket, current_file_datetime_str, conn):
    fs = s3fs.S3FileSystem(anon=False)

    # These contain precip rate in mm/hr.
    # Missing values should be stored as np.nan and propagate in later computations
    prefix = "outgoing/global/ghe_15min/netcdf_cf1.6/dtn-sat_global_ghe_15min_ValidTime_"
    s3_remote_key = (f"{s3_bucket}/{prefix}{current_file_datetime_str}.nc.gz")
    try:
        with fs.open(s3_remote_key) as fgz:
            with gzip.open(fgz, "rb") as fobj:
                print(f"...Opening {s3_remote_key}")
                nc_ds = xr.load_dataset(fobj, chunks="auto")
        conn.send(nc_ds)
        conn.close()
    except Exception:
        print(f' Error in GHE file load for file {s3_remote_key}')
        conn.send(False)
        conn.close()


def process(queue_listener, opts,s3_bucket, s3_object_key, build_tiles=True):
    """
    s3_bucket = s3://mg-sat-gridded-dtn-sat-prod-euw1/
    example s3_object_key:
    outgoing/global/ghe_15min/netcdf_cf1.6/dtn-sat_global_ghe_15min_ValidTime_20220201-1930.nc.gz

    """
    MISSING_VALUE = -9999.0

    input_ds_name = "rain"

    # list of data reference times
    reftimes = []
    # create a list to keep all processes
    parent_connections = []
    # create a list to keep connections
    processes = []
    # use a context, in case other libraries are using multiprocessing (i.e. tensorflow)
    ctx = mp.get_context("spawn")

    timesteps = []

    file_datetime_str = s3_object_key.split("/")[-1].split("_")[-1]
    file_datetime = datetime.strptime(file_datetime_str, "%Y%m%d-%H%M.nc.gz")
    for interval in range(4):
        current_file_datetime = file_datetime - timedelta(minutes=15 * interval)
        current_file_datetime_str = current_file_datetime.strftime("%Y%m%d-%H%M")
        # create a pipe for communication
        parent_conn, child_conn = ctx.Pipe()
        parent_connections.append(parent_conn)
        process = ctx.Process(
            target=load_single_ghe,
            args=(s3_bucket, current_file_datetime_str, child_conn)
        )
        processes.append(process)
        reftimes.append(current_file_datetime)

    for process in processes:
        process.start()
    for parent_connection in parent_connections:
        timestep = parent_connection.recv()
        timesteps.append(timestep)

    # convert from 1h rate to 15-minute accumulation,
    # assumed to end at reftime
    # comment out this part for now
    # will just average what timesteps we have
    # as long as there are two

    # for step_ds in timesteps:
    #     step_ds[input_ds_name] *= 0.25
    ghe_falses = [i for i, val in enumerate(timesteps) if not val]
    if len(ghe_falses) > 2:
        opts.logger.info('GHE has less than 2 15 minute files')
        return False

    # get shape of initial dataset
    init_shape = timesteps[0][input_ds_name].values.shape
    time_dim = len(timesteps)

    # initialize array with time dimension and shape of inital dataset
    all_times = np.zeros((time_dim, init_shape[0], init_shape[1]), dtype=float)
    all_times_dask = dask.array.from_array(all_times)
    del all_times

    counter = 0
    for idx, step_ds in enumerate(timesteps):
        opts.logger.info(f"...Summing: {idx}")
        if step_ds is not False:
            all_times_dask[counter, :, :] = step_ds[input_ds_name].values
            counter += 1

    # duplicate initial dataset and use it to store sum of all times

    opts.logger.info("Calculating sum across all timesteps...")
    sum60min = timesteps[0][input_ds_name].copy(deep=True)
    sum60min.values = dask.array.mean(all_times_dask, axis=0)

    # store 1h and 15min datasets in a list for later iteration
    # test comment

    output_datasets = list()
    output_datasets.append(dict(
        dataset=xr.Dataset({
            "ghe60min": (("latitude", "longitude"), sum60min)
        }),
        reftime=file_datetime,
        ds_name="ghe60min",
        longname="Hourly Global Hydro-Estimator",
        interval="001hr"
    ))
    for idx in range(0, len(timesteps)):
        output_datasets.append(dict(
            dataset=xr.Dataset({
                "ghe15min": (("latitude", "longitude"), timesteps[idx][input_ds_name])
            }),
            reftime=reftimes[idx],
            ds_name="ghe15min",
            longname="15-Minute Global Hydro-Estimator",
            interval="015mn"
        ))

    for item in output_datasets:
        item["dataset"].coords["longitude"] = (("longitude"), timesteps[0].coords["lon"])
        item["dataset"].coords["latitude"] = (("latitude"), timesteps[0].coords["lat"])

        item["dataset"][item["ds_name"]].rio.set_spatial_dims(
            x_dim="longitude", y_dim="latitude", inplace=True
        )
        raster = item["dataset"][item["ds_name"]].rio.write_crs("EPSG:4326")

        for region in queue_listener.config["domains"]:

            domain = queue_listener.config["domains"][region]

            nc_out = f"{region}_{item['reftime']:%Y%m%d_%H%M}_{item['interval']}_out.nc"
            raster_clipped = raster.rio.clip_box(
                minx=domain["minx"],
                miny=domain["miny"],
                maxx=domain["maxx"],
                maxy=domain["maxy"]
            )
            grid_width = domain["nx"]
            grid_height = domain["ny"]

            with tempfile.NamedTemporaryFile() as tmpfile:
                raster_clipped.rio.to_raster(tmpfile.name, driver="GTiff")
                # In the raster format these bounds refer to the edges of pixels, and so
                # need to be offset by half of the dx and dy so that the output
                # (at the center of the pixel) lines up
                # with the intended grid
                # make four variables for the bounds to contain line length
                bounds_minx = domain["minx"]-(domain["grid_dx_deg"]*0.5)
                bounds_miny = domain["miny"]-(domain["grid_dy_deg"]*0.5)
                bounds_maxx = domain["maxx"]+(domain["grid_dx_deg"]*0.5)
                bounds_maxy = domain["maxy"]+(domain["grid_dy_deg"]*0.5)
                warp_options = gdal.WarpOptions(outputBounds=[bounds_minx, bounds_miny,
                                                bounds_maxx, bounds_maxy],
                                                resampleAlg="bilinear", format="NetCDF",
                                                width=grid_width, height=grid_height,
                                                srcNodata=np.nan, dstNodata=MISSING_VALUE)
                ds = gdal.Warp(nc_out, tmpfile.name, options=warp_options)
                # get domain bounds for metadata
                llcrnrlon, xres, _, urcrnrlat, _, yres = ds.GetGeoTransform()
                warped_width = ds.RasterXSize
                warped_height = ds.RasterYSize
                urcrnrlon = llcrnrlon + (warped_width * xres)
                llcrnrlat = urcrnrlat + (warped_height * yres)

                llcrnrlat = round(llcrnrlat, 1)
                llcrnrlon = round(llcrnrlon, 1)
                urcrnrlat = round(urcrnrlat, 1)
                urcrnrlon = round(urcrnrlon, 1)
                ds = None  # noqa: F841

            # update metadata and conform to the CF-1.6 standard

            nc = xr.load_dataset(nc_out, decode_cf=False)
            # rename generic Band1 to specifc ds name
            nc = nc.rename({"Band1": item["ds_name"]})

            output_cfg = {
                "title" : "NOAA Hydroestimator Precip-Mask",
                "long_name": "NOAA Hydroestimator Precip-Mask",
                "units" : "mm"
            }
            opts.domain_id = region
            opts.zulu_time = item['reftime']

            s3url = base_outputs.write_output_netcdf(
                opts, 
                nc,
                opts.zulu_time,
                queue_listener.config,
                output_cfg,
                array_type="multi",
                out_var=item["ds_name"],
                output_type="GHE",
                domain_id = region,
                MISSING_VALUE = MISSING_VALUE,
                to_modi=False,
                proc_stage="preprocessed",
                freq_id=item['interval']
            )
            
            message_body = base_message.compose_message("PREPROC_HANDLER",s3url)
            is_msg       = base_message.send_message(opts.sqs_obj,message_body)
            if is_msg:
                opts.logger.info(f" - NEW-MSG {region} message SENT for {s3url}")

            os.remove(nc_out)

    return True

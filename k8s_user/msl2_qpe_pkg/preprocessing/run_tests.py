import sys
import os
import time
from datetime import datetime, timedelta
import logging.config

from queue_listener import MSL2QueueListener
import lowalt
import qpf
import ptype
import pmask
import gauge
import ghe
import persiann
import ppr_accum
import lowalt_accum
import confidence

current = os.path.dirname(os.path.realpath(__file__))
logging.config.fileConfig('%s/logging.ini' % current, disable_existing_loggers=False)
logger = logging.getLogger(__name__)


def test_lowalt(queue_listener, test_dt=datetime.utcnow()):
    bucket = "mg-radar-gridded-dtn-radar-prod-euw1"
    object_key = (f"outgoing/NorthAmerica/MaskedLowaltReflectivity/"
                  f"Radar_NorthAmerica_MaskedLowaltReflectivity_{test_dt:%Y%m%d_%H}0000.nc.gz")
    start_time = time.time()
    lowalt.process(queue_listener, bucket, object_key)
    execution_time = (time.time() - start_time)
    passing = execution_time < 30
    logger.info(f"Execution time: {execution_time:.2f} seconds")
    return passing


def test_qpf(queue_listener: MSL2QueueListener, test_dt=datetime.utcnow()) -> bool:
    """
    Returns a boolean of whether
    the QPF code executed in under
    60 seconds for one file

    """
    dt = test_dt - timedelta(hours=1)
    dt_prog = test_dt
    bucket = "mg-fcst-nwp-dtn-1fx-prod-euw1"
    object_key = (f"outgoing/grib/0.125x0.125-global_dtn_1fx/{dt:%Y%m%dT%H}0000Z/"
                  f"{dt:%Y%m%dT%H}0000Z-precipitation_rate_mean_over_1h-"
                  f"{dt_prog:%Y%m%dT%H}0000Z.grib")

    start_time = time.time()
    logger.info(f"Testing 1FX QPF data with the following object key: {object_key}...")
    qpf.process(queue_listener, bucket, object_key, build_tiles=False)
    execution_time = (time.time() - start_time)
    logger.info(f"Execution time: {execution_time:.2f} seconds")
    passing = execution_time < 60
    return passing


def test_ptype(queue_listener: MSL2QueueListener, test_dt=datetime.utcnow()) -> bool:
    """
    Returns a boolean of whether
    the Precip Type code executed in under
    60 seconds for one file

    """
    dt = test_dt.replace(minute=0, second=0, microsecond=0)
    bucket = "mg-fcst-nwp-dtn-1fx-prod-euw1"
    object_key = (f"outgoing/grib/0.125x0.125-global_dtn_1fx/{dt:%Y%m%dT%H}0000Z/"
                  f"{dt:%Y%m%dT%H}0000Z-precipitation_type_1fx-"
                  f"{dt:%Y%m%dT%H}0000Z.grib")

    start_time = time.time()
    logger.info(f"Testing 1FX PType data with the following object key: {object_key}...")
    ptype.process(queue_listener, bucket, object_key, build_tiles=False)
    execution_time = (time.time() - start_time)
    logger.info(f"Execution time: {execution_time:.2f} seconds")
    passing = execution_time < 60
    return passing


def test_pmask(queue_listener: MSL2QueueListener, test_dt=datetime.utcnow()) -> bool:
    """
    Returns a boolean of whether
    the Precip Mask code executed in under
    300 seconds for one file

    """
    dt = test_dt.replace(minute=0, second=0, microsecond=0)
    bucket = "mg-sat-gridded-dtn-sat-prod-euw1"
    object_key = (f"outgoing/global/precip-mask/netcdf_cf1.6/"
                  f"dtn-sat_global_sat-precip-mask_ValidTime_{dt:%Y%m%d-%H%M}.nc")

    start_time = time.time()
    logger.info(f"Testing precip mask data with the following object key: {object_key}...")
    pmask.process(queue_listener, bucket, object_key)
    execution_time = (time.time() - start_time)
    logger.info(f"Execution time: {execution_time:.2f} seconds")
    passing = execution_time < 300
    return passing


def test_ghe(queue_listener: MSL2QueueListener, test_dt=datetime.utcnow()) -> bool:
    """
    Returns a boolean of whether
    the GHE code executed in under
    60 seconds for one file

    """
    dt = test_dt.replace(minute=0, second=0, microsecond=0)
    bucket = "mg-sat-gridded-dtn-sat-prod-euw1"
    prefix = "outgoing/global/ghe_15min/netcdf_cf1.6/dtn-sat_global_ghe_15min_ValidTime_"
    object_key = f"{prefix}{dt:%Y%m%d-%H%M}.nc.gz"

    start_time = time.time()
    logger.info(f"Testing GHE data with the following object key: {object_key}...")
    ghe.process(queue_listener, bucket, object_key, build_tiles=False)
    execution_time = (time.time() - start_time)
    logger.info(f"Execution time: {execution_time:.2f} seconds")
    passing = execution_time < 240
    return passing


def test_persiann(queue_listener: MSL2QueueListener, test_dt=datetime.utcnow()) -> bool:
    """
    Returns a boolean of whether
    the PERSIANN code executed in under
    60 seconds for one file

    """
    dt = test_dt.replace(minute=0, second=0, microsecond=0)

    bucket = "mg-sat-gridded-dtn-sat-prod-euw1"
    object_key = (f"outgoing/global/persiann/netcdf_cf1.6/"
                  f"dtn-sat_global_persiann_ValidTime_{dt:%Y%m%d-%H%M}.nc.gz")

    logger.info(f"Testing PERSIANN data with the following object key: {object_key}...")

    start_time = time.time()
    persiann.process(queue_listener, bucket, object_key, build_tiles=False)
    execution_time = (time.time() - start_time)
    logger.info(f"Execution time: {execution_time:.2f} seconds")
    passing = execution_time < 60
    return passing


def test_ppr_accum(queue_listener: MSL2QueueListener, test_dt=datetime.utcnow()) -> bool:
    """
    Returns a boolean of whether
    the Polarimetric Precip Rate Accum code executed in under
    120 seconds for all files.

    """
    logger.info("Testing Polarimetric Precip Rate Accum...")

    start_time = time.time()
    ppr_accum.accum(queue_listener, valid_dt=test_dt, build_tiles=False)
    execution_time = (time.time() - start_time)
    logger.info(f"Execution time: {execution_time:.2f} seconds")
    passing = execution_time < 240
    return passing


def test_lowalt_accum(queue_listener: MSL2QueueListener, test_dt=datetime.utcnow()) -> bool:
    """
    Returns a boolean of whether
    the Lowalt Accum code executed in under
    120 seconds for all files.

    """

    start_time = time.time()
    logger.info(f"Testing Lowalt Accum with the following time: {test_dt:%Y%m%dT%H%M} UTC...")
    lowalt_accum.accum(queue_listener, valid_dt=test_dt, build_tiles=False)
    execution_time = (time.time() - start_time)
    logger.info(f"Execution time: {execution_time:.2f} seconds")
    passing = execution_time < 120
    return passing


def test_radar_confidence(queue_listener: MSL2QueueListener, test_dt=datetime.utcnow()) -> bool:
    """
    Returns a boolean of whether
    the Radar Confidence code executed in under
    120 seconds for all files.

    """
    logger.info(f"Testing Radar Confidence with the following time: {test_dt:%Y%m%dT%H%M} UTC...")

    start_time = time.time()
    confidence.process(queue_listener, valid_dt=test_dt, build_tiles=False)
    execution_time = (time.time() - start_time)
    logger.info(f"Execution time: {execution_time:.2f} seconds")
    passing = execution_time < 60
    return passing


def test_gauge(queue_listener: MSL2QueueListener, test_dt=datetime.utcnow()) -> bool:
    """
    Returns a boolean of whether
    the gauge code executed in under
    900 seconds for all files.

    """
    logger.info(f"Testing Gauges with the following time: {test_dt:%Y%m%dT%H%M} UTC...")

    start_time = time.time()
    gauge.process(queue_listener, "CC", valid_dt=test_dt, minute_str="test")
    execution_time = (time.time() - start_time)
    logger.info(f"Execution time: {execution_time:.2f} seconds")
    passing = execution_time < 900
    return passing


if __name__ == "__main__":
    test_dt = datetime.utcnow() - timedelta(hours=3)
    dataset_to_test = "ALL"

    if len(sys.argv) > 2:
        if sys.argv[2] != "DEFAULT":
            test_dt = datetime.strptime(sys.argv[2], "%Y%m%dT%H")
        dataset_to_test = sys.argv[1]
    elif len(sys.argv) > 1:
        dataset_to_test = sys.argv[1]
    directory = os.getcwd()
    logger.info(f"The working directory in run_tests: {directory}")
    queue_listener = MSL2QueueListener(init_sqs=False)

    if dataset_to_test == "ALL":
        logger.info(f"Testing all datasets with time: {test_dt:%Y%m%dT%H%M} UTC ...")
        lowalt_passing = test_lowalt(queue_listener, test_dt=test_dt)
        ghe_passing = test_ghe(queue_listener, test_dt=test_dt)
        qpf_passing = test_qpf(queue_listener, test_dt=test_dt)
        persiann_passing = test_persiann(queue_listener, test_dt=test_dt)
        ppr_accum_passing = test_ppr_accum(queue_listener, test_dt=test_dt)
        lowalt_accum_passing = test_lowalt_accum(queue_listener, test_dt=test_dt)
        radar_confidence_passing = test_radar_confidence(queue_listener, test_dt=test_dt)
        gauge_passing = test_gauge(queue_listener, test_dt=test_dt)
        ptype_passing = test_ptype(queue_listener, test_dt=test_dt)
        pmask_passing = test_pmask(queue_listener, test_dt=test_dt)

        if (qpf_passing and lowalt_passing and
            ghe_passing and persiann_passing and
           ppr_accum_passing and lowalt_accum_passing and
           radar_confidence_passing and gauge_passing and
           ptype_passing and pmask_passing):
            logger.info("All tests passing")
        else:
            logger.info("One or more tests did not pass...")
    elif dataset_to_test == "lowalt":
        lowalt_passing = test_lowalt(queue_listener, test_dt=test_dt)
        logger.info(f"lowalt test passing: {lowalt_passing}")
    elif dataset_to_test == "ghe":
        ghe_passing = test_ghe(queue_listener, test_dt=test_dt)
        logger.info(f"ghe test passing: {ghe_passing}")
    elif dataset_to_test == "qpf":
        qpf_passing = test_qpf(queue_listener, test_dt=test_dt)
        logger.info(f"qpf test passing: {qpf_passing}")
    elif dataset_to_test == "ptype":
        ptype_passing = test_ptype(queue_listener, test_dt=test_dt)
        logger.info(f"ptype test passing: {ptype_passing}")
    elif dataset_to_test == "pmask":
        pmask_passing = test_pmask(queue_listener, test_dt=test_dt)
        logger.info(f"pmask test passing: {pmask_passing}")
    elif dataset_to_test == "persiann":
        persiann_passing = test_persiann(queue_listener, test_dt=test_dt)
        logger.info(f"persiann test passing: {persiann_passing}")
    elif dataset_to_test == "ppr_accum":
        ppr_accum_passing = test_ppr_accum(queue_listener, test_dt=test_dt)
        logger.info(f"ppr_accum test passing: {ppr_accum_passing}")
    elif dataset_to_test == "lowalt_accum":
        lowalt_accum_passing = test_lowalt_accum(queue_listener, test_dt=test_dt)
        logger.info(f"lowalt_accum test passing: {lowalt_accum_passing}")
    elif dataset_to_test == "confidence":
        radar_confidence_passing = test_radar_confidence(queue_listener, test_dt=test_dt)
        logger.info(f"radar confidence test passing: {radar_confidence_passing}")
    elif dataset_to_test == "gauge":
        gauge_passing = test_gauge(queue_listener, test_dt=test_dt)
        logger.info(f"gauge test passing: {gauge_passing}")

"""
pmask.py

Purpose: Fetch grids containing probability of observed precipitation, remap to MSL2 domains,
         and push to S3.

Written:  T Rost, Aug. 2022

Modifications:
          R. Domingues, Nov 2022: incorporate standard netcdf output format
"""

# ========================================================================================
# Built-in modules
# ========================================================================================
import gc
import os
import tempfile
import time
from datetime import datetime

# Third party modules
import numpy as np
import xarray as xr
from osgeo import gdal

from msl2_qpe_pkg.base import output as base_outputs
from msl2_qpe_pkg.base import message as base_message
# ========================================================================================
# ========================================================================================

def process(queue_listener, opts, bucket, object_key):
    """
    Example input filename:
    dtn-sat_global_sat-precip-mask_ValidTime_20220810-0000.nc
    """
    MISSING_VALUE = -9999.0

    src_dataset = None
    valid_dt = datetime.strptime(object_key.split("/")[-1].split("_")[4], "%Y%m%d-%H%M.nc")
    opts.logger.info(f" pmask.py: Reading s3://{bucket}/{object_key}...")
    with queue_listener.s3_fs.open(bucket + "/" + object_key, "rb") as f:
        src_dataset = xr.load_dataset(f)

    ds_name = "probprecip60min"
    pmask = xr.Dataset({
        ds_name: (("latitude", "longitude"), src_dataset[ds_name].data)
    })

    lats = src_dataset.lat.values
    lons = src_dataset.lon.values

    pmask.coords["longitude"] = (("longitude"), lons)
    pmask.coords["latitude"] = (("latitude"), lats)

    pmask[ds_name].rio.set_spatial_dims(
        x_dim="longitude", y_dim="latitude", inplace=True
    )

    pmask_raster = pmask[ds_name].rio.write_crs("EPSG:4326")

    for region in queue_listener.config["domains"]:

        domain = queue_listener.config["domains"][region]

        grid_width = domain["nx"]
        grid_height = domain["ny"]
        long_name = domain["long_name"]

        warped_nc = f"{region}.nc"

        with tempfile.NamedTemporaryFile() as tmpfile:
            pmask_raster.rio.to_raster(tmpfile.name, driver="GTiff")
            # pass to GDAL to reproject, resample and convert to NetCDF
            opts.logger.info(f"...Resampling clipped raster for: {long_name}")
            # In the raster format these bounds refer to the edges of pixels, and so
            # need to be offset by half of the dx and dy so that the output
            # (at the center of the pixel) lines up
            # with the intended grid
            # make four variables for the bounds to contain line length
            bounds_minx = domain["minx"]-(domain["grid_dx_deg"]*0.5)
            bounds_miny = domain["miny"]-(domain["grid_dy_deg"]*0.5)
            bounds_maxx = domain["maxx"]+(domain["grid_dx_deg"]*0.5)
            bounds_maxy = domain["maxy"]+(domain["grid_dy_deg"]*0.5)
            warp_options = gdal.WarpOptions(outputBounds=[bounds_minx, bounds_miny,
                                            bounds_maxx, bounds_maxy],
                                            resampleAlg="bilinear", format="NetCDF",
                                            width=grid_width, height=grid_height,
                                            srcNodata=np.nan, dstNodata=MISSING_VALUE)
            ds = gdal.Warp(warped_nc, tmpfile.name, options=warp_options)
            ds = None

        gc.collect()

        # update metadata and conforming to CF1.6 standards
        pmask_cf = xr.open_dataset(warped_nc, decode_cf=False)
        # rename variable to appropriate name
        pmask_cf = pmask_cf.rename({'Band1': ds_name})

        # round data to 4 decimals
        pmask_cf[ds_name] = pmask_cf[ds_name].round(decimals=4)

        output_cfg = {
            "title" : "Satellite Precip-Mask",
            "long_name": "Satellite-derived Precipitation Probability",
            "units" : "dimensionless"
        }

        opts.domain_id = region
        opts.zulu_time = valid_dt

        s3url = base_outputs.write_output_netcdf(
            opts, 
            pmask_cf,
            valid_dt,
            queue_listener.config,
            output_cfg,
            array_type="multi",
            out_var=ds_name,
            output_type="MSK",
            domain_id = region,
            MISSING_VALUE = -9999.0,
            to_modi=False,
            proc_stage="preprocessed",
            freq_id="001hr"
        )

        message_body = base_message.compose_message("PREPROC_HANDLER",s3url)
        is_msg       = base_message.send_message(opts.sqs_obj,message_body)
        if is_msg:
            opts.logger.info(f" - NEW-MSG {region} message SENT for {s3url}")

    return True, s3url

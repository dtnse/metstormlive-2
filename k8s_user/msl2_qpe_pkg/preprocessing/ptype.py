import os
from datetime import datetime
import tempfile

import xarray as xr
from osgeo import gdal
import pandas as pd

from msl2_qpe_pkg.base import output as base_outputs
from msl2_qpe_pkg.base import message as base_message

def process(queue_listener, opts,bucket, object_key, build_tiles=True):
    """
    Example input file name:
    20210721T150000Z-precipitation_type_1fx-20210721T150000Z.grib
    """
    MISSING_VALUE = -9999.0
    # get filename and date/time of file
    path_parts = object_key.split("/")
    file_parts = path_parts[-1].split("_")
    start_date_parts = file_parts[0].split("T")
    end_date_time = file_parts[2].split("-")
    end_date_parts = end_date_time[1].split("T")

    sdt = datetime(int(start_date_parts[0][0:4]), int(start_date_parts[0][4:6]),
                   int(start_date_parts[0][6:8]), int(start_date_parts[1][0:2]),
                   int(start_date_parts[1][2:4]))
    edt = datetime(int(end_date_parts[0][0:4]), int(end_date_parts[0][4:6]),
                   int(end_date_parts[0][6:8]), int(end_date_parts[1][0:2]),
                   int(end_date_parts[1][2:4]))
    time = datetime.strftime(edt, "%Y-%m-%dT%H:00:00Z")

    for region in queue_listener.config["domains"]:

        domain = queue_listener.config["domains"][region]

        w = domain["minx"]
        s = domain["miny"]
        e = domain["maxx"]
        n = domain["maxy"]
        grid_width = domain["nx"]
        grid_height = domain["ny"]

        long_name = domain["long_name"]

        warped_nc = f"{region}.nc"

        # reproject, resample, and clip grid to domains

        with tempfile.NamedTemporaryFile() as s3_tmpfile:
            try:
                queue_listener.s3_fs.get_file(bucket + "/" + object_key, s3_tmpfile.name)
            except FileNotFoundError:
                queue_listener.s3_fs.invalidate_cache()
                opts.logger.info(f"File not Found! Invalidating cache and trying again: {object_key}")
                queue_listener.s3_fs.get_file(bucket + "/" + object_key, s3_tmpfile.name)
            else:
                opts.logger.info(f"...Warping, resampling, and clipping to domain for {long_name}")
                # pass to GDAL to reproject, resample and convert to NetCDF
                # In the raster format these bounds refer to the edges of pixels, and so
                # need to be offset by half of the dx and dy so that the output
                # (at the center of the pixel) lines up
                # with the intended grid
                # make four variables for the bounds to contain line length
                bounds_minx = w-(domain["grid_dx_deg"]*0.5)
                bounds_miny = s-(domain["grid_dy_deg"]*0.5)
                bounds_maxx = e+(domain["grid_dx_deg"]*0.5)
                bounds_maxy = n+(domain["grid_dy_deg"]*0.5)
                warp_options = gdal.WarpOptions(outputBounds=[bounds_minx, bounds_miny,
                                                bounds_maxx, bounds_maxy], dstSRS="EPSG:4326",
                                                resampleAlg="near", format="NetCDF",
                                                width=grid_width, height=grid_height,
                                                srcNodata=MISSING_VALUE, dstNodata=MISSING_VALUE)
                gdal.Warp(warped_nc, s3_tmpfile.name, options=warp_options)

        # grab file info for metadata
        forecast_reform = gdal.Open(warped_nc)
        fx_w, fx_xres, _, fx_n, _, fx_yres = forecast_reform.GetGeoTransform()
        fx_width, fx_height = forecast_reform.RasterXSize, forecast_reform.RasterYSize
        fx_e = fx_w + fx_width * fx_xres
        fx_s = fx_n + fx_height * fx_yres

        forecast_reform = None

        # update metadata and conforming to CF1.6 standards
        forecast_reform = xr.open_dataset(warped_nc, decode_cf=False)
        # rename variable to appropriate name
        forecast_reform = forecast_reform.rename({"Band1": "qpftype"})

        # add other variables
        forecast_reform["time"] = edt
        forecast_reform["forecast_reference_time"] = sdt
        forecast_reform["Latitude_Longitude"] = 0

        output_cfg = {
            "title" :  "OneDTN_Forecast_60min_precip_type",
            "long_name": "Precipitation type enumeration " \
                        "Snow:1 Rain:2 Rain/Snow:3 Freezing Rain:4" \
                        "Sleet:5 Drizzle:6 Freezing-Drizzle:7 "\
                        "Thunderstorm:8",
            "units" : "dimensionless"
        }
        opts.domain_id = region
        opts.zulu_time = edt

        s3url = base_outputs.write_output_netcdf(
            opts, 
            forecast_reform,
            opts.zulu_time,
            queue_listener.config,
            output_cfg,
            array_type="multi",
            out_var="qpftype",
            output_type="TYP",
            domain_id = region,
            MISSING_VALUE = MISSING_VALUE,
            to_modi=False,
            proc_stage="preprocessed",
            freq_id="001hr"
        )

        message_body = base_message.compose_message("PREPROC_HANDLER",s3url)
        is_msg       = base_message.send_message(opts.sqs_obj,message_body)
        if is_msg:
            opts.logger.info(f" - NEW-MSG {region} message SENT for {s3url}")

        os.remove(warped_nc)

    return True

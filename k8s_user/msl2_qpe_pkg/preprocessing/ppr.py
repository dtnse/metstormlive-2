"""
Hourly Accum Process for
Polarimetric Precip Rate (PPR)
1-km Precip Rate Mosaic for North America
extent: {"llx": -170.5, "lly": 13.5, "urx": -50, "ury": 70}

"""
import gzip
import tempfile
from datetime import datetime, timedelta

import xarray as xr
import numpy as np

from msl2_qpe_pkg.base import output as base_outputs
from msl2_qpe_pkg.base import message as base_message


def find_nearest_idx(array, value):
    # helper function to locate idx of nearest value in array
    return (np.abs(array - value)).argmin()


def accum(opts, queue_listener, valid_dt=datetime.utcnow(), build_tiles=True):
    """
    Example s3 URI
    s3://mg-radar-gridded-dtn-radar-prod-euw1/outgoing/NorthAmerica/PrecipitationRate/Radar_NorthAmerica_PrecipitationRate_20220531_180000.nc.gz
    """
    MISSING_VALUE = -9999.0
    bucket = "mg-radar-gridded-dtn-radar-prod-euw1"
    valid_dt = valid_dt.replace(minute=0, second=0, microsecond=0)
    prev_hour_dt = valid_dt - timedelta(hours=1)

    ds_name = "HourlyPolarimetricPrecipAccum"
    precision = 4

    for domain_id in queue_listener.config["domains"]:

        domain = queue_listener.config["domains"][domain_id]

        ppr_domain = domain["ppr_domain"]
        if ppr_domain is None:
            continue

        object_prefix = f"/outgoing/{ppr_domain}/PrecipitationRate/" + \
                        f"Radar_{ppr_domain}_PrecipitationRate_{prev_hour_dt:%Y%m%d_%H}*"

        # fetch list of last-hour files
        queue_listener.s3_fs.invalidate_cache()  # ensure we have the most recent files
        prev_hr_files = queue_listener.s3_fs.glob(bucket + object_prefix)
        file_count = len(prev_hr_files)

        ppr_accum = None
        for fname in prev_hr_files:
            opts.logger.info(f" PPRAccum: Reading s3://{fname} for {domain['long_name']} domain...")

            with tempfile.NamedTemporaryFile() as s3_tmpfile:
                try:
                    queue_listener.s3_fs.get_file(fname, s3_tmpfile.name)
                except FileNotFoundError:
                    # Not found!  Invalidating cache and trying again
                    queue_listener.s3_fs.invalidate_cache()
                    try:
                        queue_listener.s3_fs.get_file(fname, s3_tmpfile.name)
                    except FileNotFoundError:
                        opts.logger.info(f" PPRAccum: s3://{fname} not found! Skipping...")
                        file_count -= 1
                        continue

                # unzip and read file
                data = None
                try:
                    with gzip.open(s3_tmpfile.name) as f:
                        data = xr.load_dataset(f, decode_cf=False, engine="h5netcdf")
                        # reset special flags:
                        #  -99903.0 -> NaN: point not scanned by radar
                        #  -99900.0 -> 0.0: nothing observed
                        data.PrecipitationRate.values[
                            data.PrecipitationRate.values == -99903.0] = np.nan
                        data.PrecipitationRate.values[
                            data.PrecipitationRate.values == -99900.0] = 0.0
                except Exception:
                    opts.logger.info(f" PPRAccum: Error loading s3://{fname}! Skipping...")
                    file_count -= 1
                    continue
                if ppr_accum is None:
                    ppr_accum = data.copy(deep=True)
                else:
                    # running total -- NaNs propagate!
                    ppr_accum.PrecipitationRate.values += data.PrecipitationRate.values

        if ppr_accum is None or not file_count:
            opts.logger.error(
                f" PPRAccum: Error processing {domain['long_name']} domain. Skipping domain...")
            continue

        # source data in mm/hr -- convert sum into average rate (accumulation) for the hour
        ppr_accum.PrecipitationRate.values /= file_count

        # reset NaNs to our missing value flag
        ppr_accum.PrecipitationRate.values[
            ppr_accum.PrecipitationRate.values == np.nan] = MISSING_VALUE

        # round values, including lat-lon coordinates
        ppr_accum["PrecipitationRate"] = ppr_accum["PrecipitationRate"].round(decimals=precision)
        ppr_accum["latitude"] = ppr_accum["latitude"].round(decimals=precision)
        ppr_accum["longitude"] = ppr_accum["longitude"].round(decimals=precision)

        # store 1d arrays of latlons
        lats1d = ppr_accum["latitude"].values[:, 0]
        lons1d = ppr_accum["longitude"].values[0, :]

        # destination grid bounding coordinates
        w = domain["minx"]
        s = domain["miny"]
        e = domain["maxx"]
        n = domain["maxy"]

        # get source grid bounding coordinates of output domain, we will extract this slice
        wx = find_nearest_idx(lons1d, w)
        ex = find_nearest_idx(lons1d, e)
        sy = find_nearest_idx(lats1d, s)
        ny = find_nearest_idx(lats1d, n)

        # create 'cropped' xarray dataset, with all variables clipped to output domain
        cropped_ppr = xr.Dataset({ds_name: (
            ("lat", "lon"), ppr_accum["PrecipitationRate"].values[sy:ny+1, wx:ex+1])})
        cropped_ppr.coords["lat"] = ("lat", lats1d[sy:ny+1])
        cropped_ppr.coords["lon"] = ("lon", lons1d[wx:ex+1])
        cropped_ppr["forecast_reference_time"] = ppr_accum.forecast_reference_time
        # forecast_reference_time attributes
        cropped_ppr.forecast_reference_time.attrs[
            "standard_name"] = "forecast_reference_time"
        cropped_ppr.forecast_reference_time.attrs[
            "string"] = f"{valid_dt:%Y-%m-%dT%H:%M:00Z}"
        cropped_ppr.forecast_reference_time.attrs["period_string"] = "0 minutes"        

        # WRITE OUTPUT
        output_cfg = {
            "title" : "Dual-Polarimetric Precipitation Accumulation",
            "long_name": "Dual-Polarimetric Precipitation Accumulation",
            "units" : "mm"
        }

        opts.domain_id = domain_id
        opts.zulu_time = valid_dt

        s3url = base_outputs.write_output_netcdf(
            opts, 
            cropped_ppr,
            valid_dt,
            queue_listener.config,
            output_cfg,
            array_type="multi",
            out_var=ds_name,
            output_type="PPR",
            domain_id = domain_id,
            MISSING_VALUE = MISSING_VALUE,
            to_modi=False,
            proc_stage="preprocessed",
            freq_id="001hr"
        )
        message_body = base_message.compose_message("PPR_HANDLER",s3url)
        is_msg       = base_message.send_message(opts.sqs_obj,message_body)
        if is_msg:
            opts.logger.info(f" - NEW-MSG {domain_id} message SENT for {s3url}")


    return True
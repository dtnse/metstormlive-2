

#!/usr/bin/env python3.8
# coding: utf-8
#
# DTN, LLC - METSTORM Live2
#  MSL2 CLASS OBJECT
#   ICON & Remote Sensing Team (OCt, 2022)
######################################################################################
import os
import traceback

import numpy as np
import xarray as xr

import msl2_qpe_pkg.base.grid as base_grid

######################################################################################
######################################################################################

def load_intermediate_msl2_inputs(opts,file_use,dataset_key,domain_config):

    try:        
        with opts.s3_fs.open(file_use, "rb") as fobj:
            if opts.is_fulldomain:
                da = xr.load_dataset(fobj, decode_times=False)
            else: # Use the extra bit on the edges to make sure nothing gets cut off by floating point shenanigans
                da = xr.open_dataset(fobj, decode_times=False)\
                        .sel(lat=slice(domain_config['miny']-0.001,domain_config['maxy']+0.001),\
                             lon=slice(domain_config['minx']-0.001,domain_config['maxx']+0.001))\
                             .compute()
            output_array = da[dataset_key].values
        opts.logger.info(f"     - loaded {opts.domain_id} input file: {file_use}")
    except:
        opts.logger.info(f"     - {opts.domain_id} file NOT found: {file_use}")
        output_array = 0.0

    return np.nan_to_num(output_array)

class MSL2classObj(object):

    @classmethod
    def __init__(self,domain_id):
        self.name = "MetstormLive v2 Output Object"
        self.domain_id = domain_id
        self.available_inputs = []

    @classmethod
    def init_BASE_grid(self, domain_config):
        """
        Initializes AREA datasets that are intended to hold raw satellite data 
        for later use in products
        Args:
            config_file: str
        Returns:
            xarray AREA dataset
        """                
        self.base_grid = base_grid.init_BASE_grid(domain_config)

    @classmethod
    def load_clim_basemap(self,opts,zulu_time,domain_id,domain_config):

        try:
            basemap_prefix = f"/basemap/{domain_id}_bmp_1981to2010_{zulu_time:%b}.nc"
            basemap_key = opts.MSL2_INTERMEDIATE_BUCKET + basemap_prefix

            self.CLM = load_intermediate_msl2_inputs(
                opts,
                basemap_key,
                "Basemap",
                domain_config
                )

            self.available_inputs.append("CLM")
            opts.logger.info(f" {domain_id} MergeClimoBackground: {basemap_prefix}")
            
        except BaseException:
            opts.logger.info(f" {domain_id} MergeClimoBackground: NONE")

    @classmethod
    def get_merge_weights(self, opts):
        
        base_rco = self.base_rco.copy()

        if "PPR" in self.available_inputs:
            opts.logger.info("    - PPR available, init weights")
            ppr_init = 16 * base_rco
        else:
            opts.logger.info("    - PPR NOT available, zero weights")
            ppr_init = np.zeros(base_rco.shape)
        
        if "Z2P" in self.available_inputs:
            opts.logger.info("    - Z2P available, init weights")
            z2p_init = base_rco
        else:
            opts.logger.info("    - Z2P NOT available, zero weights")
            z2p_init = np.zeros(base_rco.shape)

        if "GHE" in self.available_inputs:
            opts.logger.info("    - GHE available, init weights")
            ghe_init = 1 - base_rco
        else:
            opts.logger.info("    - GHE NOT available, zero weights")
            ghe_init = np.zeros(base_rco.shape)

        if "PER" in self.available_inputs:
            opts.logger.info("    - PER available, init weights")
            per_init = 1 - base_rco
        else:
            opts.logger.info("    - PER NOT available, zero weights")
            per_init = np.zeros(base_rco.shape)            

        if "QPF" in self.available_inputs:
            opts.logger.info("    - QPF available, init weights")
            qpf_init = 0.5 * (1 - base_rco)
        else:
            opts.logger.info("    - QPF NOT available, zero weights")
            qpf_init = np.zeros(base_rco.shape)

        self.weights = xr.Dataset(
            {
                "ppr_init": (("lat","lon"), ppr_init),
                "z2p_init": (("lat","lon"), z2p_init),
                "ghe_init": (("lat","lon"), ghe_init),
                "per_init": (("lat","lon"), per_init),                
                "qpf_init": (("lat","lon"), qpf_init),
            }
        )

        self.weights["weights_sum"] = self.weights.to_array().sum("variable")
        self.weights = self.weights / self.weights.weights_sum

        del(ppr_init,ghe_init,per_init,qpf_init,base_rco)

    @classmethod
    def list_msl2_inputs_from_s3(
            self, opts , zulu_time, domain_id, config_data, domain_config
        ):

        object_prefix = f"/preprocessed/{zulu_time:%Y%m%d_%H}/{domain_id}_*001hr.nc"

        # listing available files
        opts.s3_fs.invalidate_cache()
        file_list = opts.s3_fs.glob(opts.MSL2_INTERMEDIATE_BUCKET + object_prefix)

        # Create a dictionary with the data files to pull from later
        files_dict = {}
        for f in file_list:
            dataset = os.path.basename(f).split("_")[1]
            files_dict.update({dataset: f})

        opts.logger.info(f" {domain_id} MergeFilesAvail: {files_dict.keys()}")

        # loading inputs
        self.data_dict = {}
        for k in files_dict.keys():
            self.data_dict[k] = files_dict[k]

        # Look for missing files
        self.missing_files = list(set(domain_config["source_inputs"]) - files_dict.keys())
        self.bqe_inputs    = list(set(domain_config["source_inputs"]) - set(self.missing_files))

        opts.logger.info(f" {domain_id} MergeMissFiles: {','.join(self.missing_files)}")

        input_shape = self.base_grid.new_2d_lat.values.shape

        # If RCO from this hour is not available, find most recent RCO file
        # If neither precip dataset present, set RCO 0 everywhere
        if 'PPR' in self.missing_files and 'Z2P' in self.missing_files:
            self.base_rco = np.zeros(input_shape)
            self.have_rco = 'Both radar missing'
        elif 'RCO' in self.data_dict:
            self.base_rco = load_intermediate_msl2_inputs(
                opts,
                self.data_dict["RCO"],
                config_data["dataset_names"]["RCO"],
                domain_config
                )
            self.have_rco = "Current Hour"
        else:
            self.have_rco = "Latest Default"
            latest_rco = opts.s3_fs.glob(opts.MSL2_INTERMEDIATE_BUCKET + "/default/{}_RCO_latest.nc".format(domain_id))
            self.data_dict["RCO"] = latest_rco[0]

            self.base_rco = load_intermediate_msl2_inputs(
                opts,
                self.data_dict["RCO"],
                config_data["dataset_names"]["RCO"],
                domain_config
                )

        self.available_inputs += list(files_dict.keys())

    @classmethod
    def get_QPE_part01(self,opts,config_data,domain_config):
        qpe_output = np.zeros(self.base_rco.shape)

        for k in self.bqe_inputs:
            opts.logger.info("     + adding {}".format(k.lower()))
            msl2_input = load_intermediate_msl2_inputs(
                opts,
                self.data_dict[k],
                config_data["dataset_names"][k],
                domain_config
                )    
            qpe_output +=  msl2_input * self.weights[
                "{}_init".format(k.lower())].values

            del(msl2_input)

        self.qpe_output = xr.Dataset(
            {
                "qpe_first_guess": (("lat","lon"), qpe_output)
            }
        )
        self.qpe_output.coords["lat"]=(("lat"), self.base_grid.lat.values)
        self.qpe_output.coords["lon"]=(("lon"), self.base_grid.lon.values)
        
        del(qpe_output)
    
    @classmethod
    def get_inputs_variance(self,opts,msl2_config,domain_config):
        max_conf_var = msl2_config["merge_parameters"]["max_input_variance"]
        stack_this = []
        for k in self.bqe_inputs:
            opts.logger.info(f"     - using {k}")
            msl2_input = load_intermediate_msl2_inputs(
                opts,
                self.data_dict[k],
                msl2_config["dataset_names"][k],
                domain_config
                )
            stack_this.append(msl2_input)
            del(msl2_input)

        vstack = np.dstack(stack_this)
        del(stack_this)

        v = np.nanvar(vstack, axis=2)
        max_v = np.nanpercentile(v,99)
        v[v>max_v] = max_v
        conf_var = (1 - (v/max_v))*max_conf_var

        self.confidence_metrics = xr.Dataset(
            {
                "input_variance": (("lat","lon"), conf_var)
            }
        )

        self.confidence_metrics.coords["lat"]=(
            ("lat"), self.base_grid.lat.values)
        self.confidence_metrics.coords["lon"]=(
            ("lon"), self.base_grid.lon.values)
        
        # self.qpe_output["which_max"] = (("lat","lon"), np.argmax(vstack, axis=2))
        # self.qpe_output["which_min"] = (("lat","lon"), np.argmin(vstack, axis=2))
        self.qpe_output["data_max"] = (("lat","lon"), np.nanmax(vstack, axis=2))
        self.qpe_output["data_min"] = (("lat","lon"), np.nanmin(vstack, axis=2))
        # self.qpe_output["data_mean"] = (("lat","lon"), np.nanmean(vstack, axis=2))
        # self.qpe_output["data_median"] = (("lat","lon"), np.nanmedian(vstack, axis=2))
        # self.qpe_output["data_standard_dev"] = (("lat","lon"), np.nanstd(vstack, axis=2))

        del(vstack, v, conf_var)

    @classmethod
    def add_generic_conf_metric(
        self,
        opts,
        domain_config,
        weight_use=0.0,
        out_var="clim_var",
        filename=None,
        norm=False,
        var_name=None,
        dict_key=None
        ):

        max_conf_cvar = weight_use
        # Variance of this months climatological basemap ####
        # should really just make climo confidence files honestly
        try:
            if filename != None:
                basemap_var_prefix = filename
                basemap_var_key = opts.MSL2_INTERMEDIATE_BUCKET + basemap_var_prefix
                with opts.s3_fs.open(basemap_var_key, "rb") as f:
                    if opts.is_fulldomain:
                        CLM_var = xr.load_dataset(f)
                    else:
                        CLM_var = xr.open_dataset(f).sel(lat=slice(domain_config['miny']-0.001,domain_config['maxy']+0.001),\
                             lon=slice(domain_config['minx']-0.001,domain_config['maxx']+0.001))\
                             .compute()
                opts.logger.info(f"      - add conf metric for: {out_var} = TRUE ")
                if norm:
                    v = CLM_var[var_name].values
                    max_v = np.nanpercentile(v,99)
                    v[v>max_v] = max_v
                    conf_cvar = (1 - (v/max_v)) * max_conf_cvar
                    del(v)
                else:
                    conf_cvar = (1 - (CLM_var[var_name].values))* max_conf_cvar
                
            elif dict_key !=None:
                CLM_var=None
                var_use = load_intermediate_msl2_inputs(
                    opts,
                    self.data_dict[dict_key],
                    var_name,
                    domain_config
                    )
                conf_cvar = var_use * max_conf_cvar

            self.confidence_metrics[out_var] = (("lat","lon"), np.nan_to_num(conf_cvar))

            del(CLM_var,conf_cvar,var_use)

        except BaseException:
            opts.logger.info(f"      - add conf metric for: {out_var} = NONE")

    @classmethod
    def apply_satellite_mask(self,opts,config_data,domain_config):

        try:
            if "MSK" in self.available_inputs:
                sat_msk = load_intermediate_msl2_inputs(
                    opts,
                    self.data_dict["MSK"],
                    config_data["dataset_names"]["MSK"],
                    domain_config
                    )
                opts.logger.info(f" {opts.domain_id} MergeHaveSatMSK: {True}")                
            else:
                opts.logger.info(f" {opts.domain_id} MergeHaveSatMSK: {False}")
                sat_msk = 0.0

            if isinstance(sat_msk,float):
                sat_msk = 1.0

            mask_adjust = self.qpe_output.qpe_first_guess.values * \
                (1 - (
                    (self.weights.ppr_init.values + self.weights.z2p_init.values)/\
                    self.weights.weights_sum.values)
                )*(1 - sat_msk)
            
            first_guess_plot = self.qpe_output.qpe_first_guess.values - mask_adjust
            first_guess_plot[first_guess_plot < 0.0] = 0.0

            self.qpe_output["best_qpe"] = (
                ("lat","lon"), first_guess_plot
            )

            self.qpe_output = self.qpe_output.drop("qpe_first_guess")
            del(mask_adjust,sat_msk,first_guess_plot)

        except KeyError:
            print(traceback.format_exc())
            opts.logger.error(f" {opts.domain_id} Failed Applying SAT MASK")
        
    @classmethod
    def add_qpe_attributes(self):

        self.qpe_output["best_qpe"].attrs["units"] = "mm"
        self.qpe_output["best_qpe"].attrs["missing"] = self.missing_files
        self.qpe_output["best_qpe"].attrs["have_gauges"] = "False"

        # self.qpe_output["which_max"].attrs["dataset_map"] = (0, "GHE", 1, "PER", 2, "PPR", 3, "Z2P", 4, "QPF")
        # self.qpe_output["which_min"].attrs["dataset_map"] = (0, "GHE", 1, "PER", 2, "PPR", 3, "Z2P", 4, "QPF")

        self.qpe_output["data_max"].attrs["units"] = "mm"
        self.qpe_output["data_min"].attrs["units"] = "mm"
        # self.qpe_output["data_mean"].attrs["units"] = "mm"
        # self.qpe_output["data_median"].attrs["units"] = "mm"
        # self.qpe_output["data_standard_dev"].attrs["units"] = "mm"


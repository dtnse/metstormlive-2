#!/usr/bin/env python3.8
# coding: utf-8
#
# DTN, LLC - METSTORM Live2
# GENERAL UTILS SCRIPT
#   ICON & Remote Sensing Team (OCt, 2022)
######################################################################################
import yaml
import json


######################################################################################
######################################################################################

def load_domain_config(opts, domain_id,config_file="msl2.json"):
    """
    Loads a JSON configuration file
    Args:
        config_file: str
    Returns:
        dictionary
    """
    cfg_file = "{}/{}".format(opts.config_path,config_file)

    with open(cfg_file, 'r') as jobj:
        config_dic = json.loads(jobj.read())

    return config_dic["domains"][domain_id]

def load_json_config(opts, config_file="msl2.json"):
    """
    Loads a JSON configuration file
    Args:
        config_file: str
    Returns:
        dictionary
    """
    cfg_file = "{}/{}".format(opts.config_path,config_file)

    with open(cfg_file, 'r') as jobj:
        config_dic = json.loads(jobj.read())

    return config_dic

def load_yaml_config(cfg_file="./config/general_config.yaml"):
    """
    Loads a YAML configuration file
    Returns:
        config: dictionary with keys
    """
    try:
        with open(cfg_file) as f:
            config = yaml.full_load(f.read())
        return config
    except Exception as e:
        print("Error reading {0}: {1}".format(cfg_file,e))


def str_to_bool(s):
    if s == 'True':
        return True
    elif s == 'False':
        return False
    else:
        return False

#!/usr/bin/env python3.8
# coding: utf-8
#
# DTN, LLC - METSTORM Live2
# MBTILES HANDLER
#   ICON & Remote Sensing Team (Oct, 2022)
######################################################################################
######################################################################################
import os
import boto3
import json

import boto3
import s3fs

from msl2_qpe_pkg.base import utils as base_utils
######################################################################################
######################################################################################

def init_SQSbotoclient(env_var="SQS_QUEUE"):

    sqs_client = boto3.client('sqs')
    QUEUE_NAME = os.getenv(env_var)
    QUEUE_URL = sqs_client.get_queue_url(QueueName = QUEUE_NAME)['QueueUrl']

    return sqs_client, QUEUE_URL

def start_sqs_clients(opts,env_var="SQS_QUEUE"):

    opts.logger.debug(" - starting AWS SQS client")

    class SQSobj(object):

        def __init__(self):
            self.name = "SQS object class"
        
        def add_client(self,env_var):
            sqs_client, queue_url = init_SQSbotoclient(
                env_var=env_var
                )
            
            self.sqs_client = sqs_client
            self.queue_url = queue_url

    
    sqs_obj = SQSobj()
    sqs_obj.add_client(env_var)

    return sqs_obj

def init_s3FileSystem(anon=False):
    import s3fs
    return s3fs.S3FileSystem(anon=anon)

def init_boto3S3Client(region="us-east-1"):
    return boto3.resource('s3', region_name=region)

class MSL2QueueListener(object):
    """
    Class that listens to MSL2 file ingest queue for messages,
    and processes accordingly

    """

    def __init__(self, opts, init_sqs=True) -> None:

        self.attempts = 0
        self.pause_between_receives_seconds = os.getenv(
            "PAUSE_BETWEEN_RECEIVES_SECONDS",
            1
        )
        self.sqs_name = os.getenv(
            'SQS_NAME',
            'metstormlive-file-queue-filtered-dev'
        )
        self.aws_account_id = os.getenv('AWS_ACCOUNT_ID', '688714617473')
        self.aws_region = os.getenv('AWS_REGION', 'us-east-1')
        self.role_arn = os.getenv(
            'ROLE_ARN',
            'arn:aws:iam::688714617473:role/metstormlive2-s3-read-write-sqs-all'
        )
        self.MSL2_INTERMEDIATE_BUCKET = os.getenv(
            'MSL2_INTERMEDIATE_BUCKET',
            'metstormlive-v2-intermediate-dev'
        )
        self.env = os.getenv('ENV', 'dev')

        # if running in AWS EKS, permissions will be assigned to the
        # pods this will be running on
        self.sqs_client = boto3.client('sqs', region_name=self.aws_region)
        self.s3 = boto3.resource('s3', region_name=self.aws_region)

        self.s3_fs = s3fs.S3FileSystem(anon=False)
        self.config = base_utils.load_json_config(opts)

        if init_sqs:
            response = self.sqs_client.get_queue_url(
                QueueName=self.sqs_name,
                QueueOwnerAWSAccountId=self.aws_account_id
            )
            self.queue_url = response.get('QueueUrl')
        else:
            self.queue_url = None

    def _delete_from_queue(self, opts, object_key, receipt):
        self.sqs_client.delete_message(
            QueueUrl=self.queue_url,
            ReceiptHandle=receipt
        )
        opts.logger.debug(f"...Deleted {object_key} from queue")

    def _parse_s3_notification(self, response):
        """
        Parse standard S3 notification message
        These messages have a subject line of:
        'Amazon S3 Notification'
        """
        messages = response.get("Messages")
        if messages:
            message = messages[0]
            message_body = message["Body"]
            receipt = message["ReceiptHandle"]
            message_json = json.loads(message_body)
            records_json = json.loads(message_json["Message"])
            record = records_json["Records"][0]
            object_key = record["s3"]["object"]["key"]
            bucket = record["s3"]["bucket"]["name"]
            return bucket, object_key, receipt

        return None, None, None

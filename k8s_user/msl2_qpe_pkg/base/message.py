#!/usr/bin/env python3.8
# coding: utf-8
#
# DTN, LLC - METSTORM Live2
# MESSAGING FUNCTIONs
#   ICON & Remote Sensing Team (Oct, 2022)
######################################################################################
######################################################################################

import json
import re
import traceback
from datetime import datetime

######################################################################################
######################################################################################

def compose_message(msg_ID,urlget,downstream_user=None,n_tries=0):

    message_body = {
        "Type": "{}-Message".format(msg_ID),
        "url_get": urlget,
        "downstream_user": downstream_user,
        "timestamp": str( datetime.utcnow() ),
        "n_tries": str(n_tries)
    }

    return message_body

def delete_message(message, queue_url, sqs_client,receipt_handle=None):
    """Delete message from SQS queue"""
    try:
        if receipt_handle == None:            
            receipt_handle = message['ReceiptHandle']
            
        sqs_client.delete_message(QueueUrl=queue_url, ReceiptHandle=receipt_handle)
    except:
        print('... ERROR deleting message from queue {0}: {1}'.format(queue_url, message))
        print(traceback.format_exc())

def send_message(sqs_obj,message_body_dic,delay=1):
    try:
        sqs_obj.sqs_client.send_message(
            QueueUrl=sqs_obj.queue_url,
            MessageBody=json.dumps(message_body_dic,indent = 4),
            DelaySeconds=delay
        )
        return True
    except:
        print('... ERROR sending message ')
        print(traceback.format_exc())
        return False

def parse_message(message, queue_url, sqs_client):
    """Parse a single SQS message. Assumes that the message follows AWS S3 Bucket Notification format"""
    try:
        # message_id   = message['MessageId']
        message_id   = message['ReceiptHandle']
        message_body = json.loads(message['Body'])
        message_type = message_body['Type']


        if re.search("Notification", message_type):
            sns_notification = json.loads(message_body['Message'])
            if "Type" in sns_notification:
                message_type = sns_notification["Type"]
                urlget          = sns_notification['url_get']
                downstream_user = sns_notification['downstream_user']
                n_tries         = sns_notification['n_tries']
            else:
                key = sns_notification['Records'][0]['s3']['object']['key']
                bucket = sns_notification['Records'][0]['s3']['bucket']['name']
                urlget = 's3://%s/%s' % (bucket, key)
                downstream_user = None
                n_tries         = str(0)
        elif re.search("-Message", message_type):
            urlget          = message_body['url_get']
            downstream_user = message_body['downstream_user']
            n_tries         = message_body['n_tries']

        return message_id, urlget, message_type, downstream_user, n_tries

    except:
        print('unable to parse message using BN format: {0}'.format(message))
        print(traceback.format_exc())        
        print('... deleting message')
        delete_message(message, queue_url, sqs_client)
        return None,None, None, None

def check_trigger_match(sec_triggers, s3url):
    is_match = False
    sec_use  = None

    for key in sec_triggers:
        for trigger in sec_triggers[key]["trigger"].split(","):
            if re.search(trigger, s3url):
                is_match = True
                sec_use = key

    return is_match, sec_use

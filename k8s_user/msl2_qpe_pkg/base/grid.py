#!/usr/bin/env python3.8
# coding: utf-8
#
# DTN, LLC - METSTORM Live2
# GENERAL UTILS SCRIPT
#   ICON & Remote Sensing Team (OCt, 2022)
######################################################################################
######################################################################################
import numpy as np
import xarray as xr

######################################################################################
######################################################################################
def init_BASE_grid(domain_config):

    # Create the lat lon grid for inverse distance weighting
    # Currently the sparse grid is required to do idw weighting at 10km
    lon = np.arange(domain_config["minx"], domain_config["maxx"]+(domain_config["grid_dx_deg"]-(domain_config["grid_dx_deg"]/10.0)),
                       domain_config["grid_dx_deg"])
    lat = np.arange(domain_config["miny"], domain_config["maxy"]+(domain_config["grid_dy_deg"]-(domain_config["grid_dy_deg"]/10.0)),
                       domain_config["grid_dy_deg"])

    new_2d_lon, new_2d_lat  = np.meshgrid(lon,lat)
    # new_sparse_lon = np.arange(domain_config["minx"], domain_config["maxx"], domain_config["sparse_dx_deg"])
    # new_sparse_lat = np.arange(domain_config["miny"], domain_config["maxy"], domain_config["sparse_dy_deg"])
    # sparse_2d_lon, sparse_2d_lat = np.meshgrid(new_sparse_lon, new_sparse_lat)

    da = xr.Dataset(
            {
                "new_2d_lon": (("lat","lon"),  new_2d_lon),
                "new_2d_lat": (("lat","lon"),  new_2d_lat),
            }
        )
    da.coords["lat"]=(("lat"), lat)
    da.coords["lon"]=(("lon"), lon)

    del(lon,lat,new_2d_lat,new_2d_lon)
    
    return da    

def get_subdomain_bounds(domain_config):

    minx = domain_config["minx"]
    maxx = domain_config["maxx"]
    miny = domain_config["miny"]
    maxy = domain_config["maxy"]
    nrows = domain_config["nrows"]
    ncols = domain_config["ncols"]
    overlap_dd = domain_config["overlap_dd"]
    
    # Add a tiny bit to the maxes so arange behaves
    # We don't specifically care about the grid spacing within the tiles,
    # just the boundaries
    lons = np.round(np.arange(minx,maxx+0.001,(maxx-minx)/ncols),2)
    lats = np.round(np.arange(miny,maxy+0.001,(maxy-miny)/nrows),2)

    # Make a dictionary with the boundaries of each tile
    # If each tile adds half the overlap_dd to each applicable side,
    # the total overlap width will be what is requested
    tilenum = 1
    domains = {}
    corners = {}
    for i in range(0,len(lats)-1):
        for j in range(0,len(lons)-1):
            if j > 0:
                corners['minx'] = lons[j] - overlap_dd * 0.5
            else:
                corners['minx'] = lons[j]
            if j < len(lons) - 2:
                corners['maxx'] = lons[j+1] + overlap_dd * 0.5
            else:
                corners['maxx'] = lons[j+1]
            if i > 0:
                corners['miny'] = lats[i] - overlap_dd * 0.5
            else:
                corners['miny'] = lats[i]
            if i < len(lats) - 2:
                corners['maxy'] = lats[i+1] + overlap_dd * 0.5
            else:
                corners['maxy'] = lats[i+1]
            domains[tilenum] = corners.copy()
            tilenum += 1

    del(lats,lons,corners)
    return domains

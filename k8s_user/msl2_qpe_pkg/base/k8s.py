#!/usr/bin/env python3.8
# coding: utf-8
#
# DTN, LLC - METSTORMLIVE2
#   BASE: Kubernetes Utils
#   Remote Sensing Team [Ric Domingues, Dec, 2022]
#####################################################################################
import os
import re
import uuid
from kubernetes import client as k8s_client
#####################################################################################
class Kubernetes:
    def __init__(self):

        # Init Kubernetes
        self.core_api = k8s_client.CoreV1Api()
        self.batch_api = k8s_client.BatchV1Api()

    @staticmethod
    def claim_volumes(volume_name="efs-volume-label"):
        volume = k8s_client.V1Volume(
            name=volume_name,
            persistent_volume_claim=k8s_client.V1PersistentVolumeClaimVolumeSource(
                claim_name=volume_name
            ),
        )

        return volume

    @staticmethod
    def token_volume():
        volume = k8s_client.V1Volume(
            name="aws-iam-coord-token",
            host_path=k8s_client.V1HostPathVolumeSource(
                path="/var/run/secrets/kubernetes.io/serviceaccount"
                ),
            )

        return volume

    @staticmethod
    def create_container(
        image, 
        name, 
        _env_vars, 
        pod_config,
        volume_name=None,
        entrypoint_cmd = ""
        ):

        pod_resources = k8s_client.V1ResourceRequirements(
            requests={
                "cpu": pod_config["Requests"]["cpu"],
                "memory": pod_config["Requests"]["memory"],
                "ephemeral-storage": pod_config["Requests"]["ephemeral-storage"]
                },
            limits={
                "cpu": pod_config["Limits"]["cpu"],
                "memory": pod_config["Limits"]["memory"]
                }                
        )

        container = k8s_client.V1Container(
            image=image,
            name=name,
            args=[None],
            env= _env_vars,
            resources = pod_resources,
            # volume_mounts=[
            #     k8s_client.V1VolumeMount(
            #         mount_path="/data",
            #         name=volume_name
            #     )
            #     ],
            command=[entrypoint_cmd],
        )

        # print(
        #     f"Created container with name: {container.name}, "
        #     f"image: {container.image} and args: {container.args}"
        # )

        return container

    @staticmethod
    def create_pod_template(
        app_name, container,
        volumes,service_account,
        restart_policy,
        job_class="default-priority"):

        job_id    = uuid.uuid4().hex[:16]
        pod_id    = job_id
        pod_name = "{}-job-{}".format(app_name,pod_id)

        pod_template = k8s_client.V1PodTemplateSpec(
            spec=k8s_client.V1PodSpec(
                restart_policy=restart_policy,
                containers=[container],
                volumes=volumes,
                service_account_name=service_account,
                automount_service_account_token=True,
                priority_class_name=job_class
                ),
            metadata=k8s_client.V1ObjectMeta(name=pod_name, labels={"pod_name": pod_name}),
        )

        return pod_template, job_id

    @staticmethod
    def create_job(app_name, pod_template,job_id):
        job_name = "{}-job-{}".format(app_name,job_id)
        
        metadata = k8s_client.V1ObjectMeta(
            name=job_name, 
            labels={
                "job_name": app_name,
                "service": app_name,
                "tags.datadoghq.com/env": os.getenv("ENV"),
                "tags.datadoghq.com/service": app_name
                }
            )

        job = k8s_client.V1Job(
            api_version="batch/v1",
            kind="Job",
            metadata=metadata,
            spec=k8s_client.V1JobSpec(
                active_deadline_seconds=3600,
                backoff_limit=0, 
                template=pod_template,
                ttl_seconds_after_finished=15
                ),
        )

        return job

    @staticmethod
    def gather_env_vars(
        app_id,
        other_vars=None
        ):

        env_vars = []
        for name, value in os.environ.items():

            if re.search("{}_".format(app_id),name):
                var = k8s_client.V1EnvVar(
                    name=name,
                    value=value
                )
                env_vars.append(var)
                # print("{0}: {1}".format(name, value))
        
        if other_vars != None:
            for key in other_vars:
                value = other_vars[key]

                var = k8s_client.V1EnvVar(
                    name=key,
                    value=value
                )
                env_vars.append(var)
                # print("{0}: {1}".format(name, value))


        return env_vars


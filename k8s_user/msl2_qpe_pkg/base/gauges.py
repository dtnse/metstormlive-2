

#!/usr/bin/env python3.8
# coding: utf-8
#
# DTN, LLC - METSTORM Live2
# GAUGE UTILS
#   ICON & Remote Sensing Team (OCt, 2022)
######################################################################################
import sys

import pandas as pd
import numpy as np
import xarray as xr
######################################################################################
######################################################################################


def get_gauge_data(opts,domain_config,domain_id):

    object_prefix = f"/preprocessed/{opts.zulu_time:%Y%m%d_%H}/{domain_id}_OBS*"

    gauge_list = opts.s3_fs.glob(opts.MSL2_INTERMEDIATE_BUCKET + object_prefix)
    opts.logger.info(f"{domain_id} MergeAllAvailGaugeFiles: {','.join(gauge_list)}")

    if gauge_list:
        gauge_file = gauge_list[-1]
        with opts.s3_fs.open(gauge_file, "rb") as f:
            GGE = pd.read_csv(f, sep=",")

            GGE = GGE.rename(columns=lambda x: x.strip())
            tot_gauge_num = len(GGE["obs_ppt_in"][:])
            opts.logger.info(tot_gauge_num)
            if not opts.is_fulldomain:
                idx = np.where((GGE["lat_dd"] >= opts.miny) & \
                               (GGE["lat_dd"] <= opts.maxy) & \
                               (GGE["lon_dd"] >= opts.minx) & \
                               (GGE["lon_dd"] <= opts.maxx))
                GGE = GGE.loc[idx]
            tot_gauge_num = len(GGE["obs_ppt_in"][:])
            opts.logger.info(tot_gauge_num)
            GGE = GGE[GGE["qc_flag"] >= domain_config["stn_qc_threshold"]]

        opts.logger.info(f"{domain_id} MergeHasGaugeFile: {gauge_file}")
        return True, GGE
        
    else:
        opts.logger.info(f"{domain_id} Gauge Data Not Available")
        return False, None
    

def calculate_gauge_bias(
    opts,
    CLM,
    first_guess_array,
    base_grid,gauge_data,
    domain_config
    ):

    # custom imports only used here
    from scipy.interpolate import griddata
    import geopandas
    from osgeo import gdal    

    # Create the normalized basemap from climo and first guess field
    first_guess = first_guess_array["best_qpe"].values.flatten()
    bmap = CLM.flatten()

    norm_bmap = bmap / (np.nanmax(bmap))
    norm_fgss = first_guess / np.nanmax(first_guess)
    dbmap = (norm_bmap + norm_fgss) / 2.0

    del(bmap,norm_bmap,norm_fgss,CLM)

    # compute nearest neighbor interpolation of first guess field to gauge points

    firstguess_at_gauge_nearest = griddata(
        (
            base_grid.new_2d_lat.values.flatten(),
            base_grid.new_2d_lon.values.flatten()
        ),
        first_guess,
        (
            gauge_data["lat_dd"],
            gauge_data["lon_dd"]
        ),
        method="nearest"
        )
    dbmap_at_gauge_nearest = griddata(
        (
            base_grid.new_2d_lat.values.flatten(),
            base_grid.new_2d_lon.values.flatten()
        ), 
        dbmap, 
        (
            gauge_data["lat_dd"],
            gauge_data["lon_dd"]
        ), 
        method="nearest"
        )
    # Convert gauge obs inches to mm
    gauge_obs = np.array(gauge_data["obs_ppt_in"][:] * 25.4)
    len_gauges = len(gauge_obs)
    opts.logger.info(f" {opts.domain_id}MergeTotalGaugesInFile: {len_gauges}")
    opts.logger.info(f" {opts.domain_id}MergeGaugesUsedInBiasCorrection: {len_gauges}")
    # Compute the normalized bias at the gauge points to
    # the dynamic basemap value at the nearest gridpoint

    nbias_file = "nbias_tmpfile.shp"
    nbias_points = (gauge_obs - firstguess_at_gauge_nearest) / dbmap_at_gauge_nearest
    geo_nbias = geopandas.GeoDataFrame(
        nbias_points, 
        geometry=geopandas.points_from_xy(gauge_data.lon_dd, gauge_data.lat_dd),
        crs="EPSG:4326"
        )
    geo_nbias.columns = ['nbias', 'geometry']
    geo_nbias.to_file(nbias_file)

    MISSING_VALUE = -9999.0
    grid_width = domain_config["nx"]
    grid_height = domain_config["ny"]

    bounds_minx = domain_config["minx"]-(domain_config["grid_dx_deg"]*0.5)
    bounds_miny = domain_config["miny"]-(domain_config["grid_dy_deg"]*0.5)
    bounds_maxx = domain_config["maxx"]+(domain_config["grid_dx_deg"]*0.5)
    bounds_maxy = domain_config["maxy"]+(domain_config["grid_dy_deg"]*0.5)

    grid_options = gdal.GridOptions(outputBounds=[bounds_minx, bounds_miny,
                                    bounds_maxx, bounds_maxy], outputSRS="EPSG:4326",
                                    algorithm="invdist", format="NetCDF",
                                    width=grid_width, height=grid_height,
                                    noData=MISSING_VALUE, zfield="nbias")
                                    
    obs_out = "nbias_grid_tmpfile.nc"

    gdal.Grid(obs_out, 'nbias_tmpfile.shp', options=grid_options)

    nbias_grid = xr.load_dataset(obs_out)
    nbias_grid = np.nan_to_num(np.flip(nbias_grid['Band1'], axis=0))

    bias_idw = nbias_grid*np.reshape(dbmap, base_grid.new_2d_lat.shape)

    del(dbmap,nbias_grid)
    
    # undo custom imports
    del sys.modules["geopandas"] 
    del geopandas
    del sys.modules["osgeo"] 
    del gdal
    del sys.modules["scipy"] 
    del griddata

    return bias_idw

#!/usr/bin/env python3.8
# coding: utf-8
#
# DTN, LLC - METSTORM Live2
# MBTILES CODES
#   ICON & Remote Sensing Team (Nov, 2022)
######################################################################################
######################################################################################

import shutil
import os

from osgeo import gdal

######################################################################################

def generate_tiles(
    opts,
    fname,
    tiles_name=None, 
    nc_dataset_name=None,
    palette="palettes/mm.txt"
    ):

    CREATION_OPTS = ["TYPE=baselayer", "ZOOM_LEVEL_STRATEGY=UPPER", "ZLEVEL=9"]
    gdal.SetConfigOption("COMPRESS_OVERVIEW", "DEFLATE")
    gdal.SetConfigOption("GDAL_NUM_THREADS", "8")
    gdal.SetConfigOption("GDAL_CACHEMAX", "16000")

    palette_fname = palette
    mbtiles_fname = "/tmp/{}.mbtiles".format(
        os.path.basename(fname).split(".")[0]
    )

    if nc_dataset_name:
        ds = gdal.Open(f"NETCDF:{fname}:{nc_dataset_name}")
    else:
        ds = gdal.Open(fname)

    # print(ds)


    # reproject to web mercator

    opts.logger.info("Reprojecting to Web Mercator (EPSG:3857)")
    warp_output = "/tmp/ppr_warp.tif"
    colors_output = "/tmp/ppr_colors.tif"
    warp_options = gdal.WarpOptions(srcSRS="EPSG:4326", dstSRS="EPSG:3857")
    # warp_options = gdal.WarpOptions(srcSRS="+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs", dstSRS="EPSG:3857", format="GTiff",warpMemoryLimit=1000)
    warp = gdal.Warp(warp_output, ds, options=warp_options)

    print("Warp Complete")
    # apply custom color palette

    opts.logger.info("Applying custom color palette to raster {}".format(palette_fname))
    dem_options = gdal.DEMProcessingOptions(colorFilename=palette_fname,
                                            format="GTiff", addAlpha=True)
                                        
    colors = gdal.DEMProcessing(colors_output, warp, "color-relief", options=dem_options)

    # generate highest zoom level of tiles

    translate_options = gdal.TranslateOptions(format="MBTILES", creationOptions=CREATION_OPTS)
    opts.logger.info("Creating Base tile layer...")
    translate = gdal.Translate(mbtiles_fname, colors, options=translate_options)

    # build the rest of the tile pyramid

    opts.logger.info("Creating the rest of the tile pyramid...")
    translate.BuildOverviews("AVERAGE", [2, 4, 8, 16])
    os.remove(colors_output)
    os.remove(warp_output)

    return mbtiles_fname

def generate_tiles_vsis3(
    opts, 
    fname, 
    tiles_name=None, 
    nc_dataset_name=None,
    palette="palettes/mm.txt"
    ):

    palette_fname = palette

    CREATION_OPTS = ["TYPE=baselayer", "ZOOM_LEVEL_STRATEGY=UPPER", "ZLEVEL=9"]
    gdal.SetConfigOption("COMPRESS_OVERVIEW", "DEFLATE")
    gdal.SetConfigOption("GDAL_NUM_THREADS", "8")
    gdal.SetConfigOption("GDAL_CACHEMAX", "16000")
    gdal.SetConfigOption("CPL_VSIL_USE_TEMP_FILE_FOR_RANDOM_WRITE", "YES")

    tiles_fname = fname.replace(".nc", ".mbtiles")

    colors_tmp = "/tmp/colors.tif"
    warp_tmp = "/tmp/warp.tif"

    if nc_dataset_name:
        # shutil.copyfile(fname, fname + ".nc")
        nc = gdal.Open(f"NETCDF:{fname}:{nc_dataset_name}")
    else:
        nc = gdal.Open(fname)
        
    # reproject to web mercator
    opts.logger.info("Reprojecting to Web Mercator (EPSG:3857)")
    warp_options = gdal.WarpOptions(srcSRS="EPSG:4326", dstSRS="EPSG:3857", format="GTiff")
    gdal.Warp(warp_tmp, nc, options=warp_options)

    # apply custom color palette
    opts.logger.info("Applying custom color palette to raster {}".format(palette_fname))
    dem_options = gdal.DEMProcessingOptions(colorFilename=palette_fname,
                                            format="GTiff", addAlpha=True)
    colors = gdal.DEMProcessing(colors_tmp, warp_tmp, "color-relief", options=dem_options)

    # generate highest zoom level of tiles
    translate_options = gdal.TranslateOptions(format="MBTILES", creationOptions=CREATION_OPTS)
    ("Creating Base tile layer...")
    translate = gdal.Translate(tiles_fname, colors, options=translate_options)

    # build the rest of the tile pyramid
    opts.logger.info("Creating the rest of the tile pyramid...")
    translate.BuildOverviews("AVERAGE", [2, 4, 8, 16])

    os.remove(colors_tmp)
    os.remove(warp_tmp)

    return tiles_fname

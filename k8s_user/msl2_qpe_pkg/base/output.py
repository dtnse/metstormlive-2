#!/usr/bin/env python3.8
# coding: utf-8
#
# DTN, LLC - METSTORM Live2
# OUTPUTS CODE
#   ICON & Remote Sensing Team (OCt, 2022)
######################################################################################
######################################################################################

from copy import deepcopy
import traceback
import tempfile

import xarray as xr
import numpy as np

######################################################################################
######################################################################################

def get_output_filename_s3key(opts,output_type,proc_stage="postprocessed",freq_id="001hr"):

    filename = f"{opts.domain_id}_{output_type}_{opts.zulu_time:%Y%m%d_%H%M}_{freq_id}.nc"
    s3key    = f"{proc_stage}/{opts.zulu_time:%Y%m%d_%H}/{filename}"

    modi_filename = f"{opts.domain_id}_msl-{output_type.lower()}_{opts.zulu_time:%Y%m%d_%H}00_{freq_id}.nc"
    modi_object_key = f"incoming/netcdf/{opts.domain_id}/{opts.zulu_time:%Y%m%dT%H}0000Z/{modi_filename}"    

    return filename, s3key, modi_filename, modi_object_key


def write_output_netcdf(
    opts, 
    out_array,
    valid_dt,
    msl2_config,
    output_cfg,    
    array_type="single",
    out_var="out_var",
    output_type="ECM",
    domain_id = "CC",
    MISSING_VALUE = -9999.0,
    to_modi=True,
    proc_stage="postprocessed",
    freq_id="001hr"
    ):
    
    try:
        out_filename, s3key, modi_filename, modi_object_key = get_output_filename_s3key(
            opts,output_type,proc_stage=proc_stage,freq_id=freq_id
            )

        if array_type == "single":
            da = xr.Dataset({
                out_var: (("lat","lon"), out_array.values),
            })

            da.coords["lon"] = (("lon"), out_array.lon.values)
            da.coords["lat"] = (("lat"), out_array.lat.values)
        else:
            da = out_array.copy(deep=True)

        da["time"] = opts.zulu_time
        da.time.attrs["units"] = "Seconds since 1970-01-01 00:00:00"

        da['time'] = valid_dt
        da['Latitude_Longitude'] = 0

        # precision for floating point attribute values
        decimals = 6
        warped_height, warped_width = da[out_var].values.shape

        llcrnrlat = np.round(da.lat.values.min(), 1)
        llcrnrlon = np.round(da.lon.values.min(), 1)
        urcrnrlat = np.round(da.lat.values.max(), 1)
        urcrnrlon = np.round(da.lon.values.max(), 1)        

        # set global attributes
        metadata   = msl2_config['metadata']
        domain_cfg = msl2_config['domains'][domain_id]


        da.attrs['title'] = output_cfg["title"]
        da.attrs['map_proj'] = metadata['map_proj']
        da.attrs['Conventions'] = metadata['Conventions']
        da.attrs['institution'] = metadata['institution']
        da.attrs['grid_mapping_name'] = metadata['grid_mapping_name']
        da.attrs['DataConvention'] = metadata['DataConvention']
        da.attrs['nx'] = warped_width
        da.attrs['ny'] = warped_height
        da.attrs['dx'] = round(domain_cfg["grid_dx_km"], decimals)  # km
        da.attrs['dy'] = round(domain_cfg["grid_dy_km"], decimals)
        da.attrs['llcrnrlat'] = round(llcrnrlat, decimals)
        da.attrs['llcrnrlon'] = round(llcrnrlon, decimals)
        da.attrs['urcrnrlat'] = round(urcrnrlat, decimals)
        da.attrs['urcrnrlon'] = round(urcrnrlon, decimals)
        da.attrs['lowerleft_latlon'] = (
            round(llcrnrlon, decimals), round(llcrnrlat, decimals))
        da.attrs['upperright_latlon'] = (
            round(urcrnrlon, decimals), round(urcrnrlat, decimals))
        da.attrs['dlat'] = round(domain_cfg["grid_dy_deg"], decimals)  # km
        da.attrs['dlon'] = round(domain_cfg["grid_dx_deg"], decimals)  # km

        # set up attributes for each band
        da[out_var].attrs['coordinates'] = 'lat lon'
        da[out_var].attrs['grid_mapping'] = 'latitude_longitude'
        da[out_var].attrs['least_significant_digit'] = 4
        da[out_var].attrs['deflate'] = 1
        da[out_var].attrs['deflate_level'] = domain_cfg['nc_compression_level']
        da[out_var].attrs['units'] = output_cfg["units"]
        da[out_var].attrs['long_name'] = output_cfg["long_name"]
        da[out_var].attrs['_FillValue'] = MISSING_VALUE

        # time attributes
        da.time.attrs['standard_name'] = 'time'
        da.time.attrs['string'] = (f"{valid_dt:%Y-%m-%dT%H:%M:%SZ}")
        da.time.attrs['period_string'] = '0 minutes'

        # latitude_longitude attributes
        da.Latitude_Longitude.attrs['grid_mapping_name'] = 'latitude_longitude'
        da.Latitude_Longitude.attrs['llcrnrlon'] = round(llcrnrlon, decimals)
        da.Latitude_Longitude.attrs['llcrnrlat'] = round(llcrnrlat, decimals)
        da.Latitude_Longitude.attrs['urcrnrlon'] = round(urcrnrlon, decimals)
        da.Latitude_Longitude.attrs['urcrnrlat'] = round(urcrnrlat, decimals)

        # longitude attributes
        da.lon.attrs['deflate'] = 1
        da.lon.attrs['deflate_level'] = domain_cfg['nc_compression_level']
        da.lon.attrs['long_name'] = 'longitude'
        da.lon.attrs['units'] = 'degrees_east'
        da.lon.attrs['standard_name'] = 'longitude'
        da.lon.attrs['least_significant_digit'] = 4
        da.lon.attrs['_FillValue'] = MISSING_VALUE

        # latitude attributes
        da.lat.attrs['deflate'] = 1
        da.lat.attrs['deflate_level'] = domain_cfg['nc_compression_level']
        da.lat.attrs['long_name'] = 'latitude'
        da.lat.attrs['units'] = 'degrees_north'
        da.lat.attrs['standard_name'] = 'latitude'
        da.lat.attrs['least_significant_digit'] = 4
        da.lat.attrs['_FillValue'] = MISSING_VALUE

        # compress all variables and save output to new netcdf file
        comp = dict(zlib=True, complevel=domain_cfg['nc_compression_level'])
        encoding = {var: comp for var in da.data_vars}

        comp = dict(zlib=True, complevel=4)
        encoding = {var: comp for var in da.data_vars}

        with tempfile.NamedTemporaryFile() as nc_tmpfile:
            da.to_netcdf(nc_tmpfile.name, encoding=encoding)
            bucket_obj = opts.s3.Bucket(opts.MSL2_INTERMEDIATE_BUCKET)
            bucket_obj.upload_file(Filename=nc_tmpfile.name, Key=s3key)

            opts.logger.info(
                f" {opts.domain_id} Upload: s3://{opts.MSL2_INTERMEDIATE_BUCKET}/{s3key}"
                )

            # Upload to MODI -- Skip in Staging environment,
            # as there is no Staging MODI bucket!
            if (opts.ENV.lower() != 'stg') & (to_modi):
                MSL2_MODI_BUCKET_obj = opts.s3.Bucket(opts.MSL2_MODI_BUCKET)
                MSL2_MODI_BUCKET_obj.upload_file(Filename=nc_tmpfile.name,
                                            Key=modi_object_key,
                                            ExtraArgs={'ACL': 'bucket-owner-full-control'})
                opts.logger.info(
                    f" {opts.domain_id} Upload: s3://{opts.MSL2_MODI_BUCKET}/{modi_object_key}"
                    )
        return f"s3://{opts.MSL2_INTERMEDIATE_BUCKET}/{s3key}"
        
    except:
        print(traceback.format_exc())
        opts.logger.error(" ERROR saving / uploading output for {}-{}".format(opts.domain_id,output_type))

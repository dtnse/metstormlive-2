#!/usr/bin/env python3.8
# coding: utf-8
#
# DTN, LLC - METSTORM Live2
# MSL2 - MERGE.py (core code)
#   ICON & Remote Sensing Team (OCt, 2022)
######################################################################################
######################################################################################
from optparse import OptionParser
import os
import sys
import logging.config
from datetime import datetime
from pathlib import Path
import time

import numpy as np
import xarray as xr

from msl2_qpe_pkg.base import msl2class
from msl2_qpe_pkg.base import aws_utils
from msl2_qpe_pkg.base import output as base_outputs
from msl2_qpe_pkg.base import utils as base_utils
from msl2_qpe_pkg.base import message as base_message

######################################################################################
######################################################################################

def dummy_merge_main(opts, domain_id="CC"):
    """Diagostic QPE based on the workflow found at:
    https://dtnse1.atlassian.net/wiki/spaces/CCS/pages/33565901012/Proposed+Multi-sensor+source+Integration+Algorithm+for+MetStormLive2+QPE
    """
    # starts run
    start_time = time.time()
    opts.epoch_timestamp = datetime.utcnow().timestamp()    
    opts.logger.info(f" {domain_id} MergeBegan: {opts.zulu_time:%Y%m%d_%H%M%S}")
    opts.domain_id = domain_id

    # loads the domain and global config
    domain_config       = base_utils.load_domain_config(opts,domain_id)
    msl2_global_config  = base_utils.load_json_config(opts)

    # INIT THE MS2 Object
    MSL2obj = msl2class.MSL2classObj(domain_id)

    if not opts.is_fulldomain:
        opts.domain_id                    += "-"+opts.tile_id 
        domain_config["abbreviated_name"] += "-"+opts.tile_id
        domain_config["minx"]              = opts.minx
        domain_config["maxx"]              = opts.maxx
        domain_config["miny"]              = opts.miny
        domain_config["maxy"]              = opts.maxy
        output_type                        = "subdomain_tiles"
        to_modi                            = False
    else:
        output_type                        = "postprocessed"
        to_modi                            = True        

    #loading the domain grid
    MSL2obj.init_BASE_grid(domain_config)

    #=========================================================================
    # DUMMY OUTPUT
    tile_id_num = int(opts.tile_id.split("-")[1])
    m,n = MSL2obj.base_grid.new_2d_lon.values.shape

    ds = xr.Dataset(
            {
                "qpe_conf": (("lat","lon"),
                np.random.rand(m,n)*tile_id_num
                ),
            }
        )
    ds.coords["lat"]=(("lat"), MSL2obj.base_grid.lat.values)
    ds.coords["lon"]=(("lon"), MSL2obj.base_grid.lon.values)


    #=========================================================================
    # ECM FINAL CALCULATION
    opts.logger.info(f"{domain_id} Writing Confidence Metric ECM Output")

    output_cfg = {
        "title" : "QPE Confidence Array",
        "long_name": "QPE Confidence Array",
        "units" : "dimensionless [0 - 1]"        
    }

    s3url = base_outputs.write_output_netcdf(
        opts, 
        ds,
        opts.zulu_time,
        msl2_global_config,
        output_cfg,
        array_type="multi",
        out_var="qpe_conf",
        output_type="ECM",
        domain_id = domain_id,
        to_modi=to_modi,
        proc_stage=output_type,
        freq_id="001hr"
    )

    #send SQS message
    if opts.is_fulldomain:
        message_body = base_message.compose_message("MERGE_HANDLER",s3url)
    else:
        message_body = base_message.compose_message("MERGE_SUBDOMAIN_HANDLER",s3url,n_tries=opts.n_slices)
    is_msg       = base_message.send_message(opts.sqs_obj,message_body)
    if is_msg:
        opts.logger.info(f"{domain_id} ECM message SENT")

    return "second_guess"

if __name__ == "__main__":
    usage = ''
    p = OptionParser(usage=usage)
    p.add_option('--verbose', '-v', dest='verbose', default=False, action='store_true',
        help='increase logging to the DEBUG level.'
        )
    p.add_option('--config_path', help='Path to Ingest Config Files',default='./config',type='string')
    p.add_option('--logterminal', help='Path to configuration file',default=False,action='store_true')

    # =================================================================== 
    # Loading configurations 
    opts, args = p.parse_args()

    logging.config.fileConfig("msl2_qpe_pkg/base/logging.ini", disable_existing_loggers=False)
    opts.logger = logging.getLogger(__name__)
    opts.logger.info("+++++ MSL2: BEGIN MERGE POD ++++++ ")    
    # =================================================================== 
    # Get environment variables

    opts.ENV = os.getenv("ENV", "dev")    

    opts.MSL2_INTERMEDIATE_BUCKET = os.getenv(
        "MSL2_INTERMEDIATE_BUCKET",
        "metstormlive-v2-intermediate-dev")

    opts.MSL2_MODI_BUCKET = os.getenv(
        "MSL2_MODI_BUCKET",
        "mg-obs-gridded-dtn-1qpe-dev-euw1")

    #merge specific env vars
    opts.domain_id = os.getenv(
        "MSL2_DOMAIN_ID",
        "CC")

    opts.is_fulldomain = base_utils.str_to_bool(
        os.getenv("IS_FULLDOMAIN",
            "True")
        )

    opts.n_slices = os.getenv(
        "N_SLICES",
        "1")

    opts.tile_id = os.getenv(
        "TILE_ID",
        "TILE-001")

    opts.minx = float(os.getenv(
        "MINX",
        "0.1"))
    opts.maxx = float(os.getenv(
        "MAXX",
        "0.2"))

    opts.miny = float(os.getenv(
        "MINY",
        "0.1"))

    opts.maxy = float(os.getenv(
        "MAXY",
        "0.2"))

    # =================================================================== 
    # Initiate AWS clients

    # If full domain, send the message to the product queue
    # If it's a subdomain, send it to the coordinator queue instead
    if opts.is_fulldomain:
        opts.sqs_obj = aws_utils.start_sqs_clients(opts,env_var="MSL2_PRODUCT_QUEUE")
    else:
        opts.sqs_obj = aws_utils.start_sqs_clients(opts,env_var="MSL2_COORDINATOR_QUEUE")

    opts.s3_fs   = aws_utils.init_s3FileSystem(anon=False)
    opts.s3      = aws_utils.init_boto3S3Client()

    # for k8s health check
    Path('/tmp/healthy').touch()    

    #--------------------------------------
    # BEGIN MERGE
    if len(sys.argv) > 1:
        zulu_time = datetime.strptime(sys.argv[1], "%Y%m%d%H")
    else:
        zulu_time = datetime.utcnow()

    opts.zulu_time = zulu_time

    dummy_merge_main(opts,domain_id=opts.domain_id)
    exit(0)

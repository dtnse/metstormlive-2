import boto3
import logging.config
from datetime import datetime,timedelta
from optparse import OptionParser
from pathlib import Path

from msl2_qpe_pkg.base import aws_utils
from msl2_qpe_pkg.base import message as base_message

if __name__ == '__main__':
    usage = ''
    p = OptionParser(usage=usage)
    p.add_option('--verbose', '-v', dest='verbose', default=False, action='store_true',
        help='increase logging to the DEBUG level.'
        )
    #p.add_option('--config_path', help='Path to Ingest Config Files',default='./config',type='string')
    #p.add_option('--logterminal', help='Path to configuration file',default=False,action='store_true')

    Path('/tmp/healthy').touch()
    # ===================================================================
    # Loading configurations
    opts, args = p.parse_args()
    # ===================================================================
    # Intialize Logging & Broader Configurations

    logging.config.fileConfig("msl2_qpe_pkg/base/logging.ini", disable_existing_loggers=False)
    logger = logging.getLogger(__name__)
    opts.logger = logger

    # ===================================================================
    # ===================================================================
    #
    logger.info("+++++ MSL2: START Looking for new METPREC-GT files ++++++ ")

    # ===================================================================
    #  INIT SQS CLIENT 
    logger.info(" - starting SQS client")
    sqs_obj   = aws_utils.start_sqs_clients(opts,env_var="MSL2_PRODUCT_QUEUE")

    # ===================================================================
    #  do stuff
    bucket_name = "grid-data.clearag.com"
    prefix = "wxgrid_long_lived_v2/250.1.2038.157/250.61/1.0.-1.4.0.3600/"
    s3 = boto3.client('s3')
    response = s3.list_objects_v2(Bucket=bucket_name,Prefix=prefix)
    now = datetime.utcnow().astimezone()
    if 'Contents' in response:
        files = response['Contents']
        for f in files:
            #print(f)
            modtime = f['LastModified']
            if now - modtime < timedelta(hours=1):
                #send SQS message
                s3url = f"s3://{bucket_name}/{f['Key']}"
                message_body = base_message.compose_message("METPREC",s3url)
                is_msg       = base_message.send_message(sqs_obj,message_body)
    else:
        logger.info(f"No METPREC files found in {bucket_name}/{prefix}")



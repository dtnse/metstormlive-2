"""
gauge.py

Purpose: Collect precip observations from OneDTN APIs and the corresponding point values
         from grids into region-specific CSV files for feeding into MetStormLive.

Written:  T Rost, Oct. 2021

Modifications:
    T Rost, Feb. 2022       Added creation of gauge density grid.  Removed pyproj dependency.
    T Rost, Mar. 2022       Switched to OneObs API V2.
    T Rost, Sep. 2022       Added creation of 24h CSV file.
    R. Domingues, Nov 2022  Make Gauge processing domain specific
    T Rost, Nov. 2022       Updated 1Obs precip parameter names.

"""


# ========================================================================================
# Built-in modules
# ========================================================================================
import copy
import json
import os
import pickle
import tempfile
import logging.config
from optparse import OptionParser

from datetime import datetime, timedelta, timezone
from math import radians, cos, sin, asin, sqrt

# Third party modules
import boto3
import numpy as np
import pandas as pd
import requests

from netCDF4 import Dataset
from scipy import interpolate, stats

from msl2_qpe_pkg.base import utils as base_utils
from msl2_qpe_pkg.base import aws_utils

# ========================================================================================
# Program defaults and global variables
# ========================================================================================
usage = ''
p = OptionParser(usage=usage)
p.add_option('--verbose', '-v', dest='verbose', default=False, action='store_true',
    help='increase logging to the DEBUG level.'
    )
p.add_option('--config_path', help='Path to Ingest Config Files',default='./config',type='string')
p.add_option('--logterminal', help='Path to configuration file',default=False,action='store_true')
# =================================================================== 
# Loading configurations 
opts, args = p.parse_args()

logging.config.fileConfig("msl2_qpe_pkg/base/logging.ini", disable_existing_loggers=False)
logger = logging.getLogger(__name__)
opts.logger = logger

opts.logger.info("+++++ MSL2: BEGIN GAUGE PROCESSING ++++++ ")

# General MSL2 configuration file
config_data = base_utils.load_json_config(opts)

# List of internal station codes to ignore
STATION_BLACKLIST = [item["stid"] for item in config_data["station_blacklist"]]

# Precip ob QC flag models
model_path = "ml-models/"
QC_MODEL_FILE = model_path + "training_20180501_gbdt_365_days_v1.8.1.pkl"
# QC_MODEL_RADARLESS_FILE = model_path + "training_20200501_gbdt_365_days_for_brazil_v1.6.pkl"

# Missing value flag to be used in CSV output
MISSING = -9.9

# Max distance (km) to look for neighboring stations
MAX_NEIGHBOR_DISTANCE = 100.0

# parameters for CSV file, in the order they are to appear
CSV_PARAMS = [
    "n",
    "stid",
    "stn_name",
    "source",
    "lat_dd",
    "lon_dd",
    "elev_ft",
    "valid_time",
    "update_time",
    "stn_type",
    "obs_ppt_in",
    "bmap_in",
    "zrp_in",
    "st4_in",
    "sat_in",
    "oth_in",
    "isop",
    "gen_isop",
    "diff_isop",
    "qc_flag"
]
# output CSV header
HEADER = [
    "n",
    "stid",
    "stn_name",
    "source",
    "lat_dd",
    "lon_dd",
    "elev_ft",
    "valid_time_utc",
    "update_time_utc",
    "stn_type",
    "obs_ppt_in",
    "bmap_in",
    "zrp_in",
    "qpf_in",
    "sat_in",
    "type",
    "isop",
    "gen_isop",
    "diff_isop",
    "qc_flag"
]

# Precip QC flag model v1.8.1 PREDICTORS
PREDICTORS = [
    "zrp_in", "sat_in", "st4_in", "obs_ppt_in", "valid_time", "frozen", "lat_dd", "lon_dd", "elev_ft"
]

# PREDICTORS for model trained without radar-estimated precip data
# PREDICTORS_RADARLESS = list(PREDICTORS[1:])

# Critical PREDICTORS -- If none of these are available, don"t set QC flag
PREDICTORS_CRITICAL = list(PREDICTORS[:3])

# Mapping of type IDs in QPF type grids to IDs expected in MetStormLive
PTYPE_MAPPINGS = {
    "1": "8",  # snow -> snow
    "2": "1",  # rain -> rain
    "3": "1",  # rain/snow -> rain
    "4": "2",  # freezing rain -> freezing rain
    "5": "4",  # sleet -> sleet
    "6": "1",  # drizzle -> rain
    "7": "2",  # freezing drizzle -> freezing rain
    "8": "1"  # thunderstorm -> rain
}


def read_nc(file, record, csv_param, csv_data, units="unitless", missing_flag=-9999, filter_only=False):
    """
    Read point values out of NetCDF file, and convert units as appropriate.
    """

    # Open the file for reading
    nc = Dataset(file, "r")

    # Grid type expected to be lat-lon
    if nc.map_proj != "lat-lon":
        exit("Unexpected projection type: %s.  Expected lat-lon.  Exiting..." % nc.map_proj)

    # Read lat/lon into NumPy arrays
    lats = nc.variables["lat"]
    lons = nc.variables["lon"]

    # Get SW and NE coordinates and dimensions from arrays
    lat_sw, lon_sw = lats[0], lons[0]
    lat_ne, lon_ne = lats[-1], lons[-1]
    nrows, ncols = len(lats), len(lons)

    # Grid spacing in degrees
    dlon = (lon_ne - lon_sw) / (ncols-1.)
    dlat = (lat_ne - lat_sw) / (nrows-1.)

    # Read data array
    data = nc.variables[record][:, :]

    # Record attributes
    attributes = nc.variables[record].ncattrs()

    # Get units
    if "units" in attributes:
        units = nc.variables[record].getncattr("units")

    # Get missing value flag
    if "missing_value" in attributes:
        missing_flag = nc.variables[record].getncattr("missing_value")

    # Loop through stations and pluck data from nearest point in array
    for stid in list(csv_data):
        station = csv_data[stid]
        col = int(round((station["lon_dd"] - lon_sw) / dlon))
        row = int(round((station["lat_dd"] - lat_sw) / dlat))

        station[csv_param] = MISSING
        if 0 <= row < nrows and 0 <= col < ncols:
            if not filter_only:
                station[csv_param] = data[row][col]
        else:  # station is off the grid!
            csv_data.pop(stid)
            continue

        # we only want to filter out stations that exist outside this domain,
        # skip the rest of the logic in this loop
        if filter_only:
            continue

        # Missing data check
        if station[csv_param] == missing_flag or station[csv_param] is np.ma.masked:
            station[csv_param] = MISSING
            continue

        # Unit conversions
        if units.lower() == "mm":  # mm->in
            station[csv_param] *= 0.03937

        # Convert QPF type IDs to those expected in MetStormLive
        if csv_param == "oth_in":
            station[csv_param] = float(
                PTYPE_MAPPINGS[str(int(station[csv_param]))]
            )
            # QC model requires simple "frozen" flag rather than ptype code;
            # set to True unless code==1 ("Rain")...
            station.update(frozen=True)
            if station[csv_param] == 1.0:
                station.update(frozen=False)

        # Value rounding
        station[csv_param] = round(station[csv_param], 2)

    # close file
    nc.close()

    return


def calculate_isop(csv_data):
    """
    Isopercental derivation from observed and climatological QPE.
    """

    # First, compute at all stations
    for station in csv_data.values():
        obs_ppt_in = station["obs_ppt_in"]
        bmap_in = station["bmap_in"]
        station["isop"] = MISSING

        if obs_ppt_in == MISSING or bmap_in == MISSING:
            continue

        if bmap_in:
            station["isop"] = round((obs_ppt_in / bmap_in) * 10000, 1)

    # Then compute spatially averaged isopercental values from data at neighboring stations
    for station in csv_data.values():
        station["gen_isop"] = MISSING
        station["diff_isop"] = MISSING

        # Isopercental value cannot be missing
        target_isop = station["isop"]
        if target_isop == MISSING:
            continue

        neighboring_sites = station.get("neighboring_sites", dict())

        # Range-based station groupings
        distance_ranges = list()
        x = 11.11
        while x < 100.0:
            distance_ranges.append(dict(
                lower_threshold=x-11.11,
                upper_threshold=x,
                avg_isop = MISSING,
                avg_distance = MISSING,
                weight = MISSING,
                stations=list()
            ))
            x += 11.11

        # Add center station to 0-11.11km threshold grouping
        distance_ranges[0]["stations"].append(dict(isop=target_isop, distance=0.0, stid=station["stid"]))

        # Add neighbors to groupings
        for d in distance_ranges:
            lower_threshold = d["lower_threshold"]
            upper_threshold = d["upper_threshold"]

            for nid in list(neighboring_sites):
                distance = neighboring_sites[nid]["distance"]  # provided by metadata API
                if distance < lower_threshold or distance >= upper_threshold:
                    continue
                if nid not in csv_data:  # remove stations for which we did not receive data
                    station["neighboring_sites"].pop(nid)
                    continue
                isop = csv_data[nid]["isop"]
                if isop == MISSING:
                    continue
                d["stations"].append(dict(
                    isop=isop, distance=distance, stid=csv_data[nid]["stid"]
                ))

        # Compute average isops and distances in each grouping
        for d in distance_ranges:
            sum_isop, sum_distance = 0.0, 0.0
            for item in d["stations"]:
                sum_isop += item["isop"]
                sum_distance += item["distance"]
            if len(d["stations"]):
                d.update(avg_isop=sum_isop/len(d["stations"]))
                d.update(avg_distance=sum_distance/len(d["stations"]))
                d.update(weight=1.0-d["avg_distance"]/MAX_NEIGHBOR_DISTANCE)

        # Require averages from at least two groupings
        if sum([1 for d in distance_ranges if d["avg_isop"] != MISSING]) < 2:
            continue

        # Multiplicative factor to rescale distance-based weights, so they sum to 100%.
        # Locations closest to target site get highest weighting; those farthest away
        # get lowest weighting.
        scale_fac = 1.0/sum([d["weight"] for d in distance_ranges if d["weight"] != MISSING])

        # Compute distance-based weighted average isopercental value
        station["gen_isop"] = round(sum([
            d["avg_isop"] * d["weight"] * scale_fac for d in distance_ranges if d["weight"] != MISSING]), 1)

        # Calculate difference with target location
        station["diff_isop"] = round(target_isop - station["gen_isop"], 1)

    return


def fetch_station_data(valid_dt, parameter, metadata_key, s3_fs):
    valid_time = int(datetime.timestamp(valid_dt))
    startTime = valid_time - 900  # +/-15min to get data approximately valid at top of the hour
    endTime = startTime + 1800

    station_data = dict()  # will hold reformatted global station data

    # client = boto3.client("secretsmanager")
    # response = client.get_secret_value(
    #    SecretId="one-obs-api-auth-tokens",
    # )
    # secrets = json.loads(response.get("SecretString"))
    # First, get API access token
    url = "https://api.auth.dtn.com/v2/tokens/authorize"
    payload = json.dumps(dict(
        grant_type="client_credentials",
        client_id="0LVrTJleC0gmRDBAK4A9CpT43Q4VqFsh",
        client_secret="GZOzhNBuEOyXZUW6Os_rLL74QPMi1nyU1c_8ytSBBDP3ljlXJG7GGrmBrrHHnjKL",
        audience="http://one-obs-api.prd.wx.zones.dtn.com/"
    ))
    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json"
    }
    response = requests.post(url, data=payload, headers=headers)
    token = response.json()["data"]["access_token"]

    # Get all observations valid nearest the top of the hour from a lat-lon box
    # covering CONUS and Brazil
    url = "https://one-obs-api.prd.wx.zones.dtn.com/v2/observations"
    params = dict(
        by="boundingBox", minLat=-35.0, maxLat=59.0, minLon=-127.0, maxLon=-30.0,
        parameters=parameter, startTime=startTime, endTime=endTime, interval="1h"
    )
    headers = dict(Authorization="Bearer {}".format(token))
    raw_data = requests.get(url, params=params, headers=headers).json()
    if not isinstance(raw_data, list):  # occurs if no observations available
        return station_data

    # Load station metadata from file.
    metadata = dict()
    if s3_fs.exists(metadata_key):
        logger.info("Reading %s..." % metadata_key)
        f = s3_fs.open(metadata_key, "r")
        metadata = json.load(f)
        f.close()

    # Combine observations with metadata, and remove bad obs.
    # Fetch any missing metadata.
    logger.info("Merging observations with station metadata...")
    updated_meta = False  # flag to indicate whether metadata dict is updated in this section
    used_stations = []  # keep track of stations, to weed out any duplicates
    for station in raw_data:
        try:
            stid = station["stationCode"]
            # Ignore blacklisted sites
            assert stid not in STATION_BLACKLIST, \
                "Station %s: Blacklisted.  Skipping..." % stid
            station_data[stid] = dict()
            combined = station_data[stid]
            # Initialize all CSV output params and model PREDICTORS
            for p in CSV_PARAMS + PREDICTORS:
                combined[p] = MISSING
            combined["lat_dd"] = round(station["latitude"], 4)
            combined["lon_dd"] = round(station["longitude"], 4)
            # Select ob nearest to valid_time
            for item in station["parameters"]:
                if item["name"] != parameter:
                    continue
                observation = item["values"][0]  #"values" should point to list of one item
                combined.update(obs_ppt_in=observation["value"])  # obs_ppt_in [mm]
                combined.update(update_time=observation["properties"]["originalTimestamp"])
                combined.update(source=observation.get("obsType", MISSING))
                break
            # Weed out clearly bad values
            assert 0.0 <= combined["obs_ppt_in"] <= 152.4, \
                "Station %s: Value check failed! %s not between 0.0 and 152.4 mm.  Skipping..." % (
                    stid, combined["obs_ppt_in"])
            # Filter out obs valid outside the selected time window
            update_unix = datetime.strptime(
                ("%s +0000" % combined["update_time"]), "%Y-%m-%dT%H:%M:%S %z"
            ).timestamp()
            assert startTime <= update_unix <= endTime, \
                "Station %s: Ob valid time %s outside acceptable range!  Skipping..." % (
                    stid, combined["update_time"])
            # Convert to inches and round
            combined["obs_ppt_in"] = round(combined["obs_ppt_in"] * 0.03937, 2)
            # Get update_time in correct format - i.e., YYYYMMDD_HHmm
            combined["update_time"] = datetime.strptime(
                ("%s +0000" % combined["update_time"]), "%Y-%m-%dT%H:%M:%S %z"
            ).strftime("%Y%m%d_%H%M")
            # Set valid_time based on API query
            combined["valid_time"] = valid_dt.strftime("%Y%m%d_%H%M")
            # Set stn_type (H=hourly, D=daily)
            combined["stn_type"] = "H"
            if parameter == "precipAcc24Hour":
                combined["stn_type"] = "D"
            # Check for station metadata - Attempt to retrieve if missing.
            if stid not in metadata:
                logger.debug("Fetching metadata for station %s..." % stid)
                url = "https://one-obs-api.prd.wx.zones.dtn.com/v2/stations"
                params = dict(
                    by="radius", lat=station["latitude"], lon=station["longitude"],
                    radius=MAX_NEIGHBOR_DISTANCE
                )
                response = requests.get(url, params=params, headers=headers).json()

                # Look for stid
                for item in response:
                    if item["stationCode"] == stid:
                        metadata[stid] = dict(
                            info=item.get("info", None),
                            networks=item.get("networks", list()),
                            obsTypes=item.get("obsTypes", list()),
                            latitude=item.get("latitude", None),
                            longitude=item.get("longitude", None),
                            elevation=item.get("elevation", MISSING),
                            tags=item.get("tags", dict()),
                            neighboring_sites=dict()
                        )
                        response.remove(item)
                        break

                # Add neighboring stations
                for item in response:
                    try:
                        metadata[stid]["neighboring_sites"][item["stationCode"]] = dict(
                            distance=item["distance"]  # only save distance for now
                        )
                    except KeyError:
                        pass

                if stid in metadata:
                    logger.info("...added!")
                    updated_meta = True  # trigger rewrite of metadata file

            # Station ID (ICAO, MADIS, or internal)
            icao, madis_id = None, None
            if metadata[stid]["tags"] is not None:
                icao = metadata[stid]["tags"].get("icao")
                madis_id = metadata[stid]["tags"].get("madisId")
            tag = icao if icao else madis_id
            combined["stid"] = tag if tag else stid

            assert combined["stid"] not in used_stations, \
                "Station %s: Found duplicate observation.  Skipping..." % combined["stid"]
            used_stations.append(combined["stid"])
            # Station name
            try:
                # Remove comma from "stn_name" string to prevent parsing errors
                assert isinstance(metadata[stid]["tags"], dict)
                combined["stn_name"] = metadata[stid]["tags"]["name"].replace(",", "")
            except (AssertionError, KeyError):
                combined["stn_name"] = MISSING
            # Elevation
            try:
                combined["elev_ft"] = round(metadata[stid]["elevation"]*3.2808)  # m->ft
            except KeyError:
                combined["elev_ft"] = MISSING
            # Neighboring stations
            try:
                combined["neighboring_sites"] = copy.deepcopy(metadata[stid]["neighboring_sites"])
            except KeyError:
                combined["neighboring_sites"] = dict()
        except Exception as e:
            logger.debug("Exception: %s" % e)
            station_data.pop(stid, None)

    # Update metadata file
    if updated_meta:
        logger.info("Updating %s..." % metadata_key)
        f = s3_fs.open(metadata_key, "w")
        f.write(json.dumps(metadata, sort_keys=True))
        f.close()

    return station_data


def create_density_grid(region, csv_data, outfile, valid_dt):

    # domain specs
    proj4 = region["proj4"]
    lat_sw = region["miny"]
    lon_sw = region["minx"]
    lat_ne = region["maxy"]
    lon_ne = region["maxx"]
    nx = region["nx"]
    ny = region["ny"]
    lrnx = region["lrnx"]
    lrny = region["lrny"]
    kde_bandwidth = region["kde_bandwidth"]
    complev = region["nc_compression_level"]

    # decimal precision
    precision = 4

    # Following logic only applicable for lat-lon projection
    supported_projections = ["latlon", "latlong", "lonlat", "longlat"]
    if all([p not in proj4 for p in supported_projections]):
        logger.info("Warning: Expected one of the following for output projection: %s" % \
            str(supported_projections))

    # Convert CSV data dict to Pandas dataframe for MUCH easier parsing and filtering
    df = pd.DataFrame.from_dict(csv_data, orient="index")

    # Filter out "bad" obs
    good_obs = df
    if "qc_flag" in df.columns:
        good_obs = df[df["qc_flag"] >= 0.8]

    # Kernel density estimation requires AT LEAST 3 stations scattered across the domain, and not
    # appoximately co-located or in a line.  In the event of a drop in gauge availability and for
    # likely better spatial distribution, we"ll require at least 10 stations
    min_obs = 10

    # Initialize density and lat-lon arrays
    gauge_density = np.zeros((ny, nx))  # write a grid of zeros if KDE logic fails
    lats1d = np.linspace(lat_sw, lat_ne, ny)  # coordinates are evenly spaced on lat-lon proj
    lons1d = np.linspace(lon_sw, lon_ne, nx)
    lats2d, lons2d = np.meshgrid(lons1d, lats1d)

    try:
        # Make sure we have enough gauges
        assert len(good_obs) >= min_obs, \
            "Warning: Found %d observations with a QC flag >= 0.8. At least %d required to compute gauge density." % (
                len(good_obs), min_obs
            )

        # Note that we want coordinates on low-resolution grid for running KDE,
        # before remapping to the high-resolution output projection. Logic is
        # extremely slow at full resolution and does not yield much added benefit.
        lrlats1d = np.linspace(lat_sw, lat_ne, lrny)
        lrlons1d = np.linspace(lon_sw, lon_ne, lrnx)

        # 2D lat-lon arrays
        lrlons2d, lrlats2d = np.meshgrid(lrlons1d, lrlats1d)

        # Compress into 1D arrays and stack them together
        grid_coords = np.vstack([lrlons2d.ravel(), lrlats2d.ravel()])

        # Similarly create 2D stack of station coordinates
        station_coords = np.vstack([good_obs["lon_dd"].to_numpy(), good_obs["lat_dd"].to_numpy()])

        # Generate kernel density estimate from station locations -- Bandwidth setting
        # determines spatial "smoothness" / spreading of data, and was determined through
        # trial and error on 1/16-deg lat-lon grid.
        kde = stats.gaussian_kde(station_coords, bw_method=kde_bandwidth)

        # Apply to the entire grid to get a density value at every point
        lr_gauge_density = np.reshape(kde.evaluate(grid_coords), lrlons2d.shape)

        # Linearly rescale data into 0-1 range.
        # Using the 99th percentile as the top of the range rather than the max density value
        # helps boost values in areas with sparser gauge coverage.
        maxval = np.percentile(lr_gauge_density, 99.0)
        lr_gauge_density = np.clip(lr_gauge_density / maxval, 0.0, 1.0)

        # Now we need to interpolate to the desired dimensions.
        # Create interpolation function that we can apply
        f = interpolate.interp2d(lrlons1d, lrlats1d, lr_gauge_density)

        # Interpolate data to dimensions of the output grid
        gauge_density = f(lons1d, lats1d)

        # Round
        gauge_density = np.around(gauge_density, decimals=precision)
    except Exception as e:
        logger.info("Exception: %s" % e)

    def haversine(lon1, lat1, lon2, lat2):
        """
        Calculate the great circle distance in kilometers between two points
        on the earth (specified in decimal degrees)
        """
        # Convert decimal degrees to radians
        lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

        # Haversine formula
        dlon = lon2 - lon1
        dlat = lat2 - lat1
        a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
        c = 2 * asin(sqrt(a))
        r = 6371 # Radius of earth in km. Unit determines return value unit.

        return c * r

    # Compute spacing of output grid in km using haversine formula.
    # Note that dx spacing is evaluated at the Equator, where it is
    # expected to be true to scale. Dy spacing should be constant
    # throughout the grid.
    dx = haversine(lons1d[0], 0.0, lons1d[1], 0.0)
    dy = haversine(lons1d[0], lats1d[0], lons1d[0], lats1d[1])

    # Spacing in degrees
    dlon = (lon_ne - lon_sw) / (nx-1)
    dlat = (lat_ne - lat_sw) / (ny-1) * (-1)

    # Open output NetCDF file for writing
    ds = Dataset(outfile, "w", format="NETCDF4")

    # Add global attributes
    ds.Conventions = "CF-1.6"
    ds.title = "PMCAS_60min_Gauge_Density"
    ds.map_proj = "lat-lon"
    ds.institution = "DTN"
    ds.grid_mapping_name = "latitude_longitude"
    ds.DataConvention = "WE:SN"
    ds.nx = nx
    ds.ny = ny
    ds.dx = round(dx, precision)  # km
    ds.dy = round(dy, precision)
    ds.llcrnrlat = round(lat_sw, precision)
    ds.llcrnrlon = round(lon_sw, precision)
    ds.urcrnrlat = round(lat_ne, precision)
    ds.urcrnrlon = round(lon_ne, precision)
    ds.lowerleft_latlon = (round(lon_sw, precision), round(lat_sw, precision))
    ds.upperright_latlon = (round(lon_ne, precision), round(lat_ne, precision))
    ds.dlat = round(dlat, precision)  # deg
    ds.dlon = round(dlon, precision)

    # Add array dimensions
    ds.createDimension("lat", ny)
    ds.createDimension("lon", nx)

    # Add file variables
    # ... gauge density array
    nc_den60min = ds.createVariable(
        "den60min", "d", ("lat", "lon",), zlib=True, least_significant_digit=precision,
        complevel=complev, fill_value=np.nan
    )
    nc_den60min.coordinates = "lat lon"
    nc_den60min.grid_mapping = "latitude_longitude"
    nc_den60min.deflate = 1
    nc_den60min.deflate_level = complev
    nc_den60min.units = "dimensionless"
    nc_den60min.long_name = "Pmcas60minGaugeDensity"
    nc_den60min.description = "PMCAS gauge density on a scale from 0.0 (lowest density in the " + \
                              "grid) to 1.0 (highest density in the grid)."
    nc_den60min[:, :] = np.copy(gauge_density)

    # ... coordinate reference system definition
    nc_crs = ds.createVariable("crs", "c")
    nc_crs.grid_mapping_name = "latitude_longitude"
    nc_crs.long_name = "CRS definition"
    nc_crs.longitude_of_prime_meridian = 0.0
    nc_crs.semi_major_axis = 6378137.0
    nc_crs.inverse_flattening = 298.257223563
    nc_crs.spatial_ref = proj4
    nc_crs.GeoTransform = "%s %s 0 %s 0 %s " % (  # what do zeros indicate?
        round(lon_sw, precision), round(dlon, precision),
        round(lat_ne, precision), round(dlat, precision)
    )

    # ... 1D latitude array
    nc_lat = ds.createVariable("lat", "d", ("lat",), fill_value=np.nan)
    nc_lat.standard_name = "latitude"
    nc_lat.long_name = "latitude"
    nc_lat.units = "degrees_north"
    nc_lat[:] = np.copy(lats1d)

    # ... 1D longitude array
    nc_lon = ds.createVariable("lon", "d", ("lon",), fill_value=np.nan)
    nc_lon.standard_name = "longitude"
    nc_lon.long_name = "longitude"
    nc_lon.units = "degrees_east"
    nc_lon[:] = np.copy(lons1d)

    # ... data reference time
    nc_time = ds.createVariable("time", "i8")
    nc_time.standard_name = "time"
    nc_time.string = (f"{valid_dt:%Y-%m-%dT%H:%M:%SZ}")
    nc_time.period_string = "0 minutes"
    nc_time.units = (f"days since {valid_dt:%Y-%m-%d %H:%M:%S}")
    nc_time.calendar = "proleptic_gregorian"
    nc_time = 0

    # ... SW and NE corner coordinates
    nc_latlon = ds.createVariable("Latitude_Longitude", "i8")
    nc_latlon.grid_mapping_name = "latitude_longitude"
    nc_latlon.llcrnrlon = round(lon_sw, precision)
    nc_latlon.llcrnrlat = round(lat_sw, precision)
    nc_latlon.urcrnrlon = round(lon_ne, precision)
    nc_latlon.urcrnrlat = round(lat_ne, precision)
    nc_latlon = 0

    ds.close()

    return


def write_csv(bucket, s3key, data):
    """
    Write data to CSV file and upload to S3. Returns 0 on success.
    """

    local = tempfile.NamedTemporaryFile(mode="w", dir=".", suffix=".csv", delete=True)
    if not os.path.isfile(local.name):
        return 1
    local.write("%s\n" % ",".join(HEADER))  # header
    for n, station in enumerate(data.values()):
        station["n"] = n  # n=station counter
        row = [str(station.get(p, MISSING)) for p in CSV_PARAMS]
        local.write("%s\n" % ",".join(row))  # data
    local.flush()  # ensure all data written to file before we try submitting it!
    logger.info(f"PreprocessUpload: {s3key}")
    bucket.upload_file(Filename=local.name, Key=s3key)
    local.close()

    return 0


def process(opts,queue_listener, domain_id, valid_dt=datetime.utcnow(), minute_str="0"):

    # -- initialize datetime to top of the hour

    valid_dt = valid_dt.replace(minute=0, second=0, microsecond=0, tzinfo=timezone.utc)

    # -- get s3bucket name from queue listener object

    s3bucket = queue_listener.MSL2_INTERMEDIATE_BUCKET
    s3bucket_obj = queue_listener.s3.Bucket(s3bucket)

    # -- File containing station metadata pulled from OneObs Stations API

    metadata_key = "%s/default/one_obs_station_metadata.json" % s3bucket

    # -- Connect to S3

    s3 = boto3.client("s3")

    # -- Load precip QC models

    qc_model = None
    logger.info("Loading %s..." % QC_MODEL_FILE)
    with open(QC_MODEL_FILE, "rb") as f:
        qc_model = pickle.load(f)
    if qc_model is None:
        logger.info("...failed!")

    # -- Retrieve 1h observations

    logger.info("Fetching 1h precip observations valid at %s UTC..." %
                valid_dt.strftime("%Y-%m-%d %H:%M"))
    station_data = fetch_station_data(valid_dt, "precipAcc60Min", metadata_key, queue_listener.s3_fs)

    # -- Obtain gridded data at station locations, run precip QC model,
    # write CSV output, and generate gauge density grid

    region      = queue_listener.config["domains"][domain_id]        
    region_abbr = region["abbreviated_name"]        

    # Duplicate global station dictionary.  This will get whittled down
    # to only the stations existing within this subdomain.
    csv_data = copy.deepcopy(station_data)

    # -- GHE grid -- #
    s3key = "preprocessed/%s" % valid_dt.strftime("%Y%m%d_%H")
    s3key += "/%s_GHE_%s_001hr.nc" % (region_abbr, valid_dt.strftime("%Y%m%d_%H%M"))
    local = (s3key.split("/"))[-1]
    try:
        logger.info("Saving s3://%s/%s to %s..." % (s3bucket, s3key, local))
        s3.download_file(s3bucket, s3key, local)
    except Exception:
        logger.info("...failed!")
    if os.path.isfile(local):
        logger.info("Reading %s..." % local)
        read_nc(local, "ghe60min", "sat_in", csv_data)  # sat_in
        os.remove(local)

    # -- QPF grid -- #
    s3key = "preprocessed/%s" % valid_dt.strftime("%Y%m%d_%H")
    s3key += "/%s_QPF_%s_001hr.nc" % (region_abbr, valid_dt.strftime("%Y%m%d_%H%M"))
    local = (s3key.split("/"))[-1]
    try:
        logger.info("Saving s3://%s/%s to %s..." % (s3bucket, s3key, local))
        s3.download_file(s3bucket, s3key, local)
    except Exception:
        logger.info("...failed!")
    if os.path.isfile(local):
        logger.info("Reading %s..." % local)
        read_nc(local, "qpf60min", "st4_in", csv_data)
        os.remove(local)

    # -- Precip type grid -- #
    s3key = "preprocessed/%s" % valid_dt.strftime("%Y%m%d_%H")
    s3key += "/%s_TYP_%s_001hr.nc" % (region_abbr, valid_dt.strftime("%Y%m%d_%H%M"))
    local = (s3key.split("/"))[-1]
    try:
        logger.info("Saving s3://%s/%s to %s..." % (s3bucket, s3key, local))
        s3.download_file(s3bucket, s3key, local)
    except Exception:
        logger.info("...failed!")
    if os.path.isfile(local):
        logger.info("Reading %s..." % local)
        read_nc(local, "qpftype", "oth_in", csv_data)  # oth_in
        os.remove(local)

    # -- Radar-estimated precip -- #
    s3key = "preprocessed/%s" % valid_dt.strftime("%Y%m%d_%H")
    s3key += "/%s_Z2P_%s_001hr.nc" % (region_abbr, valid_dt.strftime("%Y%m%d_%H%M"))
    local = (s3key.split("/"))[-1]
    try:
        logger.info("Saving s3://%s/%s to %s..." % (s3bucket, s3key, local))
        s3.download_file(s3bucket, s3key, local)
    except Exception:
        logger.info("...failed!")
    if os.path.isfile(local):
        logger.info("Reading %s..." % local)
        read_nc(local, "Precipitation_from_Lowalt", "zrp_in", csv_data)  # zrp_in
        os.remove(local)

    # -- Basemap -- #
    s3key = "basemap/%s_bmp_1981to2010_annual.nc" % region_abbr
    local = s3key.split("/")[-1]
    try:
        logger.info("Saving s3://%s/%s to %s..." % (s3bucket, s3key, local))
        s3.download_file(s3bucket, s3key, local)
    except Exception:
        logger.info("...failed!")
    if os.path.isfile(local):
        logger.info("Reading %s..." % local)
        read_nc(local, "Basemap", "bmap_in", csv_data, units="mm")  # bmap_in
        os.remove(local)
    calculate_isop(csv_data)  # derive isopercental values from obs and basemap

    # -- Determine precip observation QC flags -- #
    # if all([m is not None for m in [qc_model, qc_model_radarless]]):
    if qc_model is not None:
        logger.info("Setting QC flags...")
        for stid, station in csv_data.items():
            # Run model -- There are 2 flavors: One trained with radar-estimated
            # precipitation data, and one trained without.  Choose the appropriate
            # model based on the available data.
            m, p = qc_model, PREDICTORS
            # if station["zrp_in"] == MISSING:  # zrp_in
            #    m, p = qc_model_radarless, PREDICTORS_RADARLESS
            if all([station[param] == MISSING for param in PREDICTORS_CRITICAL]):
                logger.debug(
                    "%s: Unable to predict precip QC flag! Not enough data available." % stid
                )
                continue
            # Create data dict to pass into model, resetting output MISSING flag
            # to the format expected by the model
            data = dict()
            for param in p:
                data[param] = station[param] if station[param] != MISSING else np.nan
            if data["valid_time"] is not np.nan:
                data["valid_time"] = int(  # remove year, underscore, and convert to int
                    data["valid_time"][4:].replace("_","")
                )
            station["qc_flag"] = m.predict(
                pd.json_normalize(data)[p], num_iteration=m.best_iteration)[0]

            # QCFlag adjustments
            if station["diff_isop"] != MISSING:
                abs_diff_isop = abs(station["diff_isop"])
                qcflag_adj = -0.6
                if abs_diff_isop < 1000.0:
                    qcflag_adj = 0.0000009738 * abs_diff_isop**2 - \
                                    0.0021008287 * abs_diff_isop + \
                                    0.5232044199
                station["qc_flag"] += qcflag_adj
                station["qc_flag"] = min([station["qc_flag"], 1.0])  # upper cap at 1.0
                station["qc_flag"] = max([station["qc_flag"], 0.0])  # lower cap at 0.0

            if str(station["source"]).upper() == "METAR":
                station["qc_flag"] += station["qc_flag"] * 0.6  # increase 60% for METARs
                station["qc_flag"] = min([station["qc_flag"], 1.0])

            qpes = [v for v in [station["zrp_in"], station["sat_in"], station["st4_in"]] if v >= 0.0]
            positive_qpes = sum([1 for v in qpes if v > 0.0])
            if positive_qpes >= 2 and station["diff_isop"] < 0.0 \
            and station["obs_ppt_in"] == 0.0:
                station["qc_flag"] -= station["qc_flag"] * 0.6  # reduce 60%
                station["qc_flag"] = max([station["qc_flag"], 0.0])

            zero_qpes = sum([1 for v in qpes if v == 0.0])
            if zero_qpes >= 2 and station["diff_isop"] > 0.0 \
            and station["obs_ppt_in"] > 0.0:
                station["qc_flag"] -= station["qc_flag"] * 0.6  # reduce 60%
                station["qc_flag"] = max([station["qc_flag"], 0.0])

            if station["obs_ppt_in"] > 0.0 and len(qpes):
                avg_qpes = sum(qpes) / len(qpes)
                if 0.0 < avg_qpes / station["obs_ppt_in"] < 0.04:
                    station["qc_flag"] -= 0.4
                    station["qc_flag"] = max([station["qc_flag"], 0.0])

            if all([v in [0.0, MISSING] for v in [station["zrp_in"], station["sat_in"], station["st4_in"]]]) \
            and station["diff_isop"] in [0.0, MISSING] \
            and station["obs_ppt_in"] == 0.0:
                station["qc_flag"] += 0.4
                station["qc_flag"] = min([station["qc_flag"], 1.0])

            station["qc_flag"] = round(station["qc_flag"], 2)  # round to 2 decimals

        # -- Write CSV output -- #
        logger.info("Writing 1h station data to file...")
        s3key = (f"preprocessed/{valid_dt:%Y%m%d_%H}/"
                 f"{region_abbr}_OBS_{valid_dt:%Y%m%d_%H%M}_min{minute_str}_001hr.csv")
        if write_csv(s3bucket_obj, s3key, csv_data):
            logger.error(f"Failed to upload {s3key}!")

        # -- Create and upload gauge density grid -- #
        local = (f"{region_abbr}_DEN_{valid_dt:%Y%m%d_%H%M}_min{minute_str}_001hr.nc")
        logger.info("Creating 1h gauge density grid %s..." % local)
        create_density_grid(region, csv_data, local, valid_dt)
        s3key = (f"preprocessed/{valid_dt:%Y%m%d_%H}/{local}")
        logger.info(f"PreprocessUpload: {s3key}")
        s3bucket_obj.upload_file(Filename=local, Key=s3key)
        os.remove(local)

    # ------------------------ #
    #                          #
    # Collect 24h observations #
    #                          #
    # ------------------------ #

    # -- Adjust valid time 7 days backwards

    valid_dt = valid_dt - timedelta(days=7)

    # -- Data this old should only need to be retrieved once, as no new 24h observations
    # should typically be appearing in the database by this time. So we'll only fetch data
    # if the output CSV files haven't already been created

    fetch24h = False

    region      = queue_listener.config["domains"][domain_id]
    region_abbr = region["abbreviated_name"]

    s3key = (f"preprocessed/{valid_dt:%Y%m%d_%H}/"
                f"{region_abbr}_OBS_{valid_dt:%Y%m%d_%H%M}_024hr.csv")
    if minute_str == "test":
        s3key = (f"preprocessed/{valid_dt:%Y%m%d_%H}/"
                    f"{region_abbr}_OBS_{valid_dt:%Y%m%d_%H%M}_{minute_str}_024hr.csv")
    try:
        # test key existence
        s3.head_object(Bucket=s3bucket, Key=s3key)
    except Exception:
        fetch24h = True

    if fetch24h:

        # Retrieve 24h observations
        logger.info("Fetching 24h precip observations valid at %s UTC..." %
                    valid_dt.strftime("%Y-%m-%d %H:%M"))
        station_data = fetch_station_data(
            valid_dt, "precipAcc24Hour", metadata_key, queue_listener.s3_fs
        )

        # Filter by region
        region      = queue_listener.config["domains"][domain_id]
        region_abbr = region["abbreviated_name"]

        # Duplicate global station dictionary.  This will get whittled down
        # to only the stations existing within this subdomain.
        csv_data = copy.deepcopy(station_data)

        # Use our basemap to filter out irrelevant locations
        logger.info(f"Filtering stations for {region_abbr} region...")
        s3key = (f"basemap/{region_abbr}_bmp_1981to2010_annual.nc")
        local = s3key.split("/")[-1]
        try:
            s3.download_file(s3bucket, s3key, local)
        except Exception:
            pass
        if os.path.isfile(local):
            read_nc(local, "Basemap", "bmap_in", csv_data, filter_only=True)  # bmap_in
            os.remove(local)

        # Write CSV output
        logger.info("Writing 24h station data to file...")
        s3key = (f"preprocessed/{valid_dt:%Y%m%d_%H}/"
                    f"{region_abbr}_OBS_{valid_dt:%Y%m%d_%H%M}_024hr.csv")
        if minute_str == "test":
            s3key = (f"preprocessed/{valid_dt:%Y%m%d_%H}/"
                        f"{region_abbr}_OBS_{valid_dt:%Y%m%d_%H%M}_{minute_str}_024hr.csv")
        if write_csv(s3bucket_obj, s3key, csv_data):
            logger.error(f"Failed to upload {s3key}!")

    return


if __name__ == "__main__":

    queue_listener_obj = aws_utils.MSL2QueueListener(opts, init_sqs=False)
    now_dt = datetime.utcnow()
    minute_str = now_dt.strftime("%M")
    domain_id = os.getenv(
        "MSL2_DOMAIN_ID",
        "CC"
        )
    logger.info(f"Processing Gauges for minute {minute_str} domain {domain_id}")

    process(opts, queue_listener_obj, domain_id,valid_dt=now_dt, minute_str=minute_str)
    logger.info("Processing Gauges complete!")
    exit(0)

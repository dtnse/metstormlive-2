#!/usr/bin/env python3.8
# coding: utf-8
#
# DTN, LLC - METSTORM Live2
# MBTILES HANDLER
#   ICON & Remote Sensing Team (Oct, 2022)
######################################################################################
######################################################################################
from datetime import datetime
import gzip
import logging
from optparse import OptionParser
from pathlib import Path
import tempfile
import time
import traceback
import shutil
import os
import logging.config

import xarray as xr

from msl2_qpe_pkg.base import aws_utils
from msl2_qpe_pkg.base import message as base_message
from msl2_qpe_pkg.base import utils as base_utils
from msl2_qpe_pkg.base import mbtiles as base_mbtiles
######################################################################################
######################################################################################

def build_metprec_tiles(opts,file_url,sec_config):

    # These files are zlib-compressed GRIB2 files with the timestamp in epoch seconds
    # The timestamp is the start of the hour, not the end, so we add 3600 to it
    # Everything in generate_tiles works, but the other stuff in build_tiles doesn't
    qpe_filename = os.path.basename(file_url)
    zulu_time = datetime.fromtimestamp(int(qpe_filename.split('.')[0]) + 3600)

    # Need pigz to decompress, don't think this will work with s3fs
    url_pieces = file_url[5:].split('/')
    opts.s3.Bucket(url_pieces[0]).download_file('/'.join(url_pieces[1:]),f"/tmp/{url_pieces[-1]}")

    os.system(f"unpigz /tmp/{url_pieces[-1]}")

    mbtiles = base_mbtiles.generate_tiles(opts,
            f"/tmp/{url_pieces[-1][:-3]}",
            palette=sec_config["color_palette"]
            )
    os.remove(f"/tmp/{url_pieces[-1][:-3]}")
    qpe_tilesname = f"NA_MPG_{zulu_time:%Y%m%d_%H}00_001hr.mbtiles"

    bucket_obj = opts.s3.Bucket(opts.MSL2_INTERMEDIATE_BUCKET)
    bucket_obj.upload_file(Filename=mbtiles,
                                Key=f"tiles/{zulu_time:%Y%m%d_%H}/{qpe_tilesname}",
                                ExtraArgs={'ACL': 'bucket-owner-full-control'})

    logger.info(f" MergeTilesUpload: {qpe_tilesname}")
    if os.path.exists(mbtiles): os.remove(mbtiles)

def build_tiles(opts, fileurl, sec_config):

    tgt_var = sec_config["tgt_var"]
    qpe_filename = os.path.basename(fileurl)

    # Determine file valid time, if possible.  MSL2 filenaming
    # is expected to follow a certain convention, so we should
    # be able to obtain it from that
    zulu_time = datetime.utcnow()  # default setting
    try:
        date, time = (qpe_filename.split('_'))[2:4]
        zulu_time = datetime.strptime(
            (f"{date} {time} +0000"), "%Y%m%d %H%M %z"
        )
    except Exception:
        pass

    fgz = opts.s3fs.open(fileurl)    
    if fileurl[-3:] == '.gz':        
        with gzip.open(fgz, 'rb') as fobj:
            qpe_tiles = xr.open_dataset(fobj)
    else:
            qpe_tiles = xr.open_dataset(fgz)

    qpe_tiles[tgt_var].rio.set_spatial_dims(x_dim="lon", y_dim="lat",
                                                inplace=True)

    raster = qpe_tiles[tgt_var].rio.write_crs("EPSG:4326")

    if "grid_mapping" in raster.attrs:
        del(raster.attrs['grid_mapping'])

    with tempfile.NamedTemporaryFile() as nc_tmpfile:
        nc_tmpfile.name+=".nc"
        
        raster.rio.set_spatial_dims(x_dim="lon", y_dim="lat",
                                                inplace=True)
        raster.rio.to_raster(nc_tmpfile.name, driver="GTiff")

        # opts.s3fs.download(fileurl,nc_tmpfile.name)

        if sec_config["file_type"] == "standard":
            mbtiles = base_mbtiles.generate_tiles(
                opts,
                nc_tmpfile.name,
                palette=sec_config["color_palette"]
                )
        elif sec_config["file_type"] == "vis3":
            mbtiles = base_mbtiles.generate_tiles_vsis3(
                opts,
                nc_tmpfile.name,
                palette=sec_config["color_palette"],
                nc_dataset_name=sec_config["tgt_var"]
                )
        print("MBTILES name {}".format(mbtiles))
        data_filename = qpe_filename.replace(".nc", ".mbtiles")

        bucket_obj = opts.s3.Bucket(opts.MSL2_INTERMEDIATE_BUCKET)
        bucket_obj.upload_file(Filename=mbtiles,
                                    Key=f"tiles/{zulu_time:%Y%m%d_%H}/{data_filename}",
                                    ExtraArgs={'ACL': 'bucket-owner-full-control'})

    if os.path.exists(nc_tmpfile.name): os.remove(nc_tmpfile.name)
    if os.path.exists(mbtiles): os.remove(mbtiles)

    logger.info(f" MergeTilesUpload: {data_filename}")


######################################################################################
######################################################################################



if __name__ == '__main__':
    usage = ''
    p = OptionParser(usage=usage)
    p.add_option('--verbose', '-v', dest='verbose', default=False, action='store_true',
        help='increase logging to the DEBUG level.'
        )
    p.add_option('--config_path', help='Path to Ingest Config Files',default='./config',type='string')
    p.add_option('--logterminal', help='Path to configuration file',default=False,action='store_true')

    Path('/tmp/healthy').touch()
    # =================================================================== 
    # Loading configurations 
    opts, args = p.parse_args()
    # ===================================================================
    # Intialize Logging & Broader Configurations

    logging.config.fileConfig("msl2_qpe_pkg/base/logging.ini", disable_existing_loggers=False)
    logger = logging.getLogger(__name__)
    opts.logger = logger

    # =================================================================== 
    # =================================================================== 
    # 
    logger.info("+++++ MSL2: BEGING Mbtiles Processing ++++++ ")
                
    # =================================================================== 
    #  INIT SQS CLIENT AND STARTS LISTENING
    logger.info(" - starting SQS listener")
    sqs_obj   = aws_utils.start_sqs_clients(opts,env_var="MSL2_PRODUCT_QUEUE")

    opts.s3fs = aws_utils.init_s3FileSystem(anon=False)
    opts.s3   = aws_utils.init_boto3S3Client()

    # =================================================================== 
    #  load env variables

    opts.MSL2_INTERMEDIATE_BUCKET = os.getenv(
        "MSL2_INTERMEDIATE_BUCKET",
        "metstormlive-v2-intermediate-dev")

    # ===================================================================
    #  LOADING TRIGGER CONFIGURATIONS

    config_triggers = base_utils.load_yaml_config(
        cfg_file="{}/mbtiles_config.yaml".format(opts.config_path)
        )

    #listen to queue ------------------------------------------
    while True:
        logger.info('... listening for messages')
        time.sleep(.2)
        message = False
        try:
            response = sqs_obj.sqs_client.receive_message(
                QueueUrl=sqs_obj.queue_url, MaxNumberOfMessages=1, 
                WaitTimeSeconds=20
                )
            message = response.get('Messages', [{}])[0]
        except:
            logger.error('... error retrieving messages from queue {0}'.format(sqs_obj.queue_url))
            print(traceback.format_exc())
            message = False

        if message:
            try:
                logger.debug('... checking msg')
                message_id, urlget, message_type, downstream_user, n_tries = base_message.parse_message(
                    message,sqs_obj.queue_url,sqs_obj.sqs_client
                    )
                logger.info('... checking msg: {0} {1}'.format(
                    message_type,
                    os.path.basename(urlget))
                )
                is_match = False
                is_match, sec_use = base_message.check_trigger_match(config_triggers, urlget)                
            except:
                logger.error('... error parsing message from queue {0}'.format(sqs_obj.queue_url))
                print(traceback.format_exc())
                is_match = False

            #checks for a match with ingest triggers
            if is_match:
                logger.info('... Trigger Mbtiles production')
                # TRIGGER HERE
                try:
                    if "clearag" in urlget:                   
                        build_metprec_tiles(opts,urlget,config_triggers[sec_use])
                    else:
                        build_tiles(opts, urlget, config_triggers[sec_use])
                except:
                    opts.logger.error(" FAILED to produce mbtiles from {}".format(os.path.basename(urlget)))
                    print(traceback.format_exc())

                base_message.delete_message(message, sqs_obj.queue_url, sqs_obj.sqs_client)
            else:
                print(" delete MSG")
                base_message.delete_message(message, sqs_obj.queue_url, sqs_obj.sqs_client)

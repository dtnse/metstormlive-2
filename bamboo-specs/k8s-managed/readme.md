# K8s-Infra Managed Builds

Configuration within `k8s-managed` is managed by the K8s-Infra team
and should not be altered in any way.

When adding additional `bamboo-specs`, ensure the following lines exist
in `bamboo.yml`:

```
---
!include 'k8s-managed/build-permissions.yml'
---
!include 'k8s-managed/build.yml'
```
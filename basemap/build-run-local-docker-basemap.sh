#!/bin/bash

export AWS_PROFILE="dtn-wx-dev"

docker build -f Dockerfile.basemap -t basemap:latest . 
docker run -it --rm \
  --mount "type=bind,src=${HOME}/.aws,dst=/etc/.aws,readonly" \
  -e 'AWS_CONFIG_FILE=/etc/.aws/config' \
  -e 'AWS_SHARED_CREDENTIALS_FILE=/etc/.aws/credentials' \
  -e AWS_PROFILE \
  -e AWS_DEFAULT_REGION basemap:latest 

import os
from os import listdir
from os.path import isfile, join
from datetime import datetime

import pandas as pd
from osgeo import gdal
import xarray as xr

# from queue_listener import MSL2QueueListener
import queue_listener

def reformat(file_list):
    month_lookup = {1: "Jan", 2: "Feb", 3: "Mar", 4: "Apr", 5: "May", 6: "Jun", 7: "Jul", 8: "Aug",
                    9: "Sep", 10: "Oct", 11: "Nov", 12: "Dec"}
    for domain in queue.config["domains"]:
        w = domain["minx"]
        s = domain["miny"]
        e = domain["maxx"]
        n = domain["maxy"]
        dx = domain["grid_dx_deg"]
        dy = domain["grid_dy_deg"]
        nx = domain["nx"]
        ny = domain["ny"]
        region = domain["abbreviated_name"]
        status = []
        monthly_bmaps = []
        bucket_obj = queue.s3.Bucket(queue.MSL2_INTERMEDIATE_BUCKET)
        for file in file_list:
            # naming convention CHELSA_pr_01_1981-2010_V.2.1.tif
            month = month_lookup[int(file.split("_")[2])]
            temp_nc = f'{region}_bmp_1981to2010_{month}_temp.nc'
            output_name = f'{region}_bmp_1981to2010_{month}.nc'
            annual_outname = f'{region}_bmp_1981to2010_annual.nc'
            gdal.Warp(temp_nc, file, outputBounds=[w-dx/2.0, s-dy/2.0, e+dx/2.0, n+dy/2.0],
                      format='NetCDF', width=nx, height=ny)

            updated = xr.open_dataset(temp_nc)
            updated = updated.rename({'Band1': 'Basemap'})
            updated['Basemap'] = updated.Basemap.round(decimals=4)
            fx_height = updated.dims['lat']
            fx_width = updated.dims['lon']

            # create longitude variable
            # create pandas df with column of longitude
            lon = pd.DataFrame(updated.coords['lon'], columns=['lon'])
            # convert column to row
            lon_transposed = lon.T
            # repeat row over number of latitudes
            lon2d = pd.concat([lon_transposed] * fx_height, ignore_index=True)
            # save dataframe to xarray, assigning coordinates to lat/lon
            # with precision of 4 decimals
            updated['longitude'] = (('lat', 'lon'), round(lon2d, 4))

            # create latitude variable
            # create pandas df with column of latitude
            lat = pd.DataFrame(updated.coords['lat'])
            # convert column to row
            lat_transposed = lat.T
            # repeat row over number of longitudes
            lat2d_transposed = pd.concat([lat_transposed] * fx_width,
                                         ignore_index=True)
            # transpose dataframe so lat is changing by columns and not rows
            lat2d = lat2d_transposed.T
            # save dataframe to xarray, assigning coordinates to lat/lon
            # with precision of 4 decimals
            updated['latitude'] = (('lat', 'lon'), round(lat2d, 4))

            # copy direct non 2-d variables
            updated['forecast_reference_time'] = 0
            updated['time'] = 0
            updated['Latitude_Longitude'] = 0

            # set global attributes
            metadata = queue.config['metadata']
            updated.attrs[
                'title'] = month + 'MeanMonthlyPrecipitation'
            updated.attrs['map_proj'] = metadata['map_proj']
            updated.attrs['Conventions'] = metadata['Conventions']
            updated.attrs['institution'] = metadata['institution']
            updated.attrs['nx'] = fx_width
            updated.attrs['ny'] = fx_height
            updated.attrs['dx'] = 1.11
            updated.attrs['dy'] = 1.11
            updated.attrs['llcrnrlat'] = round(s, 1)
            updated.attrs['llcrnrlon'] = round(w, 1)
            updated.attrs['urcrnrlat'] = round(n, 1)
            updated.attrs['urcrnrlon'] = round(e, 1)
            updated.attrs['lowerleft_latlon'] = [round(w, 1), round(s, 1)]
            updated.attrs['upperright_latlon'] = [round(e, 1), round(n, 1)]
            updated.attrs['dlat'] = 0.01
            updated.attrs['dlon'] = 0.01
            updated.attrs['grid_mapping_name'] = metadata['grid_mapping_name']
            updated.attrs['DataConvention'] = metadata['DataConvention']
            updated.attrs['Description'] = 'Data from CHELSA v2.1'

            # set up attributes for each band
            # Precip from lowalt attibutes
            updated.Basemap.attrs['coordinates'] = metadata[
                'coordinates']
            updated.Basemap.attrs['grid_mapping'] = metadata[
                'grid_mapping_name']
            updated.Basemap.attrs['least_significant_digit'] = 0
            updated.Basemap.attrs['deflate'] = 1
            updated.Basemap.attrs['deflate_level'] = 4
            updated.Basemap.attrs['units'] = 'kg m-2 month-1/100'
            updated.Basemap.attrs[
                'long_name'] = month + 'MeanMonthlyPrecipitation'
            updated.Basemap.attrs['missing_value'] = -99900.0

            # time attributes
            # TODO Hard coding climatological range - will need updating if using different period
            zulu_time = datetime(1981, int(file.split("_")[2]), 1, 0, 0, 0)
            zulu_time_str = datetime.strftime(zulu_time, '%Y-%m-%dT%H:00:00Z')
            updated.time.attrs['standard_name'] = 'time'
            updated.time.attrs['string'] = zulu_time_str
            # TODO update minutes since if different
            updated.time.attrs['period_string'] = '15776640 minutes'
            updated.time.attrs['units'] = 'Minutes since ' + zulu_time_str

            # forecast_reference_time attributes
            updated.forecast_reference_time.attrs[
                'standard_name'] = 'forecast_reference_time'
            updated.forecast_reference_time.attrs['string'] = zulu_time_str
            updated.forecast_reference_time.attrs['period_string'] = '15776640 minutes'

            # latitude_longitude attributes
            updated.Latitude_Longitude.attrs['grid_mapping_name'] = metadata[
                'grid_mapping_name']
            updated.Latitude_Longitude.attrs['llcrnrlon'] = round(w, 1)
            updated.Latitude_Longitude.attrs['llcrnrlat'] = round(s, 1)
            updated.Latitude_Longitude.attrs['urcrnrlon'] = round(e, 1)
            updated.Latitude_Longitude.attrs['urcrnrlat'] = round(n, 1)

            # longitude attributes
            updated.longitude.attrs['deflate'] = 1
            updated.longitude.attrs['deflate_level'] = 4
            updated.longitude.attrs['long_name'] = 'longitude'
            updated.longitude.attrs['units'] = 'degrees_east'
            updated.longitude.attrs['standard_name'] = 'longitude'
            updated.longitude.attrs['east_significant_digit'] = 4

            # latitude attributes
            updated.latitude.attrs['deflate'] = 1
            updated.latitude.attrs['deflate_level'] = 4
            updated.latitude.attrs['long_name'] = 'latitude'
            updated.latitude.attrs['units'] = 'degrees_north'
            updated.latitude.attrs['standard_name'] = 'latitude'
            updated.latitude.attrs['east_significant_digit'] = 4

            # compress all variables and save output to new netcdf file
            comp = dict(zlib=True, complevel=4)
            encoding = {var: comp for var in updated.data_vars}
            updated.to_netcdf(output_name, encoding=encoding)
            output_s3name = 'preprocessed/' + output_name
            bucket_obj.upload_file(Filename=output_name, Key=output_s3name)
            status.append(f"Uploaded {output_name} to S3 bucket {bucket_obj.name}")
            # updated["Basemap"] = updated.Basemap.expand_dims('time')
            if file == file_list[0]:
                annual = updated.copy()
            else:
                annual['Basemap'] = annual.Basemap + updated.Basemap

            if file == file_list[-1]:
                annual['Basemap'] = annual.Basemap.round(decimals=4)
                annual.Basemap.attrs['long_name'] = 'MeanAnnualPrecipitation'
                updated.attrs['title'] = 'MeanAnnualPrecipitation'
                encoding = {var: comp for var in annual.data_vars}
                annual.to_netcdf(annual_outname, encoding=encoding)
                annual_s3name = 'preprocessed/' + annual_outname
                bucket_obj.upload_file(Filename=annual_outname, Key=annual_s3name)
                status.append(f"Uploaded {annual_outname} to s3 bucket {bucket_obj.name}")
            del updated
            os.remove(temp_nc)


if __name__ == "__main__":
    path = '/home/data/basemap/'
    files_list = listdir(path)
    list_path_files = [path + f for f in files_list if isfile(join(path, f))]

    queue = queue_listener.MSL2QueueListener()

    reformat(list_path_files)


#!/bin/bash

# Provide dataset name to test, or test all by default
export MSL2_TESTING=${1:-ALL}
export MSL2_TESTING_DATETIME=${2:-DEFAULT}

#minikube stop
minikube start --kubernetes-version=v1.21.8 --driver=hyperkit --container-runtime=docker --mount --mount-string="$HOME/.aws:/etc/.aws" 

eval $(minikube docker-env)
docker build -t msl2-dev .
kubectl config use-context minikube
kubectl delete pod msl2-minikube
# substitute environment variable into pod template
cat minikube-k8s-pod.yaml | envsubst | kubectl apply -f -
kubectl wait --for=condition=Ready=true pod/msl2-minikube && kubectl logs --follow msl2-minikube

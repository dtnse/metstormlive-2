"""
blockagemap.py

Purpose: Reformat local beam blockage + cluttermap GeoTIFF file into NetCDF format,
         and reproject to applicable MSL2 domain(s).

Written: T Rost, Jun. 2022

Modifications:

"""

# ========================================================================================
# Built-in modules
# ========================================================================================
import os
import sys

# Third party modules
import pandas as pd
import xarray as xr
from osgeo import gdal

# Import ../preprocessing/queue_listener.py
sys.path.append(os.getcwd().replace("blockagemap", "preprocessing"))
import queue_listener  # noqa: E402


def reformat(file):

    queue = queue_listener.MSL2QueueListener(init_sqs=False)
    bucket_obj = queue.s3.Bucket(queue.MSL2_INTERMEDIATE_BUCKET)

    # Get source grid domain from filename and bounding coordinates
    fname = file.split("/")[-1]
    source_region = fname.split("_")[0].upper()
    source_w, source_s, source_e, source_n = None, None, None, None
    for domain in queue.config["domains"]:
        if domain["abbreviated_name"] == source_region:
            source_w = domain["minx"]
            source_s = domain["miny"]
            source_e = domain["maxx"]
            source_n = domain["maxy"]
            break
    if None in [source_w, source_s, source_e, source_n]:
        exit("Failure to determine domain from input filename. " +
             "Expected naming convention: NA_radar_beam_block_clutter_mask_v1.tif")

    for domain in queue.config["domains"]:
        w = domain["minx"]
        s = domain["miny"]
        e = domain["maxx"]
        n = domain["maxy"]
        dx = domain["grid_dx_deg"]
        dy = domain["grid_dy_deg"]
        nx = domain["nx"]
        ny = domain["ny"]
        region = domain["abbreviated_name"]

        # skip if not contained within source grid extents
        if (w < source_w or e > source_e or
                s < source_s or n > source_n):
            continue

        print(f"Opening {file}...")
        tempfile = "temp.nc"
        gdal.Warp(tempfile, file, outputBounds=[w-dx/2.0, s-dy/2.0, e+dx/2.0, n+dy/2.0],
                  format="NetCDF", width=nx, height=ny)

        updated = xr.open_dataset(tempfile)
        updated = updated.rename({"Band1": "blockagemap"})

        # Value range natively 0.0 (blocked) to 1.0 (unblocked),
        # but we want this inverted
        point_selection = updated["blockagemap"].values >= 0.0
        updated["blockagemap"].values[point_selection] = 1.0 - updated[
            "blockagemap"].values[point_selection]

        # Round
        updated["blockagemap"] = updated.blockagemap.round(decimals=4)

        fx_height = updated.dims["lat"]
        fx_width = updated.dims["lon"]

        # create longitude variable
        # create pandas df with column of longitude
        lon = pd.DataFrame(updated.coords["lon"], columns=["lon"])
        # convert column to row
        lon_transposed = lon.T
        # repeat row over number of latitudes
        lon2d = pd.concat([lon_transposed] * fx_height, ignore_index=True)
        # save dataframe to xarray, assigning coordinates to lat/lon
        # with precision of 4 decimals
        updated["longitude"] = (("lat", "lon"), round(lon2d, 4))

        # create latitude variable
        # create pandas df with column of latitude
        lat = pd.DataFrame(updated.coords["lat"])
        # convert column to row
        lat_transposed = lat.T
        # repeat row over number of longitudes
        lat2d_transposed = pd.concat([lat_transposed] * fx_width,
                                     ignore_index=True)
        # transpose dataframe so lat is changing by columns and not rows
        lat2d = lat2d_transposed.T
        # save dataframe to xarray, assigning coordinates to lat/lon
        # with precision of 4 decimals
        updated["latitude"] = (("lat", "lon"), round(lat2d, 4))

        # copy direct non 2-d variables
        updated["Latitude_Longitude"] = 0

        # set global attributes
        metadata = queue.config["metadata"]
        updated.attrs["title"] = "Beam blockage and clutter mask"
        updated.attrs["map_proj"] = metadata["map_proj"]
        updated.attrs["Conventions"] = metadata["Conventions"]
        updated.attrs["institution"] = metadata["institution"]
        updated.attrs["nx"] = fx_width
        updated.attrs["ny"] = fx_height
        updated.attrs["dx"] = 1.11
        updated.attrs["dy"] = 1.11
        updated.attrs["llcrnrlat"] = round(s, 1)
        updated.attrs["llcrnrlon"] = round(w, 1)
        updated.attrs["urcrnrlat"] = round(n, 1)
        updated.attrs["urcrnrlon"] = round(e, 1)
        updated.attrs["lowerleft_latlon"] = [round(w, 1), round(s, 1)]
        updated.attrs["upperright_latlon"] = [round(e, 1), round(n, 1)]
        updated.attrs["dlat"] = 0.01
        updated.attrs["dlon"] = 0.01
        updated.attrs["grid_mapping_name"] = metadata["grid_mapping_name"]
        updated.attrs["DataConvention"] = metadata["DataConvention"]

        # set up attributes for each band
        # Precip from lowalt attibutes
        updated.blockagemap.attrs["coordinates"] = metadata["coordinates"]
        updated.blockagemap.attrs["grid_mapping"] = metadata["grid_mapping_name"]
        updated.blockagemap.attrs["least_significant_digit"] = 4
        updated.blockagemap.attrs["deflate"] = 1
        updated.blockagemap.attrs["deflate_level"] = domain["nc_compression_level"]
        updated.blockagemap.attrs["units"] = "dimensionless"
        updated.blockagemap.attrs["long_name"] = "Beam blockage and clutter mask"
        updated.blockagemap.attrs["description"] = "Values on a scale from 0.0 (no blockage or " + \
            "persistent clutter) to 1.0 (completely blocked / persistent clutter)."

        # latitude_longitude attributes
        updated.Latitude_Longitude.attrs["grid_mapping_name"] = metadata["grid_mapping_name"]
        updated.Latitude_Longitude.attrs["llcrnrlon"] = round(w, 1)
        updated.Latitude_Longitude.attrs["llcrnrlat"] = round(s, 1)
        updated.Latitude_Longitude.attrs["urcrnrlon"] = round(e, 1)
        updated.Latitude_Longitude.attrs["urcrnrlat"] = round(n, 1)

        # longitude attributes
        updated.longitude.attrs["deflate"] = 1
        updated.longitude.attrs["deflate_level"] = domain["nc_compression_level"]
        updated.longitude.attrs["long_name"] = "longitude"
        updated.longitude.attrs["units"] = "degrees_east"
        updated.longitude.attrs["standard_name"] = "longitude"
        updated.longitude.attrs["least_significant_digit"] = 4

        # latitude attributes
        updated.latitude.attrs["deflate"] = 1
        updated.latitude.attrs["deflate_level"] = domain["nc_compression_level"]
        updated.latitude.attrs["long_name"] = "latitude"
        updated.latitude.attrs["units"] = "degrees_north"
        updated.latitude.attrs["standard_name"] = "latitude"
        updated.latitude.attrs["least_significant_digit"] = 4

        # compress all variables and save output to new netcdf file
        output_name = f"{region}_BLK_latest.nc"
        print(f"Writing to {output_name}...")
        comp = dict(zlib=True, complevel=domain["nc_compression_level"])
        encoding = {var: comp for var in updated.data_vars}
        updated.to_netcdf(output_name, encoding=encoding)
        output_s3name = f"default/{output_name}"
        print(f"Uploading to s3://{bucket_obj.name}/{output_s3name}...")
        bucket_obj.upload_file(Filename=output_name, Key=output_s3name)

        # Cleanup
        del updated
        os.remove(tempfile)
        os.remove(output_name)

    return


if __name__ == "__main__":

    try:
        assert len(sys.argv) == 2
    except AssertionError:
        exit("Usage: %s /path/to/input/file.tif" % sys.argv[0])

    file = sys.argv[1]

    try:
        assert os.path.isfile(file)
    except AssertionError:
        exit("%s does not exist!" % file)

    reformat(file)

    exit(0)

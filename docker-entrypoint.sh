#!/bin/bash


if [[ ${MSL2APP} == "COORDINATOR" ]]; then
    cd /msl2/
    conda run --no-capture-output -n metstormlive2-preprocessing python coordinator_handler.py

elif [[ ${MSL2APP} == "PPR_ACCUM" ]]; then
    cd /msl2/
    conda run --no-capture-output -n metstormlive2-preprocessing python radar_handler.py

elif [[ ${MSL2APP} == "MERGE" ]]; then
    run_date=$(date -u +%Y%m%d%H)
    cd /msl2/
    conda run --no-capture-output -n metstormlive2-preprocessing python gauge_handler.py
    conda run --no-capture-output -n metstormlive2-preprocessing python merge_handler.py "${run_date}"

elif [[ ${MSL2APP} == "MERGEJOBTEST" ]]; then
    run_date=$(date -u +%Y%m%d%H)
    cd /msl2/
    conda run --no-capture-output -n metstormlive2-preprocessing python dummy_merge.py "${run_date}"

elif [[ ${MSL2APP} == "STITCH" ]]; then
    run_date=$(date -u +%Y%m%d%H)
    cd /msl2/
    conda run --no-capture-output -n metstormlive2-preprocessing python stitch_handler.py "${run_date}"
    cd /msl2/postprocessing/
    conda run --no-capture-output -n metstormlive2-preprocessing python allocate.py "${run_date}"

elif [[ ${MSL2APP} == "MBTILES" ]]; then
    cd /msl2/
    conda run --no-capture-output -n metstormlive2-preprocessing python mbtiles_handler.py

elif [[ ${MSL2APP} == "METPREC" ]]; then
    cd /msl2/
    conda run --no-capture-output -n metstormlive2-preprocessing python metprec_handler.py

elif [[ ${MSL2APP} == "PREPROCCESS" ]]; then    
    cd /msl2/
    conda run --no-capture-output -n metstormlive2-preprocessing python preprocessing_handler.py

else
    conda run --no-capture-output -n metstormlive2-preprocessing python run_tests.py "$MSL2_TESTING" "$MSL2_TESTING_DATETIME"
fi
